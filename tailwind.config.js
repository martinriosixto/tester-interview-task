module.exports = {
  purge: ['./public/index.html', './src/**/*.tsx'],
  theme: {
    fontFamily: {
      sans: ['CUP_Infant', 'sans-serif', '-apple-system'],
    },
    cursor: {
      auto: 'auto',
      default: 'default',
      pointer: 'pointer',
      wait: 'wait',
      text: 'text',
      move: 'move',
      grab: 'grab',
      grabbing: 'grabbing',
    },
    extend: {
      borderRadius: {
        16: '16px',
      },
      colors: {
        black: 'var(--black)',
        white: 'var(--white)',
        primary: 'var(--primary)',
        'primary-dark': 'var(--primary-dark)',
        'primary-light': 'var(--primary-light)',
        'deep-blue-dark': 'var(--deep-blue-dark)',
        overlay: 'var(--overlay)',
        'type-dark': 'var(--type-dark)',
        'skin-tone-1': 'var(--skin-tone-1)',
        'skin-tone-2': 'var(--skin-tone-2)',
        'skin-tone-3': 'var(--skin-tone-3)',
        'skin-tone-4': 'var(--skin-tone-4)',
        'skin-tone-5': 'var(--skin-tone-5)',
        'skin-tone-6': 'var(--skin-tone-6)',
        'skin-tone-7': 'var(--skin-tone-7)',
        'skin-tone-8': 'var(--skin-tone-8)',
        'skin-tone-9': 'var(--skin-tone-9)',
        'skin-tone-10': 'var(--skin-tone-10)',
        'skin-tone-11': 'var(--skin-tone-11)',
        'skin-tone-12': 'var(--skin-tone-12)',
        'skin-tone-13': 'var(--skin-tone-13)',
        'skin-tone-14': 'var(--skin-tone-14)',
      },
      inset: {
        8: '2rem',
        28: '7rem',
      },
      spacing: {
        18: '4.5rem',
      },
      opacity: {
        12: '0.12',
        24: '0.24',
      },
    },
  },
  variants: {},
  plugins: [
    process.env.NODE_ENV === 'development'
      ? require('tailwindcss-debug-screens')
      : [],
  ],
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
};

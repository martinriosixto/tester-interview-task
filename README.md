# Digital Young Learners Web UI
[![Netlify Status](https://api.netlify.com/api/v1/badges/2753082b-5aa5-454a-99e4-b6d3e8b8fcab/deploy-status)](https://app.netlify.com/sites/dyl-ui/deploys)


This is the UI repository for the Digital Young Learners project. 

## Getting started

Install and then start!

    npm i
    npm start

or

    yarn
    yarn start

## Contributing

1.  Create a branch in the format `feature/${ISSUE_ID}`.
2.  Add some code.
3.  Commit with a message prefixed `"${ISSUE_ID}: ...your commit message..."`.
4.  Create a PR!

Need an `ISSUE_ID`? Speak to the PO or Scrum Master to get a ticket added to the backlog.

# Documentation

This is a living doc. It details how we're building the front-end / UI. Things will change as we go, but that's OK.

## Vocabulary

The common language around this product between Dev - PO - UX - Assessment (Content).

_A **Test** contains some **Tasks** which are made up of **Items**._

..which is Assessment speak for ...

\*An **Exam** contains some **Activities** which are made up of **Questions\***.

## Feature Dev

Starting points for any new feature.

**TL;DR**: Keep it simple, Talk lots.

> Fewer moving parts / less is more

We want to keep things simple for as long as we can - for libraries, tooling, modules - don't add until you really need to.

> Can you tell what it is yet?

Sketch it. Show it to the other devs. Get feedback. Sketch it again. Write an implementation anyone could debug || maintain || extend || test.

> It'll come out in the wash...

Like this doc, the code will polish itself as we go. Basically, we want to Avoid Hasty Abstractions.

> Keep data close to where it lives.

This product build is going to be pretty agile. Lots is going to change along the way, including state. Avoid the noise of drilling props and types. Keep the data next to the Component that's using it. We can consolidate it down the line.

> Start BIG & work from the outside, in.

Views and Components will be Atomic - once they're finalised. Dump all the code in one Component to start. Wait until some patterns emerge. Then make like a banana (and split\*).

\*_ Sorry_

## Stack & Tooling

This project was [built with](https://github.com/dance2die/cra-template-tailwindcss-typescript/tree/master/template) `cra-template-tailwindcss-typescript`.

Which is a combo of the official [CRA TS template](https://github.com/facebook/create-react-app/tree/master/packages/cra-template-typescript) `cra-template-typescript` and `cra-template-tailwindcss` a "[streamlined Tailwind CSS template](https://github.com/GeoffSelby/cra-template-tailwindcss)".

## Git Policy

- Direct commits to master are locked down
- For any ticket or new feature, use a feature branch in the format `feature/${ISSUE_ID}`
- ...for a bug, use `bug/${ISSUE_ID}`...
- ..for a Hotfix use `hotfix/${ISSUE_ID}` and so on, whatever makes it clearest
- We use a **Squash** policy
- Branches are merged to master via PR's
- Edit the PR title and message to be concise (remove commit comments) and meaningful
- ...eg. `"${ISSUE_ID}: Added / Removed / Fixed [some feature]"`
- Assign to a team member to review
- Get up and have a cuppa and a chat about any issues...
- Merge once you have an approval

## CI / CD

Ideally, Bitbucket and AWS would give us everything we need. But for the moment it doesn't spin up preview links, so we've chucked [Vercel](https://vercel.com/martinriosixto/dyl-web-ui) (formerly Zeit) into the mix. Current set up is as follows:

1.  Any commit to a feature branch triggers a [Bitbucket Pipeline](https://bitbucket.org/product/features/pipelines) that will lint and test your code
2.  Once you create a PR, Vercel will build and deploy the App and spin up a [preview link](https://dyl-web-ui.now.sh/)
3.  Preview links will be added to the PR as a comment for the Reviewer to preview your feature
4.  Any merge to `master` will trigger a new Bitbucket Pipeline which will build, test and deploy the App to an AWS S3 bucket.

## Domains

Our Dev instance is spun up by Vercel. Our Prev and Prod instances are hosted through AWS (speak to Dan for credentials) with Route 53 domains, and S3 buckets for hosting code and assets.

**Dev:** [dyl-web-ui.now.sh/](https://dyl-web-ui.now.sh/)
**Prev:** [preview-dyl.dnpd.cloud](https://s3.console.aws.amazon.com/s3/buckets/preview-dyl.dnpd.cloud?region=eu-west-2)
**Prod:** ...to follow

## Linting

We use the default lint configuration, which runs as a pre-commit hook and in our Bitbucket Pipeline (in case you're being cheeky) alongside a _really_ simple `.prettier.rc` [config](https://prettier.io/docs/en/configuration.html), ie.

    semi: true,
    singleQuote: true,

## Testing

> Coming soon.....

# Framework

We're all about [React](https://reactjs.org/) with [hooks](https://reactjs.org/docs/hooks-intro.html).

## Syntax

We're using TypeScript as a top-level container around standard JS/X in order to get all the benefits of types, interfaces and IntelliSense from VSCode. As a general approach, in the beginning:

- Keep your types in your components
- Don't extend too early
- ...

## Theming

We're using [tailwind.css](https://tailwindcss.com/) for the simplicity of a basket of utility classes in the first instance.

Later for the way it handles splitting up [components](https://tailwindcss.com/docs/extracting-components), [theming](https://tailwindcss.com/docs/theme) and the small bundle size. There is a little bit of a learning curve, but it's quick [to pick up](https://nerdcave.com/tailwind-cheat-sheet).

When we get to worrying about scoped CSS, we'll be using [css-modules](https://github.com/css-modules/css-modules).

## Routing

We're using the slightly experimental [v.6](https://reacttraining.com/blog/react-router-v6-pre/) of `react-router`. It's much simplified and quite a deviation from the previous versions, so have a dig [round online](https://alligator.io/react/react-router-v6/) (not much in the way of official docs atm).

## API Requests

Most likely [axios](https://github.com/axios/axios)

> Coming soon.....

## Global state

[@Redux/toolkit](https://redux-toolkit.js.org/) (Will add detail when / if we start using it...)

> Coming soon.....

## Components and Re-usability

We will be [atomic](https://atomicdesign.bradfrost.com/table-of-contents/) and we are using [storybook](https://storybook.js.org/), but don't be tempted to split up your code to soon.

> IMPORTANT!

**DON'T STRESS IT** it'll feel right when it's time to split the code.

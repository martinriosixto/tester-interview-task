import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';

import { AppProvider } from 'context/App';
import {
  BrowserRouter,
  Routes,
  Route,
  useLocation,
  Navigate,
} from 'react-router-dom';
import * as Sentry from '@sentry/react';

import { analytics } from 'lib';

import './styles/index.css';

import { AudioContextProvider } from 'context/Audio';

import Test from 'pages/Test';

import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import RotateDevice from 'components/RotateDevice';
import ValidateResolution from 'components/ValidateResolution';
import useResendAnswers from 'hooks/useResendAnswers';
import useResendAnalytics from 'hooks/useResendAnalytics';

Sentry.init({
  environment: process.env.REACT_APP_ENV,
  dsn: process.env.REACT_APP_SENTRY_DSN,
});

analytics.configureAmplify();

let lastLocation = '';
function FallbackComponent() {
  return <div>An error has occurred</div>;
}

const AppRoutes = () => {
  let location = useLocation();

  const { recordEvent } = useAnalytics();
  useResendAnswers();
  useResendAnalytics();

  useEffect(() => {
    if (location.pathname !== lastLocation) {
      recordEvent(ANALYTIC_EVENT.load_page);
      lastLocation = location.pathname;
    }
  }, [location, recordEvent]);

  return (
    <Routes>
      <Navigate to="/test/item/0" />
      <Route path="/test/*" element={<Test />} />
    </Routes>
  );
};

function App() {
  const APP_VERSION = process.env.REACT_APP_VERSION;
  Sentry.setTag('version', APP_VERSION);
  return (
    <Sentry.ErrorBoundary fallback={FallbackComponent}>
      <AppProvider>
        <RotateDevice />
        <ValidateResolution />
        <BrowserRouter>
          <AudioContextProvider>
            <AppRoutes />
          </AudioContextProvider>
        </BrowserRouter>
      </AppProvider>
      {APP_VERSION && (
        <span id="APP_VERSION" className="hidden" data-version={APP_VERSION} />
      )}
    </Sentry.ErrorBoundary>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));

import { mapItemToApi } from './index';
import { spelling_task_response, comic_story_task_response } from 'responses';

describe('When mapping request for the api', () => {
  it('Should be able to map minimal differences', () => {
    const userTask = {
      task_id: 'b66712d4-1771-48d4-82df-84308451638f',
      task_type_id: 'minimal_differences',
      items: [
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=09367a97c315e44d7ae3000553180d4d9e6818bbba78c989d4e7aabfc46af768&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c21ab95820c430a37315b36c177cb53346fd6c7258fe25f18869709287807f52&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: true,
          is_example: true,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_MD%20_Fch_Floor.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=643b146fb07dc5a34ce0531c2043989c7a9cfa4a2e873d8cc104904b1e93840a&X-Amz-SignedHeaders=host',
          },
          item_id: '49eea88f-5afa-48ee-9ae8-f42adfc8f5c2',
          answer_type: 'image',
          order: 0,
          answers: [
            {
              answer_id: '128c9c23-88ea-4603-b09a-3f50097593dd',
              is_correct_answer: false,
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c0ffb5772c5e032cfab9e15693aae96c3da02be62b772895174c4dcf6fc7797e&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '77dbb430-4047-4401-928e-542dc479cdf3',
              is_correct_answer: true,
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Floor.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=efd19c69f7a15f772e0e96499a2fd3c8e1844d1c16fc011435c10bdd1eaf5f10&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=09367a97c315e44d7ae3000553180d4d9e6818bbba78c989d4e7aabfc46af768&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c21ab95820c430a37315b36c177cb53346fd6c7258fe25f18869709287807f52&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_MD_Fch_Kitchen.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=6967d18b6d8f9221b088bb3f164f9b824302d69bbe1e4e416371e967e221a76a&X-Amz-SignedHeaders=host',
          },
          item_id: '41000eb0-33e6-4a03-a743-70191f11d2dd',
          answer_type: 'image',
          order: 1,
          answers: [
            {
              answer_id: '1a4a6b52-818b-44d3-ae1b-98a836cc3284',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Chicken.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=2d481504eb1da05fe20a329963a3c3e123fe12e6d9e62836cfcd5853ad259b36&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '407748ea-8678-4b9c-9f57-ea7fdecc1686',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Kitchen.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=dbf08c8473fb9aa20c779e8dc1ab550f35d57bd3a4868cde14c85e76f48c1097&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=09367a97c315e44d7ae3000553180d4d9e6818bbba78c989d4e7aabfc46af768&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c21ab95820c430a37315b36c177cb53346fd6c7258fe25f18869709287807f52&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_MD_Fch_Mat.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c45e02cea91154241c9a28ddf34610f9d1588381445e5e8a44e6d7097a221393&X-Amz-SignedHeaders=host',
          },
          item_id: '57e5c55c-5f80-4d7d-b7f0-38d3890e27ab',
          answer_type: 'image',
          order: 2,
          answers: [
            {
              answer_id: '5e0f413e-ab28-498c-b384-47337317e1a0',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Mat.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=f1ad3bab5c1b260a2a8236d869056dbf816b7881dd9759cf742869413fa924d6&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '96704e9c-da27-40ed-8b1c-45d6da7f35f9',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Hat.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=5cd8ce6a12f75f0191802ff9438cc1ffd0c222ca04ae65723ad2519c4cb7eda3&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=09367a97c315e44d7ae3000553180d4d9e6818bbba78c989d4e7aabfc46af768&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=c21ab95820c430a37315b36c177cb53346fd6c7258fe25f18869709287807f52&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_MD_Fch_Bed.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=4d6524d4fd8236da8cdacff4fd39dda6bb49c4c247dfcaec2b1f46b7b121bf1b&X-Amz-SignedHeaders=host',
          },
          item_id: '5d51185d-ba73-4dfe-bf86-ec19f42b73b2',
          answer_type: 'image',
          order: 3,
          answers: [
            {
              answer_id: '3fc87b56-8af9-4891-ba20-9711b73f6d7e',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Bed.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=6458129914d910cc1b37e452ec41d5c12939cce17fdeb206262dcf7a40730977&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'bff8d362-539f-44eb-b339-b3a58a679902',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Bird.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=a92e95b7b86915dd9f3bf66ab824e750f85440dad3fbec6542273c3d5962d6bb&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChooseWord.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=74581969a1ffe06b25a08b1c6d44d4aa518b5b2490b96032e8bbc5eaef584455&X-Amz-SignedHeaders=host',
            helper_audio_id: null,
            rubric_text: 'Now listen and choose the correct word.',
            helper_text: null,
          },
          show_rubric: true,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_%20UK_T1_MD%20_Fch_Global.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=99891c9b4bb06948fb77b3402261300404b59344f0134ca5eb372eb61d778251&X-Amz-SignedHeaders=host',
          },
          item_id: '3d45d352-1442-4f0b-a1c5-03b447be99f1',
          answer_type: 'text',
          order: 4,
          answers: [
            {
              answer_id: '148aac8c-6954-4545-8c56-3dd67206b84c',
              display_order: 2,
              text: 'Beach',
            },
            {
              answer_id: '20bc416e-1c17-4bcc-8218-97c30e537b59',
              display_order: 1,
              text: 'House',
            },
            {
              answer_id: 'c068b29f-f926-4756-90cc-6b580537a840',
              display_order: 3,
              text: 'School',
            },
          ],
        },
      ],
      test_by_user_id: '4e3e80ec-7efd-4fcf-bfab-1041b62b6bd0',
      task_by_user_id: '512e24ff-8f58-408c-baa9-4de0f9157c58',
    };
    const taskAnswers = [
      null,
      null,
      [
        {
          answer_id: '1a4a6b52-818b-44d3-ae1b-98a836cc3284',
          display_order: 1,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Chicken.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=2d481504eb1da05fe20a329963a3c3e123fe12e6d9e62836cfcd5853ad259b36&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: '96704e9c-da27-40ed-8b1c-45d6da7f35f9',
          display_order: 1,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Hat.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIEMXT4TAV%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T130847Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEUaCWV1LXdlc3QtMiJGMEQCIDi5x8YOVv3kkCOyfut%2Bqs3ANX9npuMA1z2skuP1%2F9%2FfAiB79V4YXvrHisBByVdyRDSmCfOIvOkouEUyx70DdQkaQSr1AQi%2B%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDkyODE3NTI1OTIxNiIMNJmqGRyc3J9aoWVXKskBXofgFjzP%2FbmSHWOa%2BbM1By1aZzpWD5nd4M26b1WSM0AXJU6f1mzqL4qCjlBhSOaNRT0F1nDwGK9LV2b3Tu7IYVIV3W0NhqZSlEhc5kIltcvcBdCZxXVIXEnWN7MLhqhryWGwlghLezkHCB2PoWvxQFSIDiVfxke3Rwdw%2FcxSC82Y%2B%2BV90KfjF7y3qfSizPKG6jF6RcAXnxzPwf4bLKJBI%2FgiJHLN7kNgIP3YITRCNTbpJqkAM5RMu5dmw%2BLcJPVwQRl9JQ9ycbyfMKmBo%2FcFOuEBIUea0aruRuOXZ5BbgwLJSBG663kKuyJzHJDGaLWuGehGw7c0rIDHVXOqUY%2B%2BhJBOWHuP62L1qWI1M%2BQcwc8Mhe8pUUDICU6kSxkiCGZX70UyfCbba6shG5yJsUJEUu%2B9Z60XrOl7c7%2FNHQ3qQBBZXdhOwaOGex6iar679OeNN94ZzajNSAuEC8vi28ERffG3FrG0GMOkV8dL0HET8Tg1vNGzjH5gKE9tbLYIHBaZ280HUBqb6Q9EaNQiDR5wWfR3pWjAwsFzGEOqy0ZaV7fFpN9keKjgewpJjv%2B7rZl2gtW%2B&X-Amz-Signature=5cd8ce6a12f75f0191802ff9438cc1ffd0c222ca04ae65723ad2519c4cb7eda3&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [],
    ];
    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      { answer_id: null, item_id: '49eea88f-5afa-48ee-9ae8-f42adfc8f5c2' },
      { answer_id: null, item_id: '41000eb0-33e6-4a03-a743-70191f11d2dd' },
      {
        answer_id: '1a4a6b52-818b-44d3-ae1b-98a836cc3284',
        item_id: '57e5c55c-5f80-4d7d-b7f0-38d3890e27ab',
      },
      {
        answer_id: '96704e9c-da27-40ed-8b1c-45d6da7f35f9',
        item_id: '5d51185d-ba73-4dfe-bf86-ec19f42b73b2',
      },
      { answer_id: null, item_id: '3d45d352-1442-4f0b-a1c5-03b447be99f1' },
    ]);
  });

  it('Should be able to map labelling', () => {
    const userTask = {
      task_id: 'a88315fb-9b3b-4fd0-ac23-0d8b504e6fe3',
      background_image: { src: null },
      task_type_id: 'labeling',
      items: [
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: true,
          is_example: true,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_Matt.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=fab75a01c35abe6dc2a038d613f9236ece7bb4f5446b0948890e1689b8988200&X-Amz-SignedHeaders=host',
          },
          item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
          stimulus_text_1: 'Name:',
          stimulus_text_2: '',
          order: 0,
          answers: [
            {
              answer_id: '002946d7-80b2-4e27-bfb5-c8731369c12a',
              text: 'O',
              order: 0,
              display_order: 2,
              is_correct_answer: false,
            },
            {
              answer_id: '210a1bb7-62d0-45fe-b489-a5f345a50bdc',
              text: 'T',
              order: 3,
              display_order: 1,
              is_correct_answer: true,
            },
            {
              answer_id: '2f4b5878-2762-4ee5-88fc-d82138ff1f26',
              text: 'A',
              order: 2,
              display_order: 8,
              is_correct_answer: true,
            },
            {
              answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
              text: 'P',
              order: 1,
              display_order: 4,
              is_correct_answer: true,
            },
            {
              answer_id: '6984bf1b-242a-42ec-8cac-6e802af35f73',
              text: 'X',
              order: 0,
              display_order: 9,
              is_correct_answer: false,
            },
            {
              answer_id: '7267c8b5-63dd-429c-be6f-5530b7f2858c',
              text: 'U',
              order: 0,
              display_order: 6,
              is_correct_answer: false,
            },
            {
              answer_id: '741db00e-bb9f-4942-b5db-2f10771e7c85',
              text: 'V',
              order: 0,
              display_order: 10,
              is_correct_answer: false,
            },
            {
              answer_id: '7ca9d6e5-60e0-4a9e-b5d9-fc90e29890cc',
              text: 'D',
              order: 0,
              display_order: 7,
              is_correct_answer: false,
            },
            {
              answer_id: 'a61f45f9-408f-48cb-8e43-1e993de1ae24',
              text: 'M',
              order: 0,
              display_order: 3,
              is_correct_answer: false,
            },
            {
              answer_id: 'c4494c17-5614-4b44-b1f6-cebaab1c4a90',
              text: 'R',
              order: 0,
              display_order: 5,
              is_correct_answer: false,
            },
          ],
          boxes: 3,
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_Shell.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=6eaba894cc645d7bd19f840cb3175a318fe7cb29a2b08f2e319bb2fb94b258d3&X-Amz-SignedHeaders=host',
          },
          item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1',
          stimulus_text_1: 'Lives in:',
          stimulus_text_2: 'Street',
          order: 1,
          answers: [
            {
              answer_id: '0ae75d51-4136-493c-b95f-5b8f2eb580f7',
              text: 'N',
              display_order: 6,
            },
            {
              answer_id: '558b113b-1110-4900-91a9-7ae615ee7ef1',
              text: 'E',
              display_order: 5,
            },
            {
              answer_id: '55cdd6db-a4ce-48b8-8b3c-b8a48133dd08',
              text: 'S',
              display_order: 3,
            },
            {
              answer_id: '603297e9-eac7-45d4-891a-4e150796ad52',
              text: 'L',
              display_order: 10,
            },
            {
              answer_id: '8812284d-fbf0-45e7-807c-89dfc0436db4',
              text: 'H',
              display_order: 1,
            },
            {
              answer_id: '9ffef502-185a-40cb-9666-988b9195965f',
              text: 'N',
              display_order: 8,
            },
            {
              answer_id: 'a5eb16d2-97db-43f1-96b2-f4864d03393d',
              text: 'L',
              display_order: 7,
            },
            {
              answer_id: 'bad2f876-58d3-43bd-846e-471d0674907d',
              text: 'T',
              display_order: 9,
            },
            {
              answer_id: 'cf9071ba-786c-4533-8b9c-85664a98a653',
              text: 'C',
              display_order: 2,
            },
            {
              answer_id: 'f139ca9e-a2c1-4028-9b17-8f8cb81b6c5b',
              text: 'I',
              display_order: 4,
            },
          ],
          boxes: 5,
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_House13.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=82afb25c693724416fc90e04453f3d42c55dc55799ef8d5fe6b9bda75b03737d&X-Amz-SignedHeaders=host',
          },
          item_id: 'e1db9139-9dee-4292-8b97-80c31e79c4ec',
          stimulus_text_1: 'House Number:',
          stimulus_text_2: '',
          order: 2,
          answers: [
            {
              answer_id: '01bc51de-a018-4d6f-95d7-3c3f413bbf0b',
              text: '4',
              display_order: 5,
            },
            {
              answer_id: '0a811d86-02e8-4583-ad90-cb804e626cb4',
              text: '2',
              display_order: 2,
            },
            {
              answer_id: '19279d97-8982-4bb4-9c76-1ca4255e98bf',
              text: '3',
              display_order: 3,
            },
            {
              answer_id: '1c839f09-9f97-47c0-bf6a-6adad8e9ba02',
              text: '1',
              display_order: 1,
            },
            {
              answer_id: '293bc3be-8f8b-43b8-a410-fe25fc5aad07',
              text: '0',
              display_order: 10,
            },
            {
              answer_id: '679e869a-baac-490d-8e04-0be353ef18f1',
              text: '7',
              display_order: 7,
            },
            {
              answer_id: 'b8dfb383-995c-4e6a-aebc-3c71eea7aff3',
              text: '6',
              display_order: 6,
            },
            {
              answer_id: 'c4b74ded-0626-450b-9863-f18500f1fa1e',
              text: '8',
              display_order: 8,
            },
            {
              answer_id: 'e6cadb9c-02e9-4ad7-b358-295634b18ed2',
              text: '5',
              display_order: 5,
            },
            {
              answer_id: 'f33cdd1a-dc79-458a-9da3-d8cc51f7784f',
              text: '9',
              display_order: 9,
            },
          ],
          boxes: 2,
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_8.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=1d18b7124448dadde6788d1a9bae33f746a4f6144538666c2dccff4802d120cb&X-Amz-SignedHeaders=host',
          },
          item_id: '5fd22c18-5dca-4c5a-91e5-14f40a826dce',
          stimulus_text_1: 'Number of toys:',
          stimulus_text_2: '',
          order: 3,
          answers: [
            {
              answer_id: '06389e7e-dc0d-41f1-ae0c-3375759553df',
              text: '2',
              display_order: 2,
            },
            {
              answer_id: '368909ea-61bd-4b35-81b7-50b475544198',
              text: '6',
              display_order: 6,
            },
            {
              answer_id: '4079db53-be8a-4c52-b0ad-a9aca02cbb5e',
              text: '5',
              display_order: 5,
            },
            {
              answer_id: '7eba5076-9639-4df4-8d73-217ad4edb0b6',
              text: '9',
              display_order: 9,
            },
            {
              answer_id: '93afa67a-c9ea-452e-abbc-779ae529185a',
              text: '3',
              display_order: 3,
            },
            {
              answer_id: 'b88bcbbc-afd9-447a-9837-44bb19c77b29',
              text: '1',
              display_order: 1,
            },
            {
              answer_id: 'c1d654f6-786f-426d-a730-2b0a64a05198',
              text: '4',
              display_order: 4,
            },
            {
              answer_id: 'd39a9332-69eb-4b56-b153-90eecd582326',
              text: '8',
              display_order: 8,
            },
            {
              answer_id: 'f7c3934a-bc22-42c6-8b10-d4b8b976565d',
              text: '7',
              display_order: 7,
            },
          ],
          boxes: 1,
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_Kiwi.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=c7fb9ecc1232519ee7ff78bc9cd8a5cc9a16af469b69215a70dec63a7c6d4f17&X-Amz-SignedHeaders=host',
          },
          item_id: 'dec2017e-8f3d-4410-9619-cd8d9dad7b84',
          stimulus_text_1: 'Name of alien:',
          stimulus_text_2: '',
          order: 4,
          answers: [
            {
              answer_id: '2118a21b-38b5-4666-8ea6-755acfa83860',
              text: 'Y',
              display_order: 7,
            },
            {
              answer_id: '30390790-fdf6-4840-b253-56ef9abeccfc',
              text: 'K',
              display_order: 1,
            },
            {
              answer_id: '413763fa-de61-44cd-b8e3-30125c9035b0',
              text: 'F',
              display_order: 8,
            },
            {
              answer_id: '4c7f59c1-9700-457b-a479-099ba7253464',
              text: 'U',
              display_order: 2,
            },
            {
              answer_id: '97834c40-338d-4133-b114-e2d85e1071b2',
              text: 'I',
              display_order: 4,
            },
            {
              answer_id: 'a02ec447-87d8-483d-a2c0-dd5869393fb7',
              text: 'I',
              display_order: 6,
            },
            {
              answer_id: 'c794f973-865d-4737-99dc-d67a3cc0a164',
              text: 'C',
              display_order: 5,
            },
            {
              answer_id: 'ea82efe4-41b4-47c1-987d-5ab97ae5de37',
              text: 'R',
              display_order: 9,
            },
            {
              answer_id: 'ee44c331-ddcc-4ae0-b44b-3f25c616965e',
              text: 'W',
              display_order: 3,
            },
          ],
          boxes: 4,
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricLettersNumbers.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=eac0c82374d0778a95b438d6c986051fe705e6f6e88bd2d7c93efdfcb8a93ec6&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_LettersBoxes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=f3c831190a8243ae6cdfa835db83fe35175aaad9209fee78fa11a4a749e57a24&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose letters or numbers.',
            helper_text: 'Put the letters in the boxes.',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_L_M_Fch_11Arms.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZICGDQBBTM%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T144158Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEcaCWV1LXdlc3QtMiJHMEUCIHzifFqq5adFQzccbs2%2FPPfQZpvyvE%2BDKX4k4g7Bf5NwAiEAnjqFqXQ7nQkDrobsYOBy%2FsLMb6g1WKBoQx%2BIpZm0CUAq9QEIwP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDFOE3U%2FiYlG5ogw0BCrJAU2ifuqayb3IlFzSjpsrrZWxmo7sxWfR1zii1N%2BTiBQpnLr7tP%2BztFMRXfTozJVX3YmbHKxyEjUWCiQ8DgDs9Mg%2FERYb7Kk9QmLfuAYhvs4686jBqGatmXi3ihwid4o3Rl1S0%2FT58FK3Er9KJt%2Fwn%2BHCvp%2FZDEnysbrR%2B8%2FbJk8bV6IYPgoJcH1IEXh%2F9gC0d8bJ2m2xoTkDNEeS0Z1DQmsVj79OCln74I7ltV6ZWpQAoYFuyzs%2BFxhxNehI%2FmBIhNJrAfnm%2F%2FUcRDCctKP3BTrgAQoo72IYIoNaq1HjGAfur8kqTNAUoxc8T%2BhPmM5CEmkPv2FunX4PPsHfKIqwNmNOHH%2FBqb%2B5rsjTk31KC8%2BGLJ1hhwu%2FOxViMt5rC9pMSFH1B2OZQbArF8UNOVfJJwLkq9SQ1J3KUTznenAYSlaM6cmi0ONuCvwyfioGAuGr1UAzBroav8FbRIYtfNREVWeOJfHsWRxMHvdpR5VHl8kOIWYx6I7eSxg4488Azb784Wi9rCnItCiGglSaCCveZ8CZyUop9eIYmpt5fe46mXmpBDqhcIvTW1SSfRBRowNbK%2FYi&X-Amz-Signature=337547cac3a4cf0f0d9eedcb20777e71a8fff247567456302372e5bc3c111117&X-Amz-SignedHeaders=host',
          },
          item_id: '25de05c7-9489-4e16-a5c6-c16823c3a1a3',
          stimulus_text_1: 'Number of alien arms:',
          stimulus_text_2: '',
          order: 5,
          answers: [
            {
              answer_id: '02a04ec8-8c1a-4ced-a4cf-9b968a89b381',
              text: '9',
              display_order: 9,
            },
            {
              answer_id: '063a378b-cb87-49d7-9d72-56eea148de9d',
              text: '7',
              display_order: 7,
            },
            {
              answer_id: '0ef84530-783e-4139-9421-9e9186211b12',
              text: '2',
              display_order: 2,
            },
            {
              answer_id: '30232fdb-5e6e-4272-825e-88b3d3e1d288',
              text: '8',
              display_order: 8,
            },
            {
              answer_id: '41071105-9cd7-4373-aa57-d80e45780bb2',
              text: '3',
              display_order: 3,
            },
            {
              answer_id: '958d7d89-0022-467e-a7fd-c209c63544ea',
              text: '1',
              display_order: 5,
            },
            {
              answer_id: 'ac1d8a53-62c2-454b-97fb-f544f7faa65d',
              text: '0',
              display_order: 10,
            },
            {
              answer_id: 'b6423f89-95d5-46f4-9d13-5ffd462b55cf',
              text: '6',
              display_order: 6,
            },
            {
              answer_id: 'b69baec5-70e6-46a1-9ad0-f5d42b2b2e52',
              text: '1',
              display_order: 1,
            },
            {
              answer_id: 'd4ebb8e4-5cc0-4754-8783-0df8626d8c2f',
              text: '4',
              display_order: 4,
            },
          ],
          boxes: 2,
        },
      ],
      test_by_user_id: '6621f1dc-97c3-4947-bf05-ab647e836604',
      task_by_user_id: 'baff3840-be0e-4c91-942b-6156316fc334',
    };

    const taskAnswers = [
      [
        {
          item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
          answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
        },
      ],
      [
        { item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1', answer_id: '' },
        {
          item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1',
          answer_id: '8812284d-fbf0-45e7-807c-89dfc0436db5',
        },
        { item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1', answer_id: '' },
        {
          item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1',
          answer_id: '8812284d-fbf0-45e7-807c-89dfc0436db4',
        },
      ],
      [
        { item_id: 'e1db9139-9dee-4292-8b97-80c31e79c4ec', answer_id: '' },
        {
          item_id: 'e1db9139-9dee-4292-8b97-80c31e79c4ec',
          answer_id: 'b8dfb383-995c-4e6a-aebc-3c71eea7aff3',
        },
      ],
      [
        {
          item_id: '5fd22c18-5dca-4c5a-91e5-14f40a826dce',
          answer_id: 'c1d654f6-786f-426d-a730-2b0a64a05198',
        },
      ],
      [
        { item_id: 'dec2017e-8f3d-4410-9619-cd8d9dad7b84', answer_id: '' },
        {
          item_id: 'dec2017e-8f3d-4410-9619-cd8d9dad7b84',
          answer_id: '8812284d-fbf0-45e7-807c-89dfc0436db1',
        },
        {
          item_id: 'dec2017e-8f3d-4410-9619-cd8d9dad7b84',
          answer_id: 'ee44c331-ddcc-4ae0-b44b-3f25c616965e',
        },
      ],
      [
        {
          item_id: '25de05c7-9489-4e16-a5c6-c16823c3a1a3',
          answer_id: 'ac1d8a53-62c2-454b-97fb-f544f7faa65d',
        },
      ],
    ];
    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      {
        answers: ['44d2ba66-9749-475c-85b5-deaf1877b90f'],
        item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
      },
      {
        answers: [
          '8812284d-fbf0-45e7-807c-89dfc0436db5',
          '8812284d-fbf0-45e7-807c-89dfc0436db4',
        ],
        item_id: 'eb21ac51-84eb-4fbd-8fca-6003f9732fe1',
      },
      {
        answers: ['b8dfb383-995c-4e6a-aebc-3c71eea7aff3'],
        item_id: 'e1db9139-9dee-4292-8b97-80c31e79c4ec',
      },
      {
        answers: ['c1d654f6-786f-426d-a730-2b0a64a05198'],
        item_id: '5fd22c18-5dca-4c5a-91e5-14f40a826dce',
      },
      {
        answers: [
          '8812284d-fbf0-45e7-807c-89dfc0436db1',
          'ee44c331-ddcc-4ae0-b44b-3f25c616965e',
        ],
        item_id: 'dec2017e-8f3d-4410-9619-cd8d9dad7b84',
      },
      {
        answers: ['ac1d8a53-62c2-454b-97fb-f544f7faa65d'],
        item_id: '25de05c7-9489-4e16-a5c6-c16823c3a1a3',
      },
    ]);
  });

  it('Should be able to map scene creation', () => {
    const userTask = {
      task: {
        task_id: 'b66712d4-1771-48d4-82df-84308451638g',
        task_type_id: 'labeling',
        status: 'open',
        task_by_user_id: 'x',
      },
      // task_id: 'a413c472-3bd9-429a-ba5b-54136193b2e1',
      background_image: {
        src: './9d9f953399ef848b7329e4bb846931ae.png',
      },
      task_type_id: 'scene_creation',
      items: [
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=84f5562c721a7c2828c7f322d9fa65ed1d53959522310794c4fea38647f2054e&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=c5fdd920f3af4697b8e8c46d1cbf271690f39d2c497c7ba7c7d87683282791bc&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: true,
          is_example: true,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_WhereShoes.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=aca710be82308e5d6b9d9b66f4488638e7498d2122eadd7e0d4ebff2a4348a64&X-Amz-SignedHeaders=host',
          },
          item_id: '4c82ad0a-2dfe-48f5-b990-2a02245abcf7',
          order: 0,
          answers: [
            {
              answer_id: '0fc118f2-c1c9-48b3-8d14-8a5a2dc61bcb',
              is_correct_answer: false,
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=82d709ce8491f282836cac6c66f62d6738bcddc89688b05d2cd77e9e2c85f164&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '7b3fceb3-6b37-4928-a1e5-186b90da6d4c',
              is_correct_answer: true,
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=82d709ce8491f282836cac6c66f62d6738bcddc89688b05d2cd77e9e2c85f164&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '825bc0d9-e613-40dd-8a26-11926500de92',
              is_correct_answer: false,
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=82d709ce8491f282836cac6c66f62d6738bcddc89688b05d2cd77e9e2c85f164&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=55980015516847f699fdd27d6988a78df95da007ff3c4f0901d018ea493bd5db&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=e58ed5f3429f61892a6d4ad5dea3e36bdaa26d376ac5f024d113f27de11a0456&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_TakeBus.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=a8e057c44f5d8c345765542ec0353ad08a799b2659c6f55f3bc0511a41d98d8d&X-Amz-SignedHeaders=host',
          },
          item_id: 'ce999cb4-d02e-4499-810b-7ffb52a20cbf',
          order: 1,
          answers: [
            {
              answer_id: '359e6480-9c1f-48a8-9bcb-ac71045a297e',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '778376bf-7339-4d4c-af1d-ab4f35898691',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'd4cebfb1-a966-40e8-b15d-8e180024f080',
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=55980015516847f699fdd27d6988a78df95da007ff3c4f0901d018ea493bd5db&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=e58ed5f3429f61892a6d4ad5dea3e36bdaa26d376ac5f024d113f27de11a0456&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_BehindTree.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f618ca645866588516d4a41bb31e94bbb6602a4830edea96f6d45460e9bcc445&X-Amz-SignedHeaders=host',
          },
          item_id: '4e01411c-c3ad-4ec7-b68e-330d765c5349',
          order: 2,
          answers: [
            {
              answer_id: '1e3b3e94-01fd-4a5f-8031-a5589bfe3d7c',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '56362aff-81f3-4a4a-939f-d27bc5ee9aef',
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '9ae75313-7ac1-4afb-b430-8f59057c520e',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=55980015516847f699fdd27d6988a78df95da007ff3c4f0901d018ea493bd5db&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=e58ed5f3429f61892a6d4ad5dea3e36bdaa26d376ac5f024d113f27de11a0456&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_FoodMonkeys.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=21363c5241af22ad35508bcfbe3b8826a5fdc2d1d63bd65aabda612a7b4049d1&X-Amz-SignedHeaders=host',
          },
          item_id: '358ca769-a7d2-46ca-a624-d9537240e0f9',
          order: 3,
          answers: [
            {
              answer_id: '08d0fbf8-ba7a-4c30-baa8-8a80418a0783',
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'ad77d06f-408e-4ae7-b2a2-ee78ba5e9ce4',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'c436c23a-a2b9-4631-9822-54a0e5ffed3b',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=55980015516847f699fdd27d6988a78df95da007ff3c4f0901d018ea493bd5db&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=e58ed5f3429f61892a6d4ad5dea3e36bdaa26d376ac5f024d113f27de11a0456&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_MonkeyCamera.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=65202ffa309c41703baf5b167f598005e812dd98e6e0b24c5830e8f4e6fa6fa9&X-Amz-SignedHeaders=host',
          },
          item_id: '7be1c250-c306-409a-b740-f8ee2f80003c',
          order: 4,
          answers: [
            {
              answer_id: '6dc4f992-a1af-4acf-b332-dc3d98c498e2',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: '8d92ffe5-d625-4238-b75f-d7574980686a',
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'c1e42f64-8bf7-4f2a-a2de-5906e32b6cac',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=55980015516847f699fdd27d6988a78df95da007ff3c4f0901d018ea493bd5db&X-Amz-SignedHeaders=host',
            helper_audio_id:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_ExampleChoosePicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=e58ed5f3429f61892a6d4ad5dea3e36bdaa26d376ac5f024d113f27de11a0456&X-Amz-SignedHeaders=host',
            rubric_text: 'Listen and choose the correct picture!',
            helper_text: 'Choose the correct picture!',
          },
          show_rubric: false,
          is_example: false,
          audio: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_SC_FNar_GoingHome.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=9e6fb20aeda64b2c8107b8e02147323f8a69339e39baea0c1227aafbb959f09e&X-Amz-SignedHeaders=host',
          },
          item_id: 'f4fa85f1-0dca-47bd-8a38-60076d168b52',
          order: 5,
          answers: [
            {
              answer_id: '7ba7575e-0868-4833-818b-42738de70e37',
              display_order: 1,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'a809cbd4-f7b1-4c35-bdcc-3a17bdcb5475',
              display_order: 3,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
            {
              answer_id: 'f21fb089-1441-402d-93a5-48c75ebb3b78',
              display_order: 2,
              image: {
                src:
                  'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
      ],
      test_by_user_id: 'c68ab1fc-ff7e-43cc-a794-13f590f2867d',
      task_by_user_id: 'c6a9e5fb-0be0-4a48-a479-bccd7a238327',
    };
    const taskAnswers = [
      [
        {
          answer_id: '7b3fceb3-6b37-4928-a1e5-186b90da6d4c',
          is_correct_answer: true,
          display_order: 3,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=82d709ce8491f282836cac6c66f62d6738bcddc89688b05d2cd77e9e2c85f164&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: '778376bf-7339-4d4c-af1d-ab4f35898691',
          display_order: 1,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: '56362aff-81f3-4a4a-939f-d27bc5ee9aef',
          display_order: 3,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: 'ad77d06f-408e-4ae7-b2a2-ee78ba5e9ce4',
          display_order: 1,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: '8d92ffe5-d625-4238-b75f-d7574980686a',
          display_order: 3,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
          },
        },
      ],
      [
        {
          answer_id: 'a809cbd4-f7b1-4c35-bdcc-3a17bdcb5475',
          display_order: 3,
          image: {
            src:
              'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153942Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=f578abe2d2bc38be776d1f4a9421ac717a1a8777ba2df5aea98e11e5e13aa4e0&X-Amz-SignedHeaders=host',
          },
        },
      ],
    ];

    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      {
        item_id: '4c82ad0a-2dfe-48f5-b990-2a02245abcf7',
        answer_id: '7b3fceb3-6b37-4928-a1e5-186b90da6d4c',
      },
      {
        item_id: 'ce999cb4-d02e-4499-810b-7ffb52a20cbf',
        answer_id: '778376bf-7339-4d4c-af1d-ab4f35898691',
      },
      {
        item_id: '4e01411c-c3ad-4ec7-b68e-330d765c5349',
        answer_id: '56362aff-81f3-4a4a-939f-d27bc5ee9aef',
      },
      {
        item_id: '358ca769-a7d2-46ca-a624-d9537240e0f9',
        answer_id: 'ad77d06f-408e-4ae7-b2a2-ee78ba5e9ce4',
      },
      {
        item_id: '7be1c250-c306-409a-b740-f8ee2f80003c',
        answer_id: '8d92ffe5-d625-4238-b75f-d7574980686a',
      },
      {
        item_id: 'f4fa85f1-0dca-47bd-8a38-60076d168b52',
        answer_id: 'a809cbd4-f7b1-4c35-bdcc-3a17bdcb5475',
      },
    ]);
  });

  it('Should be able to map picture editing', () => {
    const userTask = {
      task_id: '11eb72d5-38e7-4fd8-a457-7b6d22d02fb7',
      is_published: true,
      background_image: {
        src:
          'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/picture_editing_playground.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103055Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=8a24ac035338bdeb141b51a3de871ea26e7baec268d110ec0af377f46e756867&X-Amz-SignedHeaders=host',
      },
      task_type_id: 'picture_editing',
      colors: [
        '#FF0000',
        '#009DFF',
        '#00FF0A',
        '#FFF500',
        '#FF9300',
        '#9D00FF',
        '#FA58F4',
      ],
      items: [
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: true,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_AddBook.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=ac6414cdb92524eed9cffd961905922f712c5bc7e54c35760b0d6d17d30d1e2d&X-Amz-SignedHeaders=host',
          },
          item_id: 'fe82eda9-2188-4b97-bf15-cfa54edb9f13',
          order: 0,
          phase: 'PHASE_1',
          is_example: true,
          answers: [
            {
              answer_id: 'dc21bee8-14e3-4e3a-bc3a-85a5439a9492',
              order: 1,
              coordinates: ['cc10', 'cc11', 'dd10', 'dd11', 'ee10', 'ee11'],
              is_correct_answer: true,
              image: {
                src:
                  'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/peddex_book.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=1ca01d50c052d0476a96788d7f3c39430ba462fa3d114034160eb553c1020f7e&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_AddKite.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=52e887518e8cfa0cb98bfdded8c0604c0efa457d494aae0b9ef79b7f5b4db330&X-Amz-SignedHeaders=host',
          },
          item_id: '230b170f-5aea-4ffb-9484-dfac3d863706',
          order: 1,
          phase: 'PHASE_1',
          is_example: false,
          answers: [
            {
              answer_id: '27b3616b-fe45-4373-b845-5e8c87dffcf9',
              order: 2,
              image: {
                src:
                  'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd1_kite.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=85683edc369c6dfdbd1c588c9212dfb58fcc5d63cf8847763f4fb83343fd5591&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_AddDuck.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=463d245699d6ff06bb7651c2c8556abf9a777fd04c8d3acd7fc49c81904a2d6f&X-Amz-SignedHeaders=host',
          },
          item_id: 'c75ac0d6-dd1b-4038-a8a3-6991c2dfe308',
          order: 2,
          phase: 'PHASE_1',
          is_example: false,
          answers: [
            {
              answer_id: 'd7f772c7-e197-49c0-aa87-22b602986e9c',
              order: 3,
              image: {
                src:
                  'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd2_duck.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=df1fcd5ce4b8059954a3a78b3b3bf8ad8e96cf9eba53e23b69ee1b3ad90c7491&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_AddCap.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=91a5fcc9c97749e981bd2445192838ff8087aee22f9dbba4aec256ff137e3768&X-Amz-SignedHeaders=host',
          },
          item_id: '07c5411c-72c0-49e1-9022-ff355a955c09',
          order: 3,
          phase: 'PHASE_1',
          is_example: false,
          answers: [
            {
              answer_id: '0bd32a88-bc11-4ef3-a9db-391d6a1bab5b',
              order: 4,
              image: {
                src:
                  'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd3_cap.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=34d1fd102be8294456a82e3514c4442774c6e458ff4183c05393d85806bbb4d9&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src: null,
          },
          item_id: '7d8aaf11-6370-4ec3-b4a9-db7e8d2f8a6e',
          order: 4,
          phase: 'PHASE_1',
          is_example: false,
          answers: [
            {
              answer_id: '77ef6544-c71d-4006-9eab-9a21676fd486',
              order: 5,
              image: {
                src:
                  'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd4_dog.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=bb3be9f98d0f6d5f4793b4501379cdc62a486d4b336e8e4f83489b88df9e1b44&X-Amz-SignedHeaders=host',
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Now colour some things.',
            helper_text: 'Add some colour.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_Rubric2.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=6dcb4efcef75b59d299f16b5e888d1325df77ddf064151335108ae128b3b91d3&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction2.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=a0ee7490a7198856947d5a635f91dd17b106eec7388db1084abdb1a6ddaed3f8&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: true,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_ColourSkateboard.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=48d50a5a1ca24e41eca99120d00ed9125a97ca41b569ce25d088057f60f63287&X-Amz-SignedHeaders=host',
          },
          item_id: 'c886ae2b-166f-4892-8036-3ad4fc327dba',
          order: 0,
          phase: 'PHASE_2',
          is_example: true,
          answers: [
            {
              answer_id: '2aaf200f-d303-4542-bf5b-f6d5e7f0307b',
              order: 1,
              coordinates: [
                'dd4',
                'dd5',
                'dd6',
                'dd7',
                'dd8',
                'ee3',
                'ee4',
                'ee5',
                'ee6',
                'ee7',
                'ee8',
                'ee9',
                'ff3',
                'ff4',
                'ff5',
                'ff6',
                'ff7',
                'ff8',
                'ff9',
              ],
              color_hex_value: '#009DFF',
              is_correct_answer: true,
              image: {
                src: null,
              },
            },
            {
              answer_id: '9f9baf27-b7d8-4b3f-99ac-70ea263fa9fe',
              order: 2,
              coordinates: [
                'ff27',
                'ff28',
                'ff29',
                'ff30',
                'ff31',
                'ff32',
                'ff33',
                'gg27',
                'gg28',
                'gg29',
                'gg30',
                'gg31',
                'gg32',
                'gg33',
                'hhh27',
                'hhh28',
                'hhh29',
                'hhh30',
                'hhh31',
                'hhh32',
                'hhh33',
              ],
              is_correct_answer: false,
              image: {
                src: null,
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_ColourFruit.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=a531a35c4e4b1814366327db4ea4c4460e77a1e99d7906d404599261dd5784d0&X-Amz-SignedHeaders=host',
          },
          item_id: 'e64164e6-addd-424d-beaa-cb9f38cfd2f5',
          order: 1,
          phase: 'PHASE_2',
          is_example: false,
          answers: [
            {
              answer_id: '31019218-b162-42be-904f-c4e1a261fbd6',
              order: 3,
              coordinates: [
                'jj6',
                'jj7',
                'jj8',
                'kk6',
                'kk7',
                'kk8',
                'll6',
                'll7',
                'll8',
              ],
              image: {
                src: null,
              },
            },
            {
              answer_id: '78bdc4c4-9cb0-4e7f-8593-de72c7805086',
              order: 4,
              coordinates: [
                'l13',
                'l14',
                'l15',
                'l16',
                'm13',
                'm14',
                'm15',
                'm16',
                'n13',
                'n14',
                'n15',
                'n16',
              ],
              image: {
                src: null,
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_ColourPlane.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=b7d0b1e857308efed38a311cc83a27e6f7fb932e8aac0185b971dc96f8aeb7e0&X-Amz-SignedHeaders=host',
          },
          item_id: '22ebdc22-f850-4b0f-a0ce-3390ab93d074',
          order: 2,
          phase: 'PHASE_2',
          is_example: false,
          answers: [
            {
              answer_id: 'c8149a19-fd00-49ba-a285-a3c41fcce69b',
              order: 5,
              coordinates: [
                'pp20',
                'pp21',
                'pp22',
                'pp23',
                'pp24',
                'pp25',
                'pp26',
                'pp27',
                'pp28',
                'pp29',
                'pp30',
                'qq20',
                'qq21',
                'qq22',
                'qq23',
                'qq24',
                'qq25',
                'qq26',
                'qq27',
                'qq28',
                'qq29',
                'qq30',
                'rr20',
                'rr21',
                'rr22',
                'rr23',
                'rr24',
                'rr25',
                'rr26',
                'rr27',
                'rr28',
                'rr29',
                'rr30',
                'ss25',
                'ss26',
                'ss27',
                'ss28',
              ],
              image: {
                src: null,
              },
            },
            {
              answer_id: 'ef3b41db-9cf6-46a3-bbaa-cabd22212a23',
              order: 6,
              coordinates: [
                'f30',
                'f31',
                'f32',
                'g25',
                'g26',
                'g27',
                'g28',
                'g29',
                'g30',
                'g31',
                'g32',
                'hh25',
                'hh26',
                'hh27',
                'hh28',
                'hh29',
                'hh30',
                'hh31',
                'hh32',
                'i25',
                'i26',
                'i27',
                'i28',
                'i29',
                'i30',
                'i31',
                'i32',
                'j25',
                'j26',
                'j27',
                'j28',
                'j29',
                'j30',
                'k25',
                'k26',
                'k27',
                'k28',
                'k29',
              ],
              image: {
                src: null,
              },
            },
          ],
        },
        {
          rubric: {
            rubric_text: 'Listen and put some things in the picture.',
            helper_text: 'Put the book in the picture.',
            rubric_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_FNar_RubricPutPicture.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=96d74f787250c02df52f55231e53814715404e5452fde82b239626303ddb6064&X-Amz-SignedHeaders=host',
            },
            helper_audio: {
              src:
                'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/UK_PE_FNar_ExampleInstruction1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=cd5e9ee09f3c90560096f04b508247d432fc202640e40f5ab07e8be3238bf1a0&X-Amz-SignedHeaders=host',
            },
          },
          show_rubric: false,
          audio: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/sounds/PreA1_UK_T1_PE_F_Mch_ColourBag.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=22029bbeaa70bc32a5b896dfd4ea96661c1f5748c0451fa779aece8e5b4cf898&X-Amz-SignedHeaders=host',
          },
          item_id: 'a6336d1f-53b7-4866-a7d8-72bdba1bdcaa',
          order: 3,
          phase: 'PHASE_2',
          is_example: false,
          answers: [
            {
              answer_id: '4630c0cf-8332-41e7-b8ee-0233e713def9',
              order: 7,
              coordinates: [
                'oo38',
                'oo39',
                'oo40',
                'oo41',
                'oo42',
                'oo43',
                'pp38',
                'pp39',
                'pp40',
                'pp41',
                'pp42',
                'pp43',
                'qq38',
                'qq39',
                'qq40',
                'qq41',
                'qq42',
                'qq43',
                'rr38',
                'rr39',
                'rr40',
                'rr41',
                'rr42',
                'rr43',
                'ss38',
                'ss39',
                'ss40',
                'ss41',
                'ss42',
                'ss43',
                'tt38',
                'tt39',
                'tt40',
                'tt41',
                'tt42',
                'tt43',
                'uu38',
                'uu39',
                'uu40',
                'uu41',
                'uu42',
                'uu43',
              ],
              image: {
                src: null,
              },
            },
            {
              answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
              order: 8,
              coordinates: [
                'cc15',
                'cc16',
                'cc17',
                'cc18',
                'cc19',
                'dd15',
                'dd16',
                'dd17',
                'dd18',
                'dd19',
                'ee15',
                'ee16',
                'ee17',
                'ee18',
                'ee19',
                'ff15',
                'ff16',
                'ff17',
                'ff18',
                'ff19',
                'gg15',
                'gg16',
                'gg17',
                'gg18',
                'gg19',
                'hhh15',
                'hhh16',
                'hhh17',
                'hhh18',
                'hhh19',
                'ii15',
                'ii16',
                'ii17',
                'ii18',
                'ii19',
              ],
              image: {
                src: null,
              },
            },
          ],
        },
      ],
    };
    const taskAnswers = [
      [
        {
          answer_id: 'dc21bee8-14e3-4e3a-bc3a-85a5439a9492',
          order: 1,
          coordinates: ['cc10', 'cc11', 'dd10', 'dd11', 'ee10', 'ee11'],
          is_correct_answer: true,
          image: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/peddex_book.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=1ca01d50c052d0476a96788d7f3c39430ba462fa3d114034160eb553c1020f7e&X-Amz-SignedHeaders=host',
          },
          type: 'Box',
          coordinate: { left: 225.5, top: 95 },
        },
        {
          answer_id: '27b3616b-fe45-4373-b845-5e8c87dffcf9',
          order: 2,
          image: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd1_kite.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=85683edc369c6dfdbd1c588c9212dfb58fcc5d63cf8847763f4fb83343fd5591&X-Amz-SignedHeaders=host',
          },
          type: 'Box',
          coordinate: { left: 449.5, top: 79 },
        },
        {
          answer_id: 'd7f772c7-e197-49c0-aa87-22b602986e9c',
          order: 3,
          image: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd2_duck.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=df1fcd5ce4b8059954a3a78b3b3bf8ad8e96cf9eba53e23b69ee1b3ad90c7491&X-Amz-SignedHeaders=host',
          },
          type: 'Box',
          coordinate: { left: 113.5, top: 287 },
        },
        {
          answer_id: '0bd32a88-bc11-4ef3-a9db-391d6a1bab5b',
          order: 4,
          image: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd3_cap.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=34d1fd102be8294456a82e3514c4442774c6e458ff4183c05393d85806bbb4d9&X-Amz-SignedHeaders=host',
          },
          type: 'Box',
          coordinate: { left: 321.5, top: 527 },
        },
        {
          answer_id: '77ef6544-c71d-4006-9eab-9a21676fd486',
          order: 5,
          image: {
            src:
              'https://dev-dyl-assets-bucket-f1f79c10.s3.eu-west-2.amazonaws.com/images/pedd4_dog.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIBUUIFE4C%2F20200805%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200805T103054Z&X-Amz-Expires=86400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJHMEUCIQDQFgSG0jM9EDoDoECEkiaH%2FxT751%2BZMk14BeYRLKqMIgIgUsqCeVIkw%2FGZtXSp%2Frdp7y9uTwHhn3%2BW0jegmKQzR5cq5gEIu%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNAVXpCY6c3GVAHjOiq6AYaZVu2FkItSzM204JJlub%2BDn1qosoua9mE6UNEeF98J5JojzTD76zSaL15omHk7qrDbPZxVlKrbUdl%2BH4Ud4KRNfyj9Z7A0uTQoXgOflh35YJYvnJklGoWtcjOZ5xrBWJkQzBRk7CCDaACuvcq98RkJifIB%2B%2FEWC9b5ek7TS1I332YVCUPJJ5tg%2FEkgw%2FdLlVUXUjK2dHo5pkVIWBJdad70anYycLZ6Uy5yiWJK7FeAfJGIo3Mj76rN2DC2iar5BTrgAS7YcjblpPwMhKEvo4TNiEU5ZSZ%2FzUDvuDcKlZFQvFBEs947tgctxmfgHnnma7hEiXBLkdThDMAuxiFn%2Fux5UCg%2Fv6GCK7R1Q3t7fRsSo47w43b9WTXm%2FzbG6nCKeUrfbdhcA%2FJ3EbovyBf8OsWujE30gqrZCz3JGEhHVwf3sOLUM9GVeAvhzrEUyOIUdc7flmUw1PSNKM3Nk9%2FPT8m1TuDZ90By1qmAGKUhmQwV%2FDYyvzxb4%2Fccgmk1eD6g6ZkPdbVMtIGQO5Hqh0aJFChhcH0psXMvae7fUp7Kv3iwzfdi&X-Amz-Signature=bb3be9f98d0f6d5f4793b4501379cdc62a486d4b336e8e4f83489b88df9e1b44&X-Amz-SignedHeaders=host',
          },
          type: 'Box',
          coordinate: { left: 385.5, top: 271 },
        },
        {
          answer_id: '2aaf200f-d303-4542-bf5b-f6d5e7f0307b',
          order: 1,
          coordinates: [
            'dd4',
            'dd5',
            'dd6',
            'dd7',
            'dd8',
            'ee3',
            'ee4',
            'ee5',
            'ee6',
            'ee7',
            'ee8',
            'ee9',
            'ff3',
            'ff4',
            'ff5',
            'ff6',
            'ff7',
            'ff8',
            'ff9',
          ],
          color_hex_value: '#009DFF',
          is_correct_answer: true,
          image: { src: null },
        },
        null,
        {
          answer_id: '9f9baf27-b7d8-4b3f-99ac-70ea263fa9fe',
          order: 2,
          coordinates: [
            'ff27',
            'ff28',
            'ff29',
            'ff30',
            'ff31',
            'ff32',
            'ff33',
            'gg27',
            'gg28',
            'gg29',
            'gg30',
            'gg31',
            'gg32',
            'gg33',
            'hhh27',
            'hhh28',
            'hhh29',
            'hhh30',
            'hhh31',
            'hhh32',
            'hhh33',
          ],
          is_correct_answer: false,
          image: { src: null },
          color_hex_value: '#9D00FF',
        },
        null,
        {
          answer_id: '78bdc4c4-9cb0-4e7f-8593-de72c7805086',
          order: 4,
          coordinates: [
            'l13',
            'l14',
            'l15',
            'l16',
            'm13',
            'm14',
            'm15',
            'm16',
            'n13',
            'n14',
            'n15',
            'n16',
          ],
          image: { src: null },
          color_hex_value: '#FF0000',
        },
        {
          answer_id: 'c8149a19-fd00-49ba-a285-a3c41fcce69b',
          order: 5,
          coordinates: [
            'pp20',
            'pp21',
            'pp22',
            'pp23',
            'pp24',
            'pp25',
            'pp26',
            'pp27',
            'pp28',
            'pp29',
            'pp30',
            'qq20',
            'qq21',
            'qq22',
            'qq23',
            'qq24',
            'qq25',
            'qq26',
            'qq27',
            'qq28',
            'qq29',
            'qq30',
            'rr20',
            'rr21',
            'rr22',
            'rr23',
            'rr24',
            'rr25',
            'rr26',
            'rr27',
            'rr28',
            'rr29',
            'rr30',
            'ss25',
            'ss26',
            'ss27',
            'ss28',
          ],
          image: { src: null },
          color_hex_value: '#FF9300',
        },
        {
          answer_id: 'ef3b41db-9cf6-46a3-bbaa-cabd22212a23',
          order: 6,
          coordinates: [
            'f30',
            'f31',
            'f32',
            'g25',
            'g26',
            'g27',
            'g28',
            'g29',
            'g30',
            'g31',
            'g32',
            'hh25',
            'hh26',
            'hh27',
            'hh28',
            'hh29',
            'hh30',
            'hh31',
            'hh32',
            'i25',
            'i26',
            'i27',
            'i28',
            'i29',
            'i30',
            'i31',
            'i32',
            'j25',
            'j26',
            'j27',
            'j28',
            'j29',
            'j30',
            'k25',
            'k26',
            'k27',
            'k28',
            'k29',
          ],
          image: { src: null },
          color_hex_value: '#009DFF',
        },
        null,
        {
          answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
          order: 8,
          coordinates: [
            'cc15',
            'cc16',
            'cc17',
            'cc18',
            'cc19',
            'dd15',
            'dd16',
            'dd17',
            'dd18',
            'dd19',
            'ee15',
            'ee16',
            'ee17',
            'ee18',
            'ee19',
            'ff15',
            'ff16',
            'ff17',
            'ff18',
            'ff19',
            'gg15',
            'gg16',
            'gg17',
            'gg18',
            'gg19',
            'hhh15',
            'hhh16',
            'hhh17',
            'hhh18',
            'hhh19',
            'ii15',
            'ii16',
            'ii17',
            'ii18',
            'ii19',
          ],
          image: { src: null },
          color_hex_value: '#00FF0A',
        },
      ],
    ];
    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      { coordinate: 'f15', item_id: 'fe82eda9-2188-4b97-bf15-cfa54edb9f13' },
      { coordinate: 'e29', item_id: '230b170f-5aea-4ffb-9484-dfac3d863706' },
      { coordinate: 'r8', item_id: 'c75ac0d6-dd1b-4038-a8a3-6991c2dfe308' },
      { coordinate: 'gg21', item_id: '07c5411c-72c0-49e1-9022-ff355a955c09' },
      { coordinate: 'q25', item_id: '7d8aaf11-6370-4ec3-b4a9-db7e8d2f8a6e' },
      {
        answers: [
          {
            answer_id: '2aaf200f-d303-4542-bf5b-f6d5e7f0307b',
            color_hex_value: '#009DFF',
          },
          {
            answer_id: '9f9baf27-b7d8-4b3f-99ac-70ea263fa9fe',
            color_hex_value: '#9D00FF',
          },
        ],
        item_id: 'c886ae2b-166f-4892-8036-3ad4fc327dba',
      },
      {
        answers: [
          {
            answer_id: '31019218-b162-42be-904f-c4e1a261fbd6',
            color_hex_value: null,
          },
          {
            answer_id: '78bdc4c4-9cb0-4e7f-8593-de72c7805086',
            color_hex_value: '#FF0000',
          },
        ],
        item_id: 'e64164e6-addd-424d-beaa-cb9f38cfd2f5',
      },
      {
        answers: [
          {
            answer_id: 'c8149a19-fd00-49ba-a285-a3c41fcce69b',
            color_hex_value: '#FF9300',
          },
          {
            answer_id: 'ef3b41db-9cf6-46a3-bbaa-cabd22212a23',
            color_hex_value: '#009DFF',
          },
        ],
        item_id: '22ebdc22-f850-4b0f-a0ce-3390ab93d074',
      },
      {
        answers: [
          {
            answer_id: '4630c0cf-8332-41e7-b8ee-0233e713def9',
            color_hex_value: null,
          },
          {
            answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
            color_hex_value: '#00FF0A',
          },
        ],
        item_id: 'a6336d1f-53b7-4866-a7d8-72bdba1bdcaa',
      },
    ]);
  });

  it('Should be able to map spelling', () => {
    const userTask = spelling_task_response;
    const taskAnswers = [
      [{ ...userTask.items[0].answers[0], user_answer_string: 'pen' }],
      [{ ...userTask.items[1].answers[0], user_answer_string: null }],
      [{ ...userTask.items[2].answers[0] }],
      [{ ...userTask.items[3].answers[0], user_answer_string: 'owl' }],
      [{ ...userTask.items[4].answers[0], user_answer_string: 'ink' }],
      [{ ...userTask.items[5].answers[0], user_answer_string: 's' }],
    ];
    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      {
        answer_id: userTask.items[0].answers[0].answer_id,
        item_id: userTask.items[0].item_id,
        user_answer_string: taskAnswers[0][0].user_answer_string,
      },
      {
        answer_id: userTask.items[1].answers[0].answer_id,
        item_id: userTask.items[1].item_id,
        user_answer_string: taskAnswers[1][0].user_answer_string,
      },
      {
        answer_id: userTask.items[2].answers[0].answer_id,
        item_id: userTask.items[2].item_id,
        user_answer_string: taskAnswers[2][0].user_answer_string,
      },
      {
        answer_id: userTask.items[3].answers[0].answer_id,
        item_id: userTask.items[3].item_id,
        user_answer_string: taskAnswers[3][0].user_answer_string,
      },
      {
        answer_id: userTask.items[4].answers[0].answer_id,
        item_id: userTask.items[4].item_id,
        user_answer_string: taskAnswers[4][0].user_answer_string,
      },
      {
        answer_id: userTask.items[5].answers[0].answer_id,
        item_id: userTask.items[5].item_id,
        user_answer_string: taskAnswers[5][0].user_answer_string,
      },
    ]);
  });

  it('Should be able to map comic story', () => {
    const userTask = comic_story_task_response;
    const taskAnswers = [
      [{ ...userTask.items[0].answers[0], user_answer_string: 'pen' }],
      [{ ...userTask.items[1].answers[0], user_answer_string: null }],
      [{ ...userTask.items[2].answers[0] }],
      [{ ...userTask.items[3].answers[0], user_answer_string: 'owl' }],
      [{ ...userTask.items[4].answers[0], user_answer_string: 'ink' }],
      [{ ...userTask.items[5].answers[0], user_answer_string: 's' }],
    ];
    // @ts-ignore
    const result = mapItemToApi(userTask, taskAnswers);
    expect(result).toEqual([
      {
        answer_id: userTask.items[0].answers[0].answer_id,
        item_id: userTask.items[0].item_id,
        user_answer_string: taskAnswers[0][0].user_answer_string,
      },
      {
        answer_id: userTask.items[1].answers[0].answer_id,
        item_id: userTask.items[1].item_id,
        user_answer_string: taskAnswers[1][0].user_answer_string,
      },
      {
        answer_id: userTask.items[2].answers[0].answer_id,
        item_id: userTask.items[2].item_id,
        user_answer_string: taskAnswers[2][0].user_answer_string,
      },
      {
        answer_id: userTask.items[3].answers[0].answer_id,
        item_id: userTask.items[3].item_id,
        user_answer_string: taskAnswers[3][0].user_answer_string,
      },
      {
        answer_id: userTask.items[4].answers[0].answer_id,
        item_id: userTask.items[4].item_id,
        user_answer_string: taskAnswers[4][0].user_answer_string,
      },
      {
        answer_id: userTask.items[5].answers[0].answer_id,
        item_id: userTask.items[5].item_id,
        user_answer_string: taskAnswers[5][0].user_answer_string,
      },
    ]);
  });
});

import { FactualTopicItem, PictureEditingItem } from 'types';
import { UserTask, ItemAnswer, Item } from 'types';
import { gridCoordinates } from 'lib';

interface ItemPayloadForAPI {
  item_id: string;
  coordinate?: string | null;
  answers?:
    | (
        | string
        | undefined
        | { answer_id: string; color_hex_value: string | null }
      )[]
    | null;
  answer_id?: string | null;
}

function mapSingleAnswerForItem(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
) {
  return userTask.items.map((item, index) => {
    const userAnswer = taskAnswers[index] || [];

    return {
      item_id: item.item_id,
      answer_id:
        userAnswer.length && userAnswer[0] ? userAnswer[0].answer_id : null,
    };
  });
}

function mapTextInputAnswersForItem(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
) {
  return userTask.items.map((item, index) => {
    const userAnswer = taskAnswers[index] || [];

    return {
      item_id: item.item_id,
      answer_id:
        userAnswer.length && userAnswer[0] ? userAnswer[0].answer_id : null,
      user_answer_string: userAnswer[0]
        ? userAnswer[0].user_answer_string
        : null,
    };
  });
}

function mapMultipleAnswersForItem(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
) {
  return userTask.items.map((item, index) => {
    const userAnswers = (taskAnswers[index] || []).filter(
      (a) => a?.answer_id.length
    );

    return {
      item_id: item.item_id,
      answers: userAnswers[0]
        ? userAnswers.map((userAnswer) => userAnswer?.answer_id)
        : null,
    };
  });
}

function mapPictureEditingAnswersForItem(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
) {
  // for picture editing everything is in the first item
  const _taskAnswers = taskAnswers[0] || [];

  const phase1Items = ([
    ...userTask.items,
    ...(userTask.itemz || []),
  ] as PictureEditingItem[]).filter((i) => i.phase === 'PHASE_1');

  const phase2Items = (userTask.items as PictureEditingItem[]).filter(
    (i) => i.phase === 'PHASE_2'
  );

  const phase1Answers = phase1Items.map((item) => {
    // each phase1 has a single answer
    const itemAnswerId = item.answers[0].answer_id;
    const userAnswerForAnswerId = _taskAnswers.find(
      (a) => a?.answer_id === itemAnswerId
    );

    return {
      item_id: item.item_id,
      coordinate: userAnswerForAnswerId?.coordinate
        ? gridCoordinates.calculate(
            userAnswerForAnswerId?.coordinate.left,
            userAnswerForAnswerId?.coordinate.top
          )
        : null,
    };
  });

  const phase2Answers = phase2Items.map((item) => {
    const answers = item.answers.map((answer) => {
      const userAnswerForAnswerId = _taskAnswers.find(
        (a) => a?.answer_id === answer.answer_id
      );
      return {
        answer_id: answer.answer_id,
        color_hex_value: userAnswerForAnswerId?.color_hex_value || null,
      };
    });

    return {
      item_id: item.item_id,
      answers,
    };
  });

  return [...phase1Answers, ...phase2Answers];
}

function mapFactualTopicAnswersForItem(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
) {
  // for factual topic phase 1, three items are in the  first item
  const _taskAnswers = [...taskAnswers] || [];
  const phase1Items = ([
    ...userTask.items,
    ...(userTask.itemz || []),
  ] as FactualTopicItem[]).filter((i) => i.phase === 'PHASE_1');

  const phase2Items = (userTask.items as FactualTopicItem[]).filter(
    (i) => i.phase === 'PHASE_2'
  );

  //For phase one multiple items are put together in answer for item 1
  const relevantPhase1ItemAnswers = phase1Items
    .map((item) => {
      return item.answers;
    })
    .flat(1);

  const phase1Answers = relevantPhase1ItemAnswers.map((item) => {
    const userAnswerForItemId = _taskAnswers
      .flat(1)
      .find((a) => a?.item_id === item.item_id);
    return {
      item_id: !item.item_id ? '' : item.item_id,
      text: userAnswerForItemId ? userAnswerForItemId.text : null,
    };
  });

  const phase2Answers = phase2Items.map((item) => {
    const filteredAnswers = _taskAnswers
      .flat(1)
      .filter((a) => a?.item_id === item.item_id)
      .map((a) => {
        return a?.answer_id;
      });
    return {
      item_id: item.item_id,
      answers: filteredAnswers ? filteredAnswers : null,
    };
  });

  return [...phase1Answers, ...phase2Answers];
}

export function mapItemToApi(
  userTask: UserTask<Item>,
  taskAnswers: (ItemAnswer | null)[][]
): ItemPayloadForAPI[] | ItemPayloadForAPI[][] {
  switch (userTask.task_type_id) {
    case 'labeling':
      return mapMultipleAnswersForItem(userTask, taskAnswers);
    case 'picture_editing':
      return mapPictureEditingAnswersForItem(userTask, taskAnswers);
    case 'spelling':
    case 'comic_story':
      return mapTextInputAnswersForItem(userTask, taskAnswers);
    case 'factual_topic':
      return mapFactualTopicAnswersForItem(userTask, taskAnswers);
    default:
      return mapSingleAnswerForItem(userTask, taskAnswers);
  }
}

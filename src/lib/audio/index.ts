import { UserTask, Item } from 'types';

function getItemsAudioLinks(items: Item[] = []) {
  return items.reduce((list: string[], item) => {
    if (item.audio?.src) {
      list.push(item.audio.src);
    }
    if (item.rubric?.rubric_audio.src) {
      list.push(item.rubric?.rubric_audio.src);
    }
    if (item.rubric?.helper_audio.src) {
      list.push(item.rubric?.helper_audio.src);
    }
    const answerAudioFiles: string[] = item.answers.reduce(
      (answerAudios: string[], answer) => {
        if (answer.audio?.src) {
          answerAudios.push(answer.audio?.src);
        }
        return answerAudios;
      },
      []
    );
    return list.concat(answerAudioFiles);
  }, []);
}

export function getAudioLinks(
  userTask: UserTask<Item>[] | UserTask<Item>
): string[] {
  return (Array.isArray(userTask) ? userTask : [userTask]).reduce(
    (audios: string[], userTask) => {
      const itemAudioFiles = getItemsAudioLinks(userTask.items);
      const itemzAudioFiles = getItemsAudioLinks(userTask.itemz);
      return audios.concat([...itemAudioFiles, ...itemzAudioFiles]);
    },
    []
  );
}

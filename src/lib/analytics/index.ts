import { LabellingItem } from './../../types/index.d';
import { Item, ItemAnswers, TaskItem, TaskTypeId } from 'types';
import {
  Amplify,
  Analytics,
  AWSKinesisFirehoseProvider,
  Auth,
} from 'aws-amplify';
import { gridCoordinates } from 'lib';

export interface AnalyticsEvent {
  user_id: string;
  event_name: string;
  timestamp: number;
  screen_slug: string;
  [extra: string]: any;
}

export interface AnalyticsRecorder {
  record: (analyticsEvent: AnalyticsEvent) => void;
}

const config = {
  Auth: {
    region: process.env.REACT_APP_AWS_REGION,
    userPoolId: process.env.REACT_APP_AWS_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_AWS_WEB_CLIENT_ID,
    identityPoolId: process.env.REACT_APP_AWS_IDENTITY_POOL_ID,
    identityPoolRegion: process.env.REACT_APP_AWS_REGION,
  },
  Analytics: {
    AWSKinesisFirehose: {
      // OPTIONAL -  Amazon Kinesis Firehose service region
      region: process.env.REACT_APP_AWS_REGION,

      // OPTIONAL - The buffer size for events in number of items.
      bufferSize: 1000,

      // OPTIONAL - The number of events to be deleted from the buffer when flushed.
      flushSize: 100,

      // OPTIONAL - The interval in milliseconds to perform a buffer check and flush if necessary.
      flushInterval: 5000, // 5s

      // OPTIONAL - The limit for failed recording retries.
      resendLimit: 5,
    },
  },
};

export const consoleRecorder: AnalyticsRecorder = {
  record: (analyticsEvent: AnalyticsEvent) => {
    console.table(analyticsEvent);
  },
};

export const amplifyRecorder: AnalyticsRecorder = {
  record: (analyticsEvent: AnalyticsEvent) => {
    Analytics.record(
      {
        data: JSON.stringify(analyticsEvent) + '\n',
        streamName: `${process.env.REACT_APP_ENV}-dyl-firehose`,
      },
      'AWSKinesisFirehose'
    );
  },
};

let recorders: AnalyticsRecorder[] = [];

if (process.env.REACT_APP_ANALYTICS_CONSOLE === 'true') {
  recorders.push(consoleRecorder);
}
if (process.env.REACT_APP_ANALYTICS_AMPLIFY === 'true') {
  recorders.push(amplifyRecorder);
}

export const recordEvent = (event: AnalyticsEvent) => {
  recorders.forEach((recorder) => {
    recorder.record(event);
  });
};

export const getItemAnalyticInfo = (item: Item) => {
  return {
    item_id: item.item_id,
    item_order: item.order,
    is_example: item.is_example,
  };
};

export const getAnswerAnalyticsInfo = ({
  item,
  answers,
  task_type_id,
}: {
  item: TaskItem;
  answers: ItemAnswers;
  task_type_id: TaskTypeId;
}) => {
  let answer_id = answers.map((a) => a?.answer_id).join(',');

  switch (task_type_id) {
    case 'picture_editing':
      const isColouring = answers.some((a) => a?.color_hex_value);
      if (isColouring) {
        return {
          answer_id,
          answer_colour: answers.map((a) => a?.color_hex_value).join(','),
        };
      }
      return {
        answer_id,
        answer_drop_coords:
          !isColouring &&
          answers
            .map((a) =>
              a?.coordinate
                ? gridCoordinates.calculate(
                    a?.coordinate.left,
                    a?.coordinate.top
                  )
                : undefined
            )
            .join(','),
      };
    case 'labeling':
      const answer_ids = [];
      const answer_texts = [];

      for (let index = 0; index < (item as LabellingItem).boxes + 1; index++) {
        const userAnswer = item.answers.find(
          (ans) => ans?.answer_id === answers[index]?.answer_id
        );
        answer_ids.push(userAnswer ? userAnswer.answer_id : '');
        answer_texts.push(userAnswer?.text || '');
      }

      return {
        answer_id: answer_ids.join(','),
        answer_text: answer_texts.join(','),
      };
    case 'dialogue':
      const [answer_order] = answers.map((a) => a?.order);
      return {
        answer_id,
        answer_order,
      };
    case 'spelling':
    case 'comic_story':
      const user_answer_string = answers
        .map((a) => a?.user_answer_string)
        .join('');
      return {
        answer_id,
        user_answer_string,
      };
    default:
      const answer_text = answers.map((a) => a?.text).join(',') ?? null;
      return {
        answer_id,
        ...(answer_text && { answer_text }),
      };
  }
};

export function configureAmplify(
  analyticsRecorders: AnalyticsRecorder[] = recorders
) {
  Amplify.configure(config);
  Amplify.register(Auth);
  Analytics.addPluggable(
    new AWSKinesisFirehoseProvider({ region: process.env.REACT_APP_ENV })
  );
  recorders = analyticsRecorders;
}

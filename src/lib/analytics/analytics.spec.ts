import { getAnswerAnalyticsInfo } from './index';
import { labeling_task_response } from 'responses';
import { TaskItem } from 'types';
// only the labelling task response is required as it's the only one used when mapping analytics

describe('When getting analytics info', () => {
  it('should be able to map picture editing colouring answers', () => {
    const answers = [
      {
        answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
        color_hex_value: '#00FF0A',
      },
    ];
    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'picture_editing',
    });
    expect(result).toEqual({
      answer_colour: '#00FF0A',
      answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
    });
  });

  it('should be able to map picture editing drag and drop', () => {
    const answers = [
      {
        answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
        coordinate: { left: 225.5, top: 95 },
      },
    ];
    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'picture_editing',
    });

    expect(result).toEqual({
      answer_drop_coords: 'f15',
      answer_id: '9443d444-7c11-49dc-8068-b16b446e2e23',
    });
  });

  it('should be able to map scene creation', () => {
    const answers = [
      {
        answer_id: '7b3fceb3-6b37-4928-a1e5-186b90da6d4c',
        is_correct_answer: true,
        display_order: 3,
        image: {
          src:
            'https://dev-dyl-assets.s3.eu-west-2.amazonaws.com/images/PreA1_T1_MD_Door.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA5QG4OPZIDW4U3DCK%2F20200616%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200616T153941Z&X-Amz-Expires=1800&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEEgaCWV1LXdlc3QtMiJHMEUCIB2JCCWDpbkLGSOx4l%2B6zQ%2BcXI80PrY0tfDCnw%2FoJL7rAiEAnAmXFNtLjT80IdmHYZH14vZf6KQJxDsupBeJNkq2wpIq9QEIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw5MjgxNzUyNTkyMTYiDNCAo5ex7Xevrn7r5yrJAWtSrYkL6FMFDpHLCTKvVhDjHl%2F8vzRN834IJ2mgqHG%2Bv4JYRzu%2BHFJ%2F6HdH%2B8viy0zJ6BEyxHvokjQzBMcvqDXhLokE30RHlt%2F%2BNUMjFa6vEZxECPsHzyqKVDVzgn02GrutxSgny8WDrmnQeXdoZQr5WuiI55ePRPSux1dXMyTvBMRxs76YVsFcw3wCQSwG4JXgF%2FFVkenxSfisSeBcu5BR24RItfjQJW4jlfA6fVMIvtABJD%2FLG64VeClPgmtZqgCmBHcjB%2B9KkjD%2ByKP3BTrgAVxL%2FE4mt0xDDLFEizDTtuH8LDIt1ojqS%2Fiz6%2B3eno1VXtatFq%2BSmK8%2FnZFk4N0CJdirjUZcP4snPH4p7BQ43LFn6mGvI4GXAgDEsVZWEBmtX%2FSqIME9I%2B3XS6cHUE0lg5y3Go4VYJ9Kg%2FYJC5LialYj2VQ5V34xgtfwnjRFk%2BFv4yV7ssroT%2BE10TtpMHIyubdB69K9a1UDQN9KuF%2FWySCTbEE1ZvT2OU9v8t2hwbCdJVdAJH2mYkMYdT7wq9pydQeiHh5UAM4iMencIOGvoUC9NLj6PPIEGjDSsCXc9Fzn&X-Amz-Signature=82d709ce8491f282836cac6c66f62d6738bcddc89688b05d2cd77e9e2c85f164&X-Amz-SignedHeaders=host',
        },
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'scene_creation',
    });

    expect(result).toEqual({
      answer_id: '7b3fceb3-6b37-4928-a1e5-186b90da6d4c',
    });
  });

  it('should be able to map spelling', () => {
    const answers = [
      {
        item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
        answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
        user_answer_string: 'pen',
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'spelling',
    });

    expect(result).toEqual({
      answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
      user_answer_string: 'pen',
    });
  });

  it('should be able to map comic story', () => {
    const answers = [
      {
        item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
        answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
        user_answer_string: 'pen',
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'comic_story',
    });

    expect(result).toEqual({
      answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
      user_answer_string: 'pen',
    });
  });

  it('should be able to map minimal differences', () => {
    const answers = [
      {
        item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
        answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'minimal_differences',
    });

    expect(result).toEqual({
      answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
    });
  });

  it('should be able to map labelling', () => {
    const firstLabellingItem = labeling_task_response.items[1] as TaskItem;

    const answers = [
      {
        item_id: firstLabellingItem.item_id,
        answer_id: firstLabellingItem.answers[0].answer_id,
      },
      {
        item_id: firstLabellingItem.item_id,
        answer_id: '',
      },
      { item_id: firstLabellingItem.item_id, answer_id: '' },
      {
        item_id: firstLabellingItem.item_id,
        answer_id: firstLabellingItem.answers[2].answer_id,
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: firstLabellingItem,
      answers,
      task_type_id: 'labeling',
    });

    expect(result).toEqual({
      answer_id: `${firstLabellingItem.answers[0].answer_id},,,${firstLabellingItem.answers[2].answer_id}`,
      answer_text: 'h,,,a',
    });
  });

  it('should be able to map dialogue', () => {
    const answers = [
      {
        item_id: 'b6ccd2d3-b599-4e9c-8318-ea66e0e86a77',
        answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
      },
    ];

    const result = getAnswerAnalyticsInfo({
      item: labeling_task_response.items[1],
      answers,
      task_type_id: 'dialogue',
    });

    expect(result).toEqual({
      answer_id: '44d2ba66-9749-475c-85b5-deaf1877b90f',
      answer_order: undefined,
    });
  });
});

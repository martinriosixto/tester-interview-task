import * as Sentry from '@sentry/react';
const API_URL = process.env.REACT_APP_API_URL;

interface Response<T> {
  data?: T;
  error?: any;
}

export async function http<T>(
  url: string,
  args: any,
  tag = ' API'
): Promise<Response<T>> {
  let _error: string = `API ERROR: ${url}`;
  try {
    const fetchResult = await fetch(new Request(url, args));
    _error = `${_error}${url} - Status: ${fetchResult.status} ${fetchResult.statusText}. `;
    if (fetchResult.ok) {
      const data: T = await fetchResult.json();
      return { data };
    }
  } catch (error) {
    _error = `${_error}${error}`;
    Sentry.withScope((scope) => {
      scope.setTag('error', 'api');
      scope.setTag('error.api', tag);
      scope.setContext('API', { args });
      Sentry.captureException(new Error(_error));
    });
  }
  return { error: _error };
}

export async function get<T>(
  path: string,
  args: RequestInit = {
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  },
  tag?: string
): Promise<Response<T>> {
  const result = await http<T>(`${API_URL}/${path}`, args, tag);
  return result;
}

interface Header {
  [key: string]: string;
}

export async function post<T>(
  path: string,
  {
    body,
    headers,
  }: {
    body?: any;
    headers?: Header;
  },
  tag?: string
): Promise<Response<T>> {
  const args: RequestInit = {
    method: 'post',
    body: body ? JSON.stringify(body) : undefined,
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
  };
  const result = await http<T>(`${API_URL}/${path}`, args, tag);
  return result;
}

export async function put<T>(
  path: string,
  {
    body,
    headers,
  }: {
    body?: any;
    headers?: Header;
  },
  tag?: string
): Promise<Response<T>> {
  const args: RequestInit = {
    method: 'put',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
  };
  const result = await http<T>(`${API_URL}/${path}`, args, tag);
  return result;
}

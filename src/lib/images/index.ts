import { UserTask, Item } from 'types';

function getItemsImagesImageLinks(items: Item[] = []) {
  return items.reduce((list: string[], item) => {
    const answerImageFiles: string[] = item.answers.reduce(
      (answerImages: string[], answer) => {
        if (answer.image?.src) {
          answerImages.push(answer.image.src);
        }
        return answerImages;
      },
      []
    );
    return list.concat(answerImageFiles);
  }, []);
}

export function getImageLinks(
  userTask: UserTask<Item>[] | UserTask<Item>
): string[] {
  return (Array.isArray(userTask) ? userTask : [userTask]).reduce(
    (images: string[], userTask) => {
      if (userTask.background_image?.src) {
        images.push(userTask.background_image?.src);
      }
      const itemImageFiles = getItemsImagesImageLinks(userTask.items);
      const itemzImageFiles = getItemsImagesImageLinks(userTask.itemz);
      return images.concat([...itemImageFiles, ...itemzImageFiles]);
    },
    []
  );
}

export function loadImages(
  files: string[],
  onAllLoaded: () => void,
  downloadError: () => void
) {
  let foundError = false;
  let numLoading = files.length;

  const onload = () => --numLoading === 0 && onAllLoaded();
  const onerror = () => {
    if (!foundError) {
      foundError = true;
      downloadError();
    }
  };
  const images: Record<string, HTMLImageElement> = {};

  for (let i = 0; i <= files.length - 1; i++) {
    const img = (images[files[i]] = new Image());
    img.src = files[i];
    img.onload = onload;
    img.onerror = onerror;
  }
  return images;
}

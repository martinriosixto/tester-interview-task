import * as api from './api';
import * as itemCreator from './itemCreator';
import * as apiMappers from './apiMappers';
import * as analytics from './analytics';
import * as gridCoordinates from './gridCoordinates';
import { TouchBackend } from 'react-dnd-touch-backend';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { XYCoord } from 'react-dnd';
import * as audio from './audio';
import * as images from './images';
import { SkillId } from 'types';
export {
  api,
  itemCreator,
  apiMappers,
  gridCoordinates,
  audio,
  analytics,
  images,
};

const MP3_REGEX = new RegExp('http(?:s)?://(?=.*).+.mp3', 'i');

export function getMp3FilenameFromUrl(url: string = '') {
  const matches = url.match(MP3_REGEX);
  return matches?.length ? matches[0] : undefined;
}

export const getUrlPrefix =
  process.env.REACT_APP_ENV === 'productioncn' ||
  process.env.REACT_APP_ENV === 'production'
    ? `/${new Date().toLocaleDateString('en-GB')}`
    : '';

export function isNumber(n: any) {
  return !isNaN(parseFloat(n)) && !isNaN(n - 0);
}

export function isTouchDevice() {
  return typeof window !== 'undefined' && 'ontouchstart' in window;
}

export const backendForDND = isTouchDevice() ? TouchBackend : HTML5Backend;

export function getItemStyles(
  initialOffset: XYCoord | null,
  currentOffset: XYCoord | null
) {
  if (!initialOffset || !currentOffset) {
    return {
      display: 'none',
    };
  }

  const { x, y } = currentOffset;

  const transform = `translate(${x}px, ${y}px)`;
  return {
    transform,
  };
}

export const classNames = (classes: string[]) =>
  classes.filter((el) => el).join(' ');

export const sleep = (time: number) => {
  return new Promise((resolve) => setTimeout(resolve, time));
};

export const getAllTasksWithItems = async (
  user_id: string,
  skill_id: SkillId,
  debug: boolean = false,
  token_id: string
) => {
  const getTestsResult = await api.getTests({
    user_id,
    skill_id,
    token_id,
  });

  if (!getTestsResult.data || getTestsResult.error) {
    return null;
  }

  const testForSkill = getTestsResult.data.find((t) => {
    return t.skill_id === skill_id;
  });

  if (!testForSkill) {
    return null;
  }

  let { test_by_user_id } = testForSkill;

  const getTasksResult = await api.getTasks({
    user_id,
    test_by_user_id,
  });

  if (!getTasksResult.data || getTasksResult.error) {
    return null;
  }

  const promises = getTasksResult.data.map(async (t) => {
    const result = await api.getTaskItem({
      user_id,
      test_by_user_id,
      task_by_user_id: t.task_by_user_id,
      debug,
    });
    return {
      userTask: result.data,
      task: t,
    };
  });

  const userTasks = await Promise.all(promises);

  return {
    test_by_user_id,
    userTasks,
  };
};

export const getAllTasks = async (
  user_id: string,
  skill_id: SkillId,
  token_id: string
) => {
  const getTestsResult = await api.getTests({
    user_id,
    skill_id,
    token_id,
  });

  if (!getTestsResult.data || getTestsResult.error) {
    throw new Error(getTestsResult.error);
  }

  const testForSkill = getTestsResult.data.find((t) => {
    return t.skill_id === skill_id;
  });

  if (!testForSkill) {
    throw new Error('Error creating test for skill');
  }

  let { test_by_user_id } = testForSkill;

  const getTasksResult = await api.getTasks({
    user_id,
    test_by_user_id,
  });

  if (!getTasksResult.data || getTasksResult.error) {
    throw new Error(getTestsResult.error);
  }

  // const promises = getTasksResult.data.map(async (t) => {
  //   const result = await api.getTaskItem({
  //     user_id,
  //     test_by_user_id,
  //     task_by_user_id: t.task_by_user_id,
  //   });
  //   return {
  //     userTask: result.data,
  //     task: t,
  //   };
  // });

  // const userTasks = await Promise.all(promises);

  return {
    test_by_user_id,
    tasks: getTasksResult.data,
  };
};

import { ItemStep, Item, TaskTypeId, FactualTopicItem } from 'types';
const TRANSITION_TIME = 2000;

export const STEP_DEFAULTS = {
  itemStepShow: undefined,
  transitionTime: undefined,
  canSelectAnswer: true,
  navVisible: true,
  showProgress: true,
  bringProgressForward: false,
  overlayVisible: false,
  answersOverlayVisible: false,
  showRubricHelpButton: true,
};

const SHOW_BACKGROUND: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_BACKGROUND',
  canSelectAnswer: false,
  transitionTime: TRANSITION_TIME,
  navVisible: false,
};

const SHOW_ANSWERS_ON_BACKGROUND: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_ANSWERS_ON_BACKGROUND',
  canSelectAnswer: false,
  showProgress: true,
  showRubricHelpButton: false,
  transitionTime: TRANSITION_TIME,
};

const SHOW_ANSWERS: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_ANSWERS',
  canSelectAnswer: false,
  transitionTime: TRANSITION_TIME,
  navVisible: false,
};

const SHOW_CUSTOM_NAVIGATION: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_CUSTOM_NAVIGATION',
  canSelectAnswer: true,
  transitionTime: TRANSITION_TIME,
};

const SHOW_NAVIGATION: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_NAVIGATION',
  canSelectAnswer: false,
  transitionTime: TRANSITION_TIME,
};

const SHOW_RECORD_BUTTON: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_RECORD_BUTTON',
};

const SHOW_QUESTION: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_QUESTION',
  canSelectAnswer: false,
  transitionTime: 1280,
};

const SHOW_STIMULUS: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_STIMULUS',
  canSelectAnswer: false,
  transitionTime: 1280,
};

const SHOW_ASSETS: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_ASSETS',
  canSelectAnswer: false,
  transitionTime: TRANSITION_TIME,
};

const SHOW_ANSWERS_TOGGLE: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_ANSWERS_TOGGLE',
};

const SHOW_OVERLAY: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_OVERLAY',
  canSelectAnswer: false,
  showProgress: true,
  showRubricHelpButton: false,
  overlayVisible: true,
  transitionTime: TRANSITION_TIME,
};

const HIDE_OVERLAY: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'HIDE_OVERLAY',
  canSelectAnswer: false,
  transitionTime: TRANSITION_TIME,
};

const HIDE_STAR_OVERLAY: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'HIDE_STAR_OVERLAY',
  canSelectAnswer: false,
  transitionTime: 1000,
  showProgress: true,
  showRubricHelpButton: false,
  bringProgressForward: true,
};

const SHOW_RUBRIC: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_RUBRIC',
  canSelectAnswer: false,
};

const SHOW_RUBRIC_WITH_OVERLAY: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_RUBRIC_WITH_OVERLAY',
  canSelectAnswer: false,
  overlayVisible: true,
};

const CYCLE_THRU_ITEM_ANSWERS: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'CYCLE_THRU_ITEM_ANSWERS',
  canSelectAnswer: false,
};

const SHOW_EXAMPLE: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_EXAMPLE',
  canSelectAnswer: false,
  overlayVisible: true,
};

const SHOW_HELPER_ANIMATION: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_HELPER_ANIMATION',
  canSelectAnswer: true,
};

const SHOW_CUSTOM_HELPER: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_CUSTOM_HELPER',
  canSelectAnswer: true,
};

const PLAY_AUDIO_CANNOT_SELECT: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'PLAY_AUDIO_CANNOT_SELECT',
  canSelectAnswer: false,
};

const USER_SELECT_FIRST_TIME: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'USER_SELECT_FIRST_TIME',
};

const CUSTOM_EXAMPLE_USER_SELECT_FIRST_TIME: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'CUSTOM_EXAMPLE_USER_SELECT_FIRST_TIME',
};

const CUSTOM_USER_SELECT_FIRST_TIME: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'CUSTOM_USER_SELECT_FIRST_TIME',
  transitionTime: 10000,
};

const SHOW_LISTEN_AGAIN: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_LISTEN_AGAIN',
};

const PLAY_AUDIO_CAN_SELECT: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'PLAY_AUDIO_CAN_SELECT',
};

const PLAY_LISTEN_AGAIN_AUDIO: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'PLAY_LISTEN_AGAIN_AUDIO',
};

const WAIT_FOR_EXAMPLE_TO_BE_COMPLETED: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'WAIT_FOR_EXAMPLE_TO_BE_COMPLETED',
};

const WAIT_TO_SHOW_FINISH: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'WAIT_TO_SHOW_FINISH',
};

const SHOW_FINISH_BUTTON: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_FINISH_BUTTON',
};

const POST_FINISH_STEP: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'POST_FINISH_STEP',
};

const SHOW_STAR: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_STAR',
  canSelectAnswer: false,
  showProgress: true,
  showRubricHelpButton: false,
  bringProgressForward: true,
  overlayVisible: true,
  transitionTime: TRANSITION_TIME + 1000,
};

const SHOW_STAR_FOR_INTRO: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_STAR_FOR_INTRO',
  canSelectAnswer: false,
  showProgress: true,
  bringProgressForward: true,
  overlayVisible: true,
};

//THIS IS JUST PLACEHOLDER for read write specific actions
const SHOW_INPUTS_SPECIFIC: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_INPUTS_SPECIFIC',
};

const SHOW_KEYBOARD: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'SHOW_KEYBOARD',
};

const DROP_OPACITY_WRONG_ANSWERS: ItemStep = {
  ...STEP_DEFAULTS,
  id: 'DROP_OPACITY_WRONG_ANSWERS',
};

function createStepWithOverrides(step: ItemStep, overrides: Object) {
  return { ...step, ...overrides };
}

export function createLabelingItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_ANSWERS,
      SHOW_NAVIGATION,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      PLAY_AUDIO_CANNOT_SELECT,
      SHOW_HELPER_ANIMATION,
      WAIT_FOR_EXAMPLE_TO_BE_COMPLETED,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  if (item.rubric && item.show_rubric) {
    return [
      SHOW_ANSWERS,
      SHOW_RUBRIC,
      PLAY_AUDIO_CAN_SELECT,
      SHOW_LISTEN_AGAIN,
      PLAY_LISTEN_AGAIN_AUDIO,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_ANSWERS,
    PLAY_AUDIO_CAN_SELECT,
    USER_SELECT_FIRST_TIME,
    SHOW_LISTEN_AGAIN,
    PLAY_LISTEN_AGAIN_AUDIO,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createMinimalDifferencesItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_ANSWERS,
      SHOW_NAVIGATION,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      PLAY_AUDIO_CANNOT_SELECT,
      SHOW_HELPER_ANIMATION,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  if (!item.is_example && item.rubric && item.show_rubric) {
    return [
      SHOW_ANSWERS,
      SHOW_NAVIGATION,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      HIDE_OVERLAY,
      PLAY_AUDIO_CANNOT_SELECT,
      USER_SELECT_FIRST_TIME,
      SHOW_LISTEN_AGAIN,
      PLAY_LISTEN_AGAIN_AUDIO,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_ANSWERS,
    SHOW_NAVIGATION,
    PLAY_AUDIO_CANNOT_SELECT,
    USER_SELECT_FIRST_TIME,
    SHOW_LISTEN_AGAIN,
    PLAY_LISTEN_AGAIN_AUDIO,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createPictureEditingItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_ANSWERS,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      PLAY_AUDIO_CANNOT_SELECT,
      SHOW_CUSTOM_HELPER,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  if (item.rubric && item.show_rubric) {
    return [
      SHOW_ANSWERS,
      SHOW_RUBRIC,
      PLAY_AUDIO_CANNOT_SELECT,
      USER_SELECT_FIRST_TIME,
      SHOW_LISTEN_AGAIN,
      PLAY_LISTEN_AGAIN_AUDIO,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_ANSWERS,
    PLAY_AUDIO_CAN_SELECT,
    USER_SELECT_FIRST_TIME,
    SHOW_LISTEN_AGAIN,
    PLAY_LISTEN_AGAIN_AUDIO,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createDialogueItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_ANSWERS,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      CYCLE_THRU_ITEM_ANSWERS,
      CUSTOM_EXAMPLE_USER_SELECT_FIRST_TIME,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_ANSWERS,
    CYCLE_THRU_ITEM_ANSWERS,
    CUSTOM_USER_SELECT_FIRST_TIME,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createSceneCreationItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_ANSWERS,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      PLAY_AUDIO_CANNOT_SELECT,
      SHOW_HELPER_ANIMATION,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  if (item.rubric && item.show_rubric) {
    return [
      SHOW_BACKGROUND,
      SHOW_ANSWERS,
      SHOW_NAVIGATION,
      SHOW_RUBRIC,
      PLAY_AUDIO_CAN_SELECT,
      SHOW_LISTEN_AGAIN,
      PLAY_LISTEN_AGAIN_AUDIO,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
      SHOW_ANSWERS_ON_BACKGROUND,
    ];
  }

  return [
    SHOW_BACKGROUND,
    createStepWithOverrides(SHOW_ANSWERS, { answersOverlayVisible: true }),
    createStepWithOverrides(SHOW_NAVIGATION, { answersOverlayVisible: true }),
    createStepWithOverrides(PLAY_AUDIO_CAN_SELECT, {
      answersOverlayVisible: true,
    }),
    createStepWithOverrides(USER_SELECT_FIRST_TIME, {
      answersOverlayVisible: true,
    }),
    createStepWithOverrides(SHOW_LISTEN_AGAIN, { answersOverlayVisible: true }),
    createStepWithOverrides(PLAY_LISTEN_AGAIN_AUDIO, {
      answersOverlayVisible: true,
    }),
    createStepWithOverrides(SHOW_FINISH_BUTTON, {
      answersOverlayVisible: true,
    }),
    POST_FINISH_STEP,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
    SHOW_ANSWERS_ON_BACKGROUND,
  ];
}

export function createReadWriteItemBaseSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      createReadWriteStepWithDelay(SHOW_ANSWERS, 6000),
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      SHOW_INPUTS_SPECIFIC,
      SHOW_HELPER_ANIMATION,
      WAIT_FOR_EXAMPLE_TO_BE_COMPLETED,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    createReadWriteStepWithDelay(SHOW_ANSWERS, 6000),
    SHOW_CUSTOM_NAVIGATION,
    SHOW_INPUTS_SPECIFIC,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createFactualTopicItemSteps(
  item: FactualTopicItem
): ItemStep[] {
  if (item.is_example) {
    return [
      item.phase === 'PHASE_1'
        ? createReadWriteStepWithDelay(SHOW_ANSWERS, 8000)
        : SHOW_ANSWERS,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      DROP_OPACITY_WRONG_ANSWERS,
      SHOW_HELPER_ANIMATION,
      WAIT_FOR_EXAMPLE_TO_BE_COMPLETED,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }
  return [
    SHOW_ANSWERS,
    createReadWriteStepWithDelay(SHOW_NAVIGATION, 1000),
    WAIT_TO_SHOW_FINISH,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createOnlineMessagesItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_BACKGROUND,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      SHOW_QUESTION,
      SHOW_STIMULUS,
      SHOW_ANSWERS_TOGGLE,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_BACKGROUND,
    SHOW_QUESTION,
    SHOW_STIMULUS,
    SHOW_ANSWERS_TOGGLE,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createComicStoryItemSteps(item: Item): ItemStep[] {
  if (item.is_example) {
    return [
      SHOW_BACKGROUND,
      SHOW_STIMULUS,
      SHOW_ASSETS,
      SHOW_QUESTION,
      SHOW_OVERLAY,
      SHOW_RUBRIC_WITH_OVERLAY,
      SHOW_EXAMPLE,
      HIDE_OVERLAY,
      SHOW_KEYBOARD,
      DROP_OPACITY_WRONG_ANSWERS,
      SHOW_HELPER_ANIMATION,
      WAIT_FOR_EXAMPLE_TO_BE_COMPLETED,
      SHOW_FINISH_BUTTON,
      SHOW_OVERLAY,
      SHOW_STAR,
      HIDE_STAR_OVERLAY,
    ];
  }

  return [
    SHOW_BACKGROUND,
    SHOW_ASSETS,
    SHOW_STIMULUS,
    SHOW_QUESTION,
    SHOW_KEYBOARD,
    SHOW_OVERLAY,
    SHOW_STAR,
    HIDE_STAR_OVERLAY,
  ];
}

export function createSpeakingItemSteps(item: Item): ItemStep[] {
  // Deafault is Intro which is also SIMPLE
  // ....this will change
  return [
    SHOW_BACKGROUND,
    PLAY_AUDIO_CAN_SELECT,
    SHOW_RECORD_BUTTON,
    SHOW_FINISH_BUTTON,
    SHOW_OVERLAY,
    SHOW_STAR_FOR_INTRO,
    HIDE_STAR_OVERLAY,
  ];
}

function checkShowKeyboard(taskTypeId: TaskTypeId): boolean {
  switch (taskTypeId) {
    case 'spelling':
      return true;
    default:
      return false;
  }
}

export function createShowReadWriteInputsSpecific(
  item: Item,
  taskTypeId: TaskTypeId
): ItemStep[] {
  const specificInputSteps = [];
  let showKeyboard = checkShowKeyboard(taskTypeId);
  if (showKeyboard) {
    specificInputSteps.push(SHOW_KEYBOARD);
  }

  if (item.is_example) {
    specificInputSteps.push(
      createReadWriteStepWithDelay(DROP_OPACITY_WRONG_ANSWERS, 400)
    );
  } else {
    if (!showKeyboard) {
      specificInputSteps.push(SHOW_FINISH_BUTTON);
    }
  }
  return specificInputSteps;
}

export function createReadWriteStepWithDelay(
  step: ItemStep,
  delay: number,
  canSelectAnswer?: boolean | undefined
): ItemStep {
  return {
    ...step,
    transitionTime: delay,
    canSelectAnswer:
      canSelectAnswer !== undefined ? canSelectAnswer : step.canSelectAnswer,
  };
}

export function createReadWriteItemSteps(
  item: Item,
  taskTypeId: TaskTypeId
): ItemStep[] {
  const readWriteSteps = createReadWriteItemBaseSteps(item);

  if (readWriteSteps.indexOf(SHOW_INPUTS_SPECIFIC) >= 0) {
    readWriteSteps.splice(
      readWriteSteps.indexOf(SHOW_INPUTS_SPECIFIC),
      1,
      ...createShowReadWriteInputsSpecific(item, taskTypeId)
    );
  }

  return readWriteSteps;
}
export function createItemSteps(
  item: Item,
  taskTypeId: TaskTypeId = 'minimal_differences'
): ItemStep[] {
  switch (taskTypeId) {
    case 'labeling':
      return createLabelingItemSteps(item);
    case 'scene_creation':
      return createSceneCreationItemSteps(item);
    case 'picture_editing':
      return createPictureEditingItemSteps(item);
    case 'dialogue':
      return createDialogueItemSteps(item);
    case 'information_posters':
    case 'spelling':
      return createReadWriteItemSteps(item, taskTypeId);
    case 'factual_topic':
      return createFactualTopicItemSteps(item as FactualTopicItem);
    case 'online_messages':
      return createOnlineMessagesItemSteps(item);
    case 'comic_story':
      return createComicStoryItemSteps(item);
    // So what we really need here is to switch by 'skill_id'
    case 'meet_and_greet':
      // case 'scene_highlighting':
      // case 'scene_builder':
      // case 'shop':
      // case 'questions_about_you':
      return createSpeakingItemSteps(item);
    default:
      return createMinimalDifferencesItemSteps(item);
  }
}

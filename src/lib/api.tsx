import { put, post } from './dylFetch';
import { User, Test, UserTask, Item, Task, SkillId } from 'types';

interface UpdateUser {
  age: number;
  nationality: string;
  username: string;
  avatar?: string;
}

export async function updateUser(user_id: string, body: UpdateUser) {
  const headers = {
    Authorization: user_id,
  };

  const { error, data } = await put<User>(`me/users`, {
    body,
    headers,
  });
  return { error, data };
}

export async function createUser(body: any) {
  const { error, data } = await post<User>(
    'me/users',
    {
      body,
    },
    'createUser'
  );
  return { error, data };
}

export async function postAnswers({
  task_by_user_id,
  test_by_user_id,
  user_id,
  body,
}: {
  task_by_user_id: string;
  test_by_user_id: string;
  user_id: string;
  body: any;
}) {
  const headers = {
    Authorization: user_id,
  };
  const { error, data } = await post(
    `me/tests/${test_by_user_id}/tasks/${task_by_user_id}/user_answers`,
    {
      headers,
      body,
    },
    'postAnswers'
  );
  return { error, data };
}

export async function getTests({
  user_id,
  skill_id,
  token_id,
}: {
  user_id: string;
  skill_id: SkillId;
  token_id: string;
}) {
  const headers = {
    Authorization: user_id,
  };

  const body = { token_id };

  const { error, data } = await post<Test[]>(
    `me/tests/?skill_id=${skill_id}`,
    {
      headers,
      body,
    },
    'getTests'
  );
  return { error, data };
}

export async function getTasks({
  user_id,
  test_by_user_id,
}: {
  user_id: string;
  test_by_user_id: string;
}) {
  const headers = {
    Authorization: user_id,
  };
  const { error, data } = await post<Task[]>(
    `me/tests/${test_by_user_id}/tasks`,
    {
      headers,
    },
    'getTasks'
  );
  return { error, data };
}

export async function getTaskItem({
  user_id,
  task_by_user_id,
  test_by_user_id,
  debug = false,
}: {
  user_id: string;
  task_by_user_id: string;
  test_by_user_id: string;
  debug?: boolean;
}) {
  const headers = {
    Authorization: user_id,
  };
  const { error, data } = await post<UserTask<Item>>(
    `me/tests/${test_by_user_id}/tasks/${task_by_user_id}/items${
      debug ? '?debug=true' : ''
    }`,
    {
      headers,
    },
    'getTaskItem'
  );
  return { error, data };
}

export async function postSurvey({
  test_by_user_id,
  user_id,
  body,
}: {
  test_by_user_id: string;
  user_id: string;
  body: any;
}) {
  const headers = {
    Authorization: user_id,
  };
  const { error, data } = await post(
    `me/tests/${test_by_user_id}/surveys`,
    {
      headers,
      body,
    },
    'postSurvey'
  );
  return { error, data };
}

export async function resendAnswers(answersToResend: any[]) {
  const answersNotSent: any = [];

  for (let i = 0; i < answersToResend.length; i++) {
    try {
      const { error } = await postAnswers(answersToResend[i]);
      if (error) {
        answersNotSent.push(answersToResend[i]);
      }
    } catch (error) {
      answersNotSent.push(answersToResend[i]);
    }
  }

  return answersNotSent;
}

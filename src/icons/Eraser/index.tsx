import React from 'react';
import style from './eraser.module.css';

export default function Eraser() {
  return (
    <svg className={style.eraser} viewBox="0 0 112 64">
      <g id="eraser" transform="translate(17 13)">
        <path
          id="bounding-box"
          className={style.st0}
          d="M-17-13H95v64H-17V-13z"
        />
        <g transform="matrix(1, 0, 0, 1, -17, -13)">
          <g id="eraserBody-shadow-2" transform="translate(17 13)">
            <path
              className={style.st1}
              d="M83,47h-4H15h-4v-2H-1c-3.2,0-6.2-1.2-8.5-3.5S-13,36.2-13,33V5c0-3.2,1.2-6.2,3.5-8.5S-4.2-7-1-7h12v-2h4
				h64h4v2h4h4v4v44v4h-4h-4V47z"
            />
            <path
              className={style.st1}
              d="M79,43v-2h8V-3h-8v-2H15v2H-1c-4.4,0-8,3.6-8,8v28c0,4.4,3.6,8,8,8h16v2H79 M87,51h-8H15H7v-2h-8
				c-8.8,0-16-7.2-16-16V5c0-8.8,7.2-16,16-16h8v-2h8h64h8v2l0,0h8v8v44v8h-8V51z"
            />
          </g>
        </g>
        <g id="eraserBody-outline">
          <path
            className={style.st1}
            d="M81,45h-2H15h-2v-2H-1c-5.5,0-10-4.5-10-10V5C-11-0.5-6.5-5-1-5h14v-2h2h64h2v2h6h2v2v44v2h-2h-6V45z"
          />
          <path
            className={style.st2}
            d="M79,43v-2h8V-3h-8v-2H15v2H-1c-4.4,0-8,3.6-8,8v28c0,4.4,3.6,8,8,8h16v2H79 M83,47h-4H15h-4v-2H-1
			c-3.2,0-6.2-1.2-8.5-3.5S-13,36.2-13,33V5c0-3.2,1.2-6.2,3.5-8.5S-4.2-7-1-7h12v-2h4h64h4v2h4h4v4v44v4h-4h-4V47z"
          />
        </g>
        <g id="eraserCover-Blue" transform="translate(20)">
          <path
            className={style.st3}
            d="M63.5,43.5H63H-9h-0.5V43V-5v-0.5H-9h72h0.5V-5v48V43.5z"
          />
          <path
            className={style.st2}
            d="M-9-5v48h72V-5H-9 M-10-6h1h72h1v1v48v1h-1H-9h-1v-1V-5V-6z"
          />
        </g>
        <path
          id="eraserCover-Red"
          className={style.st4}
          d="M11-5h44v48H11V-5z"
        />
        <path id="eraserShadow" className={style.st5} d="M11,11h72v16H11V11z" />
      </g>
    </svg>
  );
}

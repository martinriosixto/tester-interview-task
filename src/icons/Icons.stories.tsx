import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import Eraser from './Eraser';
import Pencil from './Pencil';

export default {
  title: 'Icons',
  decorators: [withKnobs]
};

export const Icons = () => {
  return (
    <div className="m-4">
      <div className="m-4">
        <Eraser />
      </div>
      <div className="m-4">
        <Pencil color="#FF0000" />
      </div>
    </div>
  );
};

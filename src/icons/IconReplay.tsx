import React from 'react';

export default function IconReplay() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
      <path className="a text-transparent fill-current" d="M0,0H40V40H0Z" />
      <path
        className="b fill-current"
        d="M-21.922-200.219A13.3,13.3,0,0,0-9.1-210.067h3.609l-5.522-9.568-5.524,9.566h3.171a9.232,9.232,0,0,1-8.558,5.8,9.232,9.232,0,0,1-9.221-9.223,9.232,9.232,0,0,1,9.221-9.221,9.193,9.193,0,0,1,6.658,2.851l2.083-3.609a13.209,13.209,0,0,0-8.74-3.295A13.289,13.289,0,0,0-35.2-213.494,13.289,13.289,0,0,0-21.922-200.219Z"
        transform="translate(40.196 233.019)"
      />
    </svg>
  );
}

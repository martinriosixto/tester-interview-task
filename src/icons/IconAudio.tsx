import React from 'react';

export default function IconAudio() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
      <path className="a text-transparent fill-current" d="M0,0H40V40H0Z" />
      <path
        className="b fill-current"
        d="M-32.08-228.762a4.589,4.589,0,0,0,4.589-4.589v0h0v-14.887h14.74v10.631a4.575,4.575,0,0,0-1.7-.329,4.589,4.589,0,0,0-4.59,4.589,4.589,4.589,0,0,0,4.589,4.589,4.589,4.589,0,0,0,4.589-4.589v0h0v-20.655H-30.375v16.4a4.57,4.57,0,0,0-1.705-.329,4.589,4.589,0,0,0-4.589,4.589A4.589,4.589,0,0,0-32.08-228.762Z"
        transform="translate(42.919 261.509)"
      />
    </svg>
  );
}

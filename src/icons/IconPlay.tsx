import React from 'react';

export default function IconPlay() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
      <path className="a text-transparent fill-current" d="M0,0H40V40H0Z" />
      <path
        className="b fill-current"
        d="M-15.493-135.849l-1.056-12.072s2.965,1.634,6.15,3.835c4.282,2.96,9.112,6.775,9.112,7.428,0,1.593-16.048,11.877-16.048,11.877"
        transform="translate(30.275 156.309)"
      />
    </svg>
  );
}

import React from 'react';

export default function IconMic() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path className="fill-current" d="M6 6h12v12H6z" />
    </svg>
  );
}

import React from 'react';

export default function IconBack() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 40 40"
      style={{ transform: 'rotate(180deg)' }}
    >
      <path className="a text-transparent fill-current" d="M0,0H40V40H0Z" />
      <path
        className="b fill-current"
        d="M25.978,9.979c-.006-.053.094-4.29,0-4.342s-4.712,0-6.485.052-7.12.076-8.006.1V0S0,11.735,0,12.225c0,1.2,12.076,13.94,12.076,13.94l.245-6.529a20.587,20.587,0,0,1,3.715-.07c2.744.1,9.379.175,9.554.1s.165-.921.169-1.911.212-2.171.177-2.241"
        transform="translate(33.018 33.665) rotate(180)"
      />
    </svg>
  );
}

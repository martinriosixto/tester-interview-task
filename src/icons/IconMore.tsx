import React from 'react';

export default function IconMore() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path className="text-transparent fill-current" d="M0,0H24V24H0Z" />
      <path d="M18,14.5A2.5,2.5,0,1,1,20.5,12,2.5,2.5,0,0,1,18,14.5Zm-6,0A2.5,2.5,0,1,1,14.5,12,2.5,2.5,0,0,1,12,14.5Zm-6,0A2.5,2.5,0,1,1,8.5,12,2.5,2.5,0,0,1,6,14.5Z" />
      <path
        className="fill-current text-white"
        d="M18,10a2,2,0,1,1-2,2,2,2,0,0,1,2-2m-6,0a2,2,0,1,1-2,2,2,2,0,0,1,2-2M6,10a2,2,0,1,1-2,2,2,2,0,0,1,2-2M18,9a3,3,0,1,0,3,3,3,3,0,0,0-3-3ZM12,9a3,3,0,1,0,3,3,3,3,0,0,0-3-3ZM6,9a3,3,0,1,0,3,3A3,3,0,0,0,6,9Z"
      />
    </svg>
  );
}

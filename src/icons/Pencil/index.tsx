import React from 'react';
import style from './pencil.module.css';

interface PencilProps {
  color: string;
}
const Pencil: React.FC<PencilProps> = ({ color }) => {
  return (
    <svg viewBox="0 0 200 64" className={style.pencil}>
      <g id="pencil" transform="translate(29.448 16)">
        <path
          id="bounding-box"
          className={style.st0}
          d="M-29.4-16h200v64h-200C-29.4,48-29.4-16-29.4-16z"
        />
        <path
          id="pancilBackground-shadow_1_"
          className={style.st1}
          d="M39.6-8L-7.7,16.3L39.6,40H160V-8H39.6 M37.7-16h1.9H160h8v8v48v8h-8H39.6
		h-1.9L36,47.2l-47.3-23.7l-14.1-7.1l14-7.2L36-15.1L37.7-16z"
        />
        <path
          id="pancilBackground"
          className={style.st1}
          d="M162.1,42h-2H39.6h-0.5l-0.4-0.2L-8.6,18.1l-3.5-1.8l3.5-1.8L38.7-9.8l0.4-0.2h0.5H160
		h2v2v48v2H162.1z"
        />
        <path
          id="pencilBody-outline_1_"
          className={style.st2}
          d="M39.6-8L-7.7,16.3L39.6,40H160V-8H39.6 M38.7-12h1h120.4h4v4v48v4h-4H39.6h-0.9
		l-0.9-0.4L-9.5,19.9l-7.1-3.5l7-3.6l47.4-24.4L38.7-12z"
        />
        <path
          id="pencil-color"
          className={style.st3}
          d="M40,40V-8h119.8v48H40z M-7.9,16.6l22-11.3v22L-7.9,16.6z"
          fill={color}
        />
        <path
          id="pancilHighlight"
          className={style.st4}
          d="M39,28h120v16H39V28z"
        />
        <path
          id="pancilShadow"
          className={style.st5}
          d="M39.8-8h120V8h-120V-8z"
        />
      </g>
    </svg>
  );
};

export default Pencil;

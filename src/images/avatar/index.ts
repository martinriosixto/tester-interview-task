export const avatarSkinTones = [
  '#d6d6d6',
  '#f0d9c5',
  '#edceaf',
  '#ecc6aa',
  '#e6bf97',
  '#d9ac90',
  '#c5a690',
  '#a78973',
  '#9f7c52',
  '#956535',
  '#7d4f2d',
  '#6c4227',
  '#5f3721',
  '#472e21',
];

const CLOUDFRONT_URL = process.env.REACT_APP_AWS_CLOUDFRONT_STATIC_ASSETS_DOMAIN;

const avatarImages = [
  [
    { src: CLOUDFRONT_URL + 'head-1.svg', label: 'head' },
    { src: CLOUDFRONT_URL + 'head-2.svg', label: 'head' },
    { src: CLOUDFRONT_URL + 'head-3.svg', label: 'head' },
  ],
  [
    { src: CLOUDFRONT_URL + 'body-1.svg', label: 'body' },
    { src: CLOUDFRONT_URL + 'body-2.svg', label: 'body' },
    { src: CLOUDFRONT_URL + 'body-3.svg', label: 'body' },
  ],
  [
    { src: CLOUDFRONT_URL + 'legs-1.svg', label: 'legs' },
    { src: CLOUDFRONT_URL + 'legs-2.svg', label: 'legs' },
    { src: CLOUDFRONT_URL + 'legs-3.svg', label: 'legs' },
  ],
];

export const avatarKit = [
  [
    [
      { src: CLOUDFRONT_URL + 'headwear-1.svg', label: 'headwear' },
      { src: CLOUDFRONT_URL + 'headwear-2.svg', label: 'headwear' },
      { src: CLOUDFRONT_URL + 'headwear-3.svg', label: 'headwear' },
      { src: CLOUDFRONT_URL + 'headwear-4.svg', label: 'headwear' },
    ]
  ],
  [
    [
      { src: CLOUDFRONT_URL + 'facewear-1.svg', label: 'facewear' },
      { src: CLOUDFRONT_URL + 'facewear-2.svg', label: 'facewear' },
      { src: CLOUDFRONT_URL + 'facewear-3.svg', label: 'facewear' },
      { src: CLOUDFRONT_URL + 'facewear-4.svg', label: 'facewear' },
    ]
  ],
  [
    [
      { src: CLOUDFRONT_URL + 'backwear-1.svg', label:  'backwear' },
      { src: CLOUDFRONT_URL + 'backwear-2.svg', label:  'backwear' },
      { src: CLOUDFRONT_URL + 'backwear-3.svg', label:  'backwear' },
      { src: CLOUDFRONT_URL + 'backwear-4.svg', label:  'backwear' },
    ]
  ],
  [
    [
      { src: CLOUDFRONT_URL + 'footwear-1.svg', label:  'footwear' },
      { src: CLOUDFRONT_URL + 'footwear-2.svg', label:  'footwear' },
      { src: CLOUDFRONT_URL + 'footwear-3.svg', label:  'footwear' },
      { src: CLOUDFRONT_URL + 'footwear-4.svg', label:  'footwear' },
    ]
  ],
  [
    [
      { src: CLOUDFRONT_URL + 'holding-1.svg', label:  'holding' },
      { src: CLOUDFRONT_URL + 'holding-2.svg', label:  'holding' },
      { src: CLOUDFRONT_URL + 'holding-3.svg', label:  'holding' },
      { src: CLOUDFRONT_URL + 'holding-4.svg', label:  'holding' },
    ]
  ],
]

export default avatarImages;

const backgroundImages = [
  { src:  process.env.REACT_APP_AWS_CLOUDFRONT_STATIC_ASSETS_DOMAIN + 'dyl_background_1@2x.png'},
  { src:  process.env.REACT_APP_AWS_CLOUDFRONT_STATIC_ASSETS_DOMAIN + 'dyl_background_2@2x.png' },
  { src:  process.env.REACT_APP_AWS_CLOUDFRONT_STATIC_ASSETS_DOMAIN + 'dyl_background_3@2x.png' },
];

export const randomiseBackground = () =>{
  return backgroundImages[Math.floor(Math.random() * 3)].src;
}

export default backgroundImages;

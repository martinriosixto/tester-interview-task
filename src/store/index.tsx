import * as Sentry from '@sentry/react';
import {
  User,
  State,
  UserTask,
  ItemAnswer,
  Item,
  Theme,
  Task,
  SkillId,
} from 'types';

export type Action =
  | { type: 'CREATE_USER_REQUEST' }
  | { type: 'CREATE_USER_SUCCESS'; payload: User }
  | { type: 'CREATE_USER_FAILED' }
  | { type: 'UPDATE_USER_REQUEST' }
  | { type: 'UPDATE_USER_SUCCESS'; payload: User }
  | { type: 'UPDATE_USER_FAILED' }
  | {
      type: 'UPDATE_AVATAR_THEME';
      payload: Theme;
    }
  | {
      type: 'LOAD_TASKS_SUCCESS';
      payload: {
        tasks: Task[];
        test_by_user_id: string;
      };
    }
  | {
      type: 'ADD_USER_TASKS';
      payload: {
        userTasks: {
          task: Task;
          userTask?: UserTask<Item>;
        }[];
      };
    }
  | { type: 'ADD_EXAMPLE_SEEN'; payload: string }
  | { type: 'SET_ERROR'; payload: string }
  | { type: 'SET_TEST_BY_USER_ID'; payload: string }
  | {
      type: 'UPDATE_TASK_ANSWERS';
      payload: { taskAnswers: (ItemAnswer | null)[][] };
    }
  | { type: 'SET_CURRENT_TASK_INDEX'; payload: number }
  | { type: 'SET_CURRENT_ITEM_INDEX'; payload: number }
  | { type: 'SET_SKILL'; payload: SkillId }
  | { type: 'SET_DEBUG'; payload: boolean }
  | { type: 'RESET_TASK_ANSWERS' }
  | { type: 'NEEDS_TO_BE_IN_FULL_SCREEN'; payload: boolean }
  | { type: 'SET_ANSWERS_TO_RESEND'; payload: any[] }
  | { type: 'SET_ANALYTICS_TO_RESEND'; payload: any[] };

export type Dispatch = (action: Action) => void;
export const AppReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'CREATE_USER_REQUEST': {
      return { ...state, isLoading: true };
    }
    case 'CREATE_USER_SUCCESS': {
      return { ...state, user: { ...action.payload }, isLoading: false };
    }
    case 'CREATE_USER_FAILED': {
      return { ...state, isLoading: false, isError: true };
    }
    case 'UPDATE_USER_REQUEST': {
      return { ...state, isLoading: true };
    }
    case 'UPDATE_USER_SUCCESS': {
      return { ...state, user: action.payload, isLoading: false };
    }
    case 'UPDATE_USER_FAILED': {
      return { ...state, isLoading: false, isError: true };
    }
    case 'UPDATE_AVATAR_THEME': {
      return { ...state, theme: { ...action.payload } };
    }
    case 'SET_SKILL': {
      return { ...state, skill_id: action.payload };
    }
    case 'SET_DEBUG': {
      return { ...state, debug: action.payload };
    }
    case 'SET_TEST_BY_USER_ID': {
      return {
        ...state,
        test_by_user_id: action.payload,
      };
    }
    case 'ADD_USER_TASKS': {
      return {
        ...state,
        userTasks: [...(state.userTasks || []), ...action.payload.userTasks],
      };
    }
    case 'ADD_EXAMPLE_SEEN': {
      return {
        ...state,
        examplesSeen: [...state.examplesSeen, action.payload],
      };
    }
    case 'UPDATE_TASK_ANSWERS': {
      return {
        ...state,
        taskAnswers: action.payload.taskAnswers,
      };
    }
    case 'SET_CURRENT_TASK_INDEX': {
      const currentTask = state.userTasks && state.userTasks[action.payload];
      return {
        ...state,
        currentTaskIndex: action.payload,
        currentItemIndex: 0,
        task_by_user_id: currentTask?.task.task_by_user_id,
        userTask: currentTask?.userTask,
        taskAnswers: undefined,
        hasSeenExample: {},
      };
    }
    case 'SET_CURRENT_ITEM_INDEX': {
      return {
        ...state,
        currentItemIndex: action.payload,
      };
    }
    case 'RESET_TASK_ANSWERS': {
      return {
        ...state,
        taskAnswers: undefined,
      };
    }
    case 'NEEDS_TO_BE_IN_FULL_SCREEN': {
      return {
        ...state,
        needsToBeInFullscreen: action.payload,
      };
    }
    case 'SET_ANSWERS_TO_RESEND': {
      return {
        ...state,
        answersToResend: action.payload,
      };
    }
    case 'SET_ANALYTICS_TO_RESEND': {
      return {
        ...state,
        analyticsToResend: action.payload,
      };
    }
    case 'SET_ERROR': {
      Sentry.captureException(action.payload);
      return {
        ...state,
        error: action.payload,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action}`);
    }
  }
};

import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import ItemAudioPlayerIcon from './index';

export default {
    title: 'Item Audio Player Icon',
    component: ItemAudioPlayerIcon,
    decorators: [withKnobs],
};

export const AudioPlayerComponent = () => (
    <div>
        <div className="m-8 bg-red-500">
            <ItemAudioPlayerIcon />
        </div>

        <div className="m-8 bg-red-500">
            <h2>Without animation</h2>
            <ItemAudioPlayerIcon animate={false} />
        </div>

        <div className="m-8 bg-red-500">
            <h2>With custom duration</h2>
            <ItemAudioPlayerIcon duration={2} />
        </div>
    </div>
);

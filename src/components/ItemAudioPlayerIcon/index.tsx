import React from 'react';
import IconAudio from 'icons/IconAudio';
import styles from './ItemAudioPlayerIcon.module.css';

import { motion } from 'framer-motion';

interface ItemAudioPlayerIconProps {
    animate?: boolean;
    duration?: number;
}

const ItemAudioPlayerIcon: React.FC<ItemAudioPlayerIconProps> = ({
     animate = true,
     duration,
 }) => {
    return (
        <>
        <div className={styles.player}>
        <span className={styles.content}>
          {animate ? (
              <motion.span
                  className={styles.icon}
                  animate={{ scale: 0.8 }}
                  transition={{
                      yoyo: Infinity,
                      repeatDelay: 0.4,
                      duration: duration,
                  }}
              >
                  <IconAudio />
              </motion.span>
          ) : (
              <span className={styles.icon}>
              <IconAudio />
            </span>
          )}
        </span>
            </div>
        </>
    );
};
export default ItemAudioPlayerIcon;
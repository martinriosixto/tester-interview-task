import React from 'react';
import Lottie from 'react-lottie';

import animationData from 'images/lotties/draganddrop.json';

interface LottieDragProps {
  width?: number;
  top?: number;
  left?: number;
  right?: number;
  bottom?: number;
  autoplay?: boolean;
}

const LottieDrag: React.FC<LottieDragProps> = ({
  width = 124,
  top,
  left,
  bottom,
  right,
  autoplay = true,
}) => {
  const options = {
    loop: true,
    autoplay,
    animationData: animationData,
  };

  return (
    <Lottie
      style={{
        position: 'absolute',
        zIndex: 10,
        pointerEvents: 'none',
        top: top ? `${top}px` : undefined,
        left: left ? `${left}px` : undefined,
        bottom: bottom ? `${bottom}px` : undefined,
        right: right ? `${right}px` : undefined,
      }}
      options={options}
      width={width}
    />
  );
};

export default LottieDrag;

import React from 'react';

import LottieDrag from './index';

export default {
  title: 'Lottie Drag',
  component: [LottieDrag],
};

export const Example = () => (
  <div className="m-8">
    <LottieDrag />
  </div>
);

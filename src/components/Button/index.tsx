import React from 'react';
import { classNames } from 'lib';
import IconForward from 'icons/IconForward';
import './Button.css';

interface ButtonLargeProps {
  Icon?: any;
  text: string;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  className?: string;
}

export const ButtonLarge: React.FC<ButtonLargeProps> = ({
  className,
  text,
  Icon,
  onClick,
}) => {
  return (
    <button
      className={classNames(['dyl-button dyl-button__large', className || ''])}
      onClick={onClick}
    >
      <span className="dyl-button__content_wrapper animated-component">
        <span className="dyl-button__content">
          {Icon && <span className="dyl-button__icon">{Icon && <Icon />}</span>}
          {text && (
            <span
              className={`dyl-button__text ${
                Icon ? '' : 'dyl-button__text--left-margin'
              }`}
            >
              {text}
            </span>
          )}
        </span>
      </span>
    </button>
  );
};

interface ButtonSmallProps {
  Icon: any;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export const ButtonSmall: React.FC<ButtonSmallProps> = ({
  children,
  onClick,
  Icon,
  ...props
}) => {
  return (
    <button
      className="dyl-button dyl-button__small animated-component"
      onClick={onClick}
      {...props}
    >
      <span className="dyl-button__content_wrapper">
        <span className="dyl-button__content">
          <span className="dyl-button__icon">
            <Icon />
          </span>
        </span>
      </span>
    </button>
  );
};

interface OkButtonProps {
  className?: string;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export const OkButton: React.FC<OkButtonProps> = ({
  onClick,
  className,
  ...props
}) => {
  return (
    <button className="dyl-button dyl-button__ok" onClick={onClick} {...props}>
      <span className="dyl-button__content_wrapper animated-component">
        <span className="dyl-button__content">
          <span className="dyl-button__icon">
            <IconForward />
          </span>
          <span className={`dyl-button__text`}>OK</span>
        </span>
      </span>
    </button>
  );
};

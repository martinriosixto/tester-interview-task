import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';

import { ButtonLarge, ButtonSmall, OkButton } from './index';
import IconForward from 'icons/IconForward';
import IconHelp from 'icons/IconHelp';
export default {
  title: 'Buttons',
  component: ButtonLarge,
  decorators: [withKnobs],
};

export const AllButtons = () => (
  <div>
    <div className="m-8 bg-red-300">
      <ButtonLarge
        onClick={action('clicked')}
        text="Finish"
        Icon={IconForward}
      />
    </div>
    <div className="m-8">
      <ButtonLarge onClick={action('clicked')} text="Finish" />
    </div>
    <div className="m-8">
      <ButtonSmall onClick={action('clicked')} Icon={IconForward} />
    </div>
    <div className="m-8">
      <ButtonSmall onClick={action('clicked')} Icon={IconHelp} />
    </div>
    <div className="m-8">
      <OkButton onClick={action('clicked')} />
    </div>
  </div>
);

export const ButtonLargeWithIcon = () => (
  <div>
    <div className="m-8">
      <ButtonLarge
        onClick={action('clicked')}
        text="Finish"
        Icon={IconForward}
      />
    </div>
  </div>
);
export const ButtonLargeJustText = () => (
  <div>
    <div className="m-8">
      <ButtonLarge onClick={action('clicked')} text="Finish" />
    </div>
  </div>
);
export const ButtonSmallWithIcon = () => (
  <div>
    <div className="m-8">
      <ButtonSmall onClick={action('clicked')} Icon={IconForward} />
    </div>
  </div>
);

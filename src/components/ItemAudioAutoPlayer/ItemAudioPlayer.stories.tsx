import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import ItemAudioAutoPlayer from './index';

export default {
  title: 'Item Audio Auto Player',
  component: ItemAudioAutoPlayer,
  decorators: [withKnobs]
};

export const AudioPlayerComponent = () => (
  <div>
    <div className="m-8 bg-red-500">
      <ItemAudioAutoPlayer itemAudio={{ src: '' }} onEnded={() => {}} />
    </div>
  </div>
);

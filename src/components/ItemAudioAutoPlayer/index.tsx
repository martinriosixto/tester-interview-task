import React, { useCallback, useEffect, useRef } from 'react';
import { ItemAudio } from 'types';
import './ItemAudioPlayer.css';
import ItemAudioPlayerIcon from 'components/ItemAudioPlayerIcon';
import useAudio from 'hooks/useAudio';
import { sleep } from 'lib';

const AUDIO_ENTRY_EXIT_DURATION = 1000;

interface ItemAudioAutoPlayerProps {
  itemAudio: ItemAudio;
  onEnded: () => void;
  isAudioPlaying?: (isPlaying: boolean) => void;
}

const ItemAudioAutoPlayer: React.FC<ItemAudioAutoPlayerProps> = ({
  itemAudio,
  onEnded,
  isAudioPlaying,
}) => {
  const audio = useAudio(itemAudio.src);
  const isRunning = useRef(false);

  const run = useCallback(async () => {
    await sleep(AUDIO_ENTRY_EXIT_DURATION);
    isAudioPlaying && isAudioPlaying(true);
    await audio.play();
    await sleep(AUDIO_ENTRY_EXIT_DURATION);
    onEnded && onEnded();
    isAudioPlaying && isAudioPlaying(false);
    isRunning.current = false;
  }, [audio, isAudioPlaying, onEnded]);

  useEffect(() => {
    if (!isRunning.current) {
      isRunning.current = true;
      run();
    }
    return () => {};
  }, [isRunning, run]);
  return <ItemAudioPlayerIcon animate={audio.isPlaying} />;
};

export default ItemAudioAutoPlayer;

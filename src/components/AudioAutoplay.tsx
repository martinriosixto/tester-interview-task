import React from 'react';
import { ItemAudio } from 'types';
import useAudio from 'hooks/useAudio';
import useMountEffect from 'hooks/useMountEffect';
import * as Sentry from '@sentry/react';

interface AudioAutoplayProps {
  itemAudio: ItemAudio;
  onEnded?: () => void;
}

const AudioAutoplay: React.FC<AudioAutoplayProps> = ({
  itemAudio,
  onEnded,
}) => {
  const { play } = useAudio(itemAudio.src);

  useMountEffect(() => {
    play()
      .then(() => {
        onEnded && onEnded();
      })
      .catch((err) => {
        Sentry.withScope((scope) => {
          scope.setTag('error', 'audio');
          scope.setTag('error.audio', 'autoplay');
          Sentry.captureException(new Error(`AUDIO ERROR: ${err}`), {
            contexts: {
              audio: {
                src: itemAudio.src,
              },
            },
          });
        });
      });
  });

  return null;
};

export default AudioAutoplay;

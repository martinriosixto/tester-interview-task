import React from 'react';

import ProgressBar from './index';

export default {
  title: 'ProgressBar',
  component: ProgressBar,
};

export const Example = () => {
  return (
    <div className="w-48 space-y-8">
      <ProgressBar counter={0} text="1/1" />
      <ProgressBar counter={0} />
      <ProgressBar counter={25} />
      <ProgressBar counter={50} />
      <ProgressBar counter={75} />
      <ProgressBar counter={100} />
    </div>
  );
};

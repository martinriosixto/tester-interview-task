import React from 'react';
import styles from './ProgressBar.module.css';
import { classNames } from 'lib';

interface ProgressBarProps {
  counter: number;
  text?: string;
}

const ProgressBar: React.FC<ProgressBarProps> = ({ counter, text }) => (
  <div className={styles.progress}>
    {text ? (
      <span className={classNames([styles.text, 'mr-1'])}>{text}</span>
    ) : null}
    <div className={styles.bar}>
      <span style={{ width: `${counter}%` }}></span>
    </div>
  </div>
);

export default ProgressBar;

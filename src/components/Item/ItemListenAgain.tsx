import React from 'react';
import AudioAutoplay from 'components/AudioAutoplay';
import { loadAudio } from 'utils';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { Item } from 'types';
import { getItemAnalyticInfo } from 'lib/analytics';
const listenAgain = loadAudio('./PreA1_UK_FNar_ListenAgain.mp3');

interface ItemListenAgainProps {
  item: Item;
  moveToNextItem: () => void;
}

const ItemListenAgain: React.FC<ItemListenAgainProps> = ({
  item,
  moveToNextItem,
}) => {
  const { recordEvent } = useAnalytics();

  return (
    <button
      onClick={() =>
        recordEvent(ANALYTIC_EVENT.select_ok, getItemAnalyticInfo(item))
      }
      className="bg-primary w-40 text-white font-bold p-4 rounded-lg"
    >
      Listen again
      <AudioAutoplay
        itemAudio={{ src: listenAgain }}
        onEnded={() => {
          recordEvent(
            ANALYTIC_EVENT.audio_listen_again_complete,
            getItemAnalyticInfo(item)
          );
          moveToNextItem();
        }}
      />
    </button>
  );
};

export default ItemListenAgain;

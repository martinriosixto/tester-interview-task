import React from 'react';

import { motion } from 'framer-motion';

// Note. To trigger exit animation must be the descendant of an <AnimatePresence/> component... this split is a bit pants...

type ItemOverlayProps = {
  duration?: number;
  delay?: number;
  onAnimationComplete?: () => void;
  zIndex?: number;
};

const ItemOverlay: React.FC<ItemOverlayProps> = ({
  onAnimationComplete,
  duration = 0.64,
  delay = 0.64,
  zIndex,
}) => (
  <motion.div
    initial={{ opacity: 0, scale: 0 }}
    animate={{ opacity: 1, scale: 10 }}
    exit={{
      opacity: 0,
      scale: 0,
      transition: { delay },
    }}
    transition={{ ease: 'easeInOut', duration }}
    className="overlay"
    style={{ zIndex: zIndex }}
    onAnimationComplete={onAnimationComplete}
  />
);

export default ItemOverlay;

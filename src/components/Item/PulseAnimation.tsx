import { motion } from 'framer-motion';
import React from 'react';


interface PulseAnimationProps {
    children?: React.ReactNode;
    delay?: number;
}


const PulseAnimation: React.FC<PulseAnimationProps> = ({ children, delay = 3.2 }) => (
  <motion.div
    animate={{ scale: 1.2 }}
    transition={{
      yoyo: Infinity,
      repeatDelay: 0.4,
      delay: delay
    }}
  >
    {children}
  </motion.div>
);

export default PulseAnimation;

import React from 'react';
import './ItemProgress.css';

interface ItemProgressProps {
  total: number;
  current: number;
}

const ItemProgress: React.FC<ItemProgressProps> = ({ total, current = 1 }) => {
  return (
    <div className="dyl-item-progress">
      <span className="dyl-item-progress__text">
        {current}/{total}
      </span>
      <div className="dyl-item-progress__bar">
        <span style={{ width: `${(current / total) * 100}%` }}></span>
      </div>
    </div>
  );
};

export default ItemProgress;

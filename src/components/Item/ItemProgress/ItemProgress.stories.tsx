import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import ItemProgress from './index';

export default {
  title: 'ItemProgress',
  component: ItemProgress,
  decorators: [withKnobs],
};

export const Example = () => (
  <div className="m-8 flex flex-col space-y-12">
    <ItemProgress current={1} total={5} />
    <ItemProgress current={2} total={5} />
    <ItemProgress current={3} total={5} />
    <ItemProgress current={4} total={5} />
    <ItemProgress current={5} total={5} />
  </div>
);

import React from 'react';
import AudioAutoplay from 'components/AudioAutoplay';
import { Item } from 'types';
import { loadAudio } from 'utils';
import { CardWithButton } from 'components/Card';
import IconForward from 'icons/IconForward';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { getItemAnalyticInfo } from 'lib/analytics';

interface ItemExampleProps {
  item: Item;
  handleClick: () => void;
}

const ItemExample: React.FC<ItemExampleProps> = ({
  item,
  handleClick: moveToNextItem,
}) => {
  const { recordEvent } = useAnalytics();

  if (!item.rubric) {
    return null;
  }

  const handleCardClick = () => {
    recordEvent(ANALYTIC_EVENT.select_ok, getItemAnalyticInfo(item));
    moveToNextItem();
  };

  return (
    <CardWithButton
      Icon={IconForward}
      buttonText="OK"
      onClick={handleCardClick}
    >
      Here’s an example.
      <AudioAutoplay
        itemAudio={{ src: loadAudio('./PreA1_UK_FNar_ExampleIntro.mp3') }}
        onEnded={() => {
          recordEvent(
            ANALYTIC_EVENT.audio_example_complete,
            getItemAnalyticInfo(item)
          );
        }}
      />
    </CardWithButton>
  );
};

export default ItemExample;

import React from 'react';
import { motion } from 'framer-motion';

type NavElemAnimationProps = {
  left?: number | string;
  right?: number | string;
  bottom?: number | string;
  classes?: string | null;
};

const NavElemAnimation: React.FC<NavElemAnimationProps> = ({
  children,
  left = 'auto',
  right = 'auto',
  bottom = 32,
  classes,
  ...props
}) => (
  <motion.div
    style={{ left, right, bottom, transition: 'all ease-in-out' }}
    initial={{ translateY: 1024 }}
    animate={{ translateY: 0 }}
    exit={{ translateY: 1024 }}
    transition={{ ease: 'easeInOut', duration: 0.4 }}
    className={`absolute z-10 ${classes ? classes : ' '}`}
    {...props}
  >
    {children}
  </motion.div>
);
export default NavElemAnimation;

import React from 'react';
import AudioAutoplay from 'components/AudioAutoplay';
import { loadAudio } from 'utils';

const ItemFantastic = () => {
  const ItemfantasticAudio = [
    loadAudio('./PreA1_UK_FNar_DoingGreat.mp3'),
    loadAudio('./PreA1_UK_FNar_Fantastic.mp3'),
    loadAudio('./PreA1_UK_FNar_Great.mp3'),
    loadAudio('./PreA1_UK_FNar_ThatsCool.mp3'),
    loadAudio('./PreA1_UK_FNar_ThatsFantastic.mp3'),
    loadAudio('./PreA1_UK_FNar_ThatsReallyGood.mp3'),
  ];

  const setRandomAudio = ItemfantasticAudio[Math.floor(Math.random() * 8)];

  return <AudioAutoplay itemAudio={{ src: setRandomAudio }} />;
};

export default ItemFantastic;

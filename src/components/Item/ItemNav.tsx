import React, { useState } from 'react';

import ItemProgress from './ItemProgress';
import { ButtonSmall, OkButton } from 'components/Button';
import CardAnimation from 'components/CardAnimation';
import { TaskItem, UserTask, Item, ItemStep } from 'types';
import ItemAudioAutoPlayer from 'components/ItemAudioAutoPlayer';
import IconHelp from 'icons/IconHelp';
import { motion, AnimatePresence } from 'framer-motion';
import ItemRubric from './ItemRubric';
import NavElemAnimation from './NavElemAnimation';
import AudioAutoplay from 'components/AudioAutoplay';
import PulseAnimation from './PulseAnimation';
import { loadAudio } from 'utils';
import IconPlay from 'icons/IconPlay';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { getItemAnalyticInfo } from 'lib/analytics';

const finishAudio = loadAudio('./PreA1_UK_FNar_TapToFinish.mp3');
const listenAgainAudio = loadAudio('./PreA1_UK_FNar_ListenAgain.mp3');

interface RenderNavProps {
  userTask: UserTask<Item>;
  step: ItemStep;
  item: TaskItem;
  moveToTheNextStep: () => void;
  itemNumber: number;
}

const ItemNav: React.FC<RenderNavProps> = ({
  userTask,
  step,
  item,
  moveToTheNextStep,
  itemNumber,
}) => {
  const [isRubricVisible, setIsRubricVisible] = useState(false);
  const [isListenAgainAudioPlaying, setIsListenAgainAudioPlaying] = useState(
    false
  );
  const { recordEvent } = useAnalytics();

  function renderFinishButton() {
    return (
      <OkButton
        onClick={() => {
          recordEvent(ANALYTIC_EVENT.select_finish);
          moveToTheNextStep();
        }}
      />
    );
  }

  const handleIsListenAgainAudioPlaying = (value: boolean) => {
    setIsListenAgainAudioPlaying(value);
  };

  const showFinishButton =
    step.id === 'SHOW_FINISH_BUTTON' ||
    (step.id === 'SHOW_KEYBOARD' && !item.is_example);

  return (
    <motion.div
      initial={{ visibility: 'hidden' }}
      animate={{ visibility: 'visible' }}
    >
      <AnimatePresence>
        {!step.id.includes('PLAY_AUDIO') &&
          item.rubric &&
          step.showRubricHelpButton && (
            <NavElemAnimation left={24} key={'help_key'}>
              <ButtonSmall
                onClick={() => {
                  recordEvent(ANALYTIC_EVENT.select_help);
                  setIsRubricVisible(true);
                }}
                Icon={IconHelp}
              />
            </NavElemAnimation>
          )}
        {isRubricVisible && item.rubric && (
          <CardAnimation data-testid="SHOW_RUBRIC">
            <ItemRubric
              playAudio={!isListenAgainAudioPlaying}
              item={item}
              handleClick={() => setIsRubricVisible(false)}
            />
          </CardAnimation>
        )}
        {item.audio && step.id.includes('PLAY_AUDIO') && (
          <NavElemAnimation
            left={userTask.is_introductory ? 24 : 120}
            key={'audio_key'}
            data-testid="SHOW_PLAYING_AUDIO"
          >
            <ItemAudioAutoPlayer
              onEnded={() => {
                recordEvent(
                  ANALYTIC_EVENT.audio_stimulus_complete,
                  getItemAnalyticInfo(item)
                );
                moveToTheNextStep();
              }}
              itemAudio={{ src: item.audio.src }}
            />
          </NavElemAnimation>
        )}
        {item.audio && step.id === 'PLAY_LISTEN_AGAIN_AUDIO' && (
          <NavElemAnimation
            left={120}
            key={'audio_key'}
            data-testid="PLAY_LISTEN_AGAIN_AUDIO"
          >
            <ItemAudioAutoPlayer
              isAudioPlaying={handleIsListenAgainAudioPlaying}
              onEnded={() => {
                recordEvent(
                  ANALYTIC_EVENT.audio_listen_again_complete,
                  getItemAnalyticInfo(item)
                );
                moveToTheNextStep();
              }}
              itemAudio={{ src: item.audio.src }}
            />
          </NavElemAnimation>
        )}
        {showFinishButton && (
          <NavElemAnimation
            right={32}
            key="finish_key"
            data-testid="SHOW_FINISH"
          >
            {item.is_example && (
              <AudioAutoplay itemAudio={{ src: finishAudio }} />
            )}

            {item.is_example ? (
              <PulseAnimation delay={5}>{renderFinishButton()}</PulseAnimation>
            ) : (
              renderFinishButton()
            )}
          </NavElemAnimation>
        )}
        {step.id === 'SHOW_LISTEN_AGAIN' && (
          <NavElemAnimation
            left={120}
            key={'play_key'}
            data-testid="SHOW_LISTEN_AGAIN"
          >
            <AudioAutoplay itemAudio={{ src: listenAgainAudio }} />
            <PulseAnimation>
              <ButtonSmall
                Icon={IconPlay}
                onClick={() => {
                  recordEvent(ANALYTIC_EVENT.select_listen_again);
                  moveToTheNextStep();
                }}
              />
            </PulseAnimation>
          </NavElemAnimation>
        )}

        {step.showProgress && (
          <NavElemAnimation
            right={28}
            bottom={showFinishButton ? 120 : 32}
            key="progress_key"
            // @ts-ignore
            layout
            classes={step.bringProgressForward ? 'z-40' : null}
          >
            <ItemProgress total={userTask.items.length} current={itemNumber} />
          </NavElemAnimation>
        )}
      </AnimatePresence>
    </motion.div>
  );
};

export default ItemNav;

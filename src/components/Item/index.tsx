import React, { useState, useEffect } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import * as Sentry from '@sentry/react';
import './item.css';

import {
  TaskItem,
  UserTask,
  Item,
  ItemAnswer,
  ItemAnswers,
  ItemStep,
  UserTaskAnswers,
  TaskTypeId,
  TaskProps,
} from 'types';

import OnlineMessages from 'components/Tasks/OnlineMessages';

import CardAnimation from 'components/CardAnimation';
import WithTimeout from 'components/ WithTimeout';
import AudioAutoplay from 'components/AudioAutoplay';

import ItemRubric from './ItemRubric';
import ItemOverlay from './ItemOverlay';
import ItemNav from './ItemNav';
import ItemStar from './ItemStar';
import ItemExample from './ItemExample';
import ItemFantastic from './ItemFantastic';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { getAnswerAnalyticsInfo, getItemAnalyticInfo } from 'lib/analytics';

const COMPONENT_LOOK_UP: Record<TaskTypeId, React.FC<TaskProps>> = {
  minimal_differences: OnlineMessages,
  labeling: OnlineMessages,
  scene_creation: OnlineMessages,
  picture_editing: OnlineMessages,
  dialogue: OnlineMessages,
  information_posters: OnlineMessages,
  spelling: OnlineMessages,
  online_messages: OnlineMessages,
  comic_story: OnlineMessages,
  factual_topic: OnlineMessages,
  meet_and_greet: OnlineMessages,
};

interface ItemComponentProps {
  completedCallback: (itemAnswers: ItemAnswers) => void;
  userTask: UserTask<Item>;
  item: TaskItem;
  taskAnswers?: UserTaskAnswers;
  itemIndex: number;
  steps: ItemStep[];
  initialStepIndex?: number;
}

const ItemComponent: React.FC<ItemComponentProps> = ({
  completedCallback,
  userTask,
  item,
  itemIndex,
  taskAnswers,
  steps,
  initialStepIndex = 0,
}) => {
  const [itemAnswer, setItemAnswer] = useState<(ItemAnswer | null)[]>([]);
  const [stepIndex, setStepIndex] = useState(initialStepIndex);
  const [itemNumber, setItemNumber] = useState(itemIndex);

  const { recordEvent } = useAnalytics();

  const isLastItem = userTask.items.length === itemNumber;

  const step = steps[stepIndex];

  const isLastStep = stepIndex + 1 === steps.length;
  const hideItemComponent =
    isLastStep && step.id !== 'SHOW_ANSWERS_ON_BACKGROUND';

  const moveToTheNextStep = () => {
    setStepIndex(stepIndex + 1);
  };

  useEffect(() => {
    if (step === undefined) {
      completedCallback(itemAnswer);
    }
  }, [completedCallback, itemAnswer, step]);

  if (!userTask || !item || !step) {
    return null;
  }

  if (process.env.REACT_APP_DEV_MODE === 'true') {
    console.log('STEP', step.id, stepIndex);
  }
  const handleSetItemAnswer = (answer: ItemAnswers) => {
    recordEvent(ANALYTIC_EVENT.select_answer, {
      ...getItemAnalyticInfo(item),
      ...getAnswerAnalyticsInfo({
        answers: answer,
        task_type_id: userTask.task_type_id,
        item,
      }),
    });

    setItemAnswer([...answer]);
    if (
      step &&
      (step.id === 'USER_SELECT_FIRST_TIME' ||
        step.id === 'SHOW_HELPER_ANIMATION')
    ) {
      moveToTheNextStep();
    }
  };

  Sentry.setTag('task.item', `${userTask.task_type_id}:${item.order}`);

  Sentry.setContext('step', {
    task_type_id: userTask.task_type_id,
    id: step.id,
    index: stepIndex,
  });

  let TaskComponent = COMPONENT_LOOK_UP[userTask.task_type_id] || null;

  const handleStarAnimationFin = () => {
    setItemNumber(itemIndex + 1);
  };

  return (
    <>
      {!hideItemComponent && (
        <TaskComponent
          step={step}
          userTask={userTask}
          item={item}
          itemAnswer={itemAnswer}
          handleSetItemAnswer={handleSetItemAnswer}
          taskAnswers={taskAnswers}
          moveToTheNextStep={moveToTheNextStep}
        />
      )}

      {step.transitionTime && (
        <WithTimeout
          key={step.id}
          callback={moveToTheNextStep}
          duration={step.transitionTime}
        />
      )}

      <AnimatePresence>
        {step.overlayVisible && (
          <ItemOverlay
            onAnimationComplete={
              step.transitionTime ? undefined : moveToTheNextStep
            }
          />
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.answersOverlayVisible && <ItemOverlay zIndex={1} />}
      </AnimatePresence>

      <AnimatePresence>
        {step.id === 'SHOW_RUBRIC_WITH_OVERLAY' && item.rubric && (
          <CardAnimation data-testid="SHOW_RUBRIC_WITH_OVERLAY">
            <ItemRubric item={item} handleClick={moveToTheNextStep} />
          </CardAnimation>
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.id === 'SHOW_EXAMPLE' && item.rubric && (
          <CardAnimation data-testid="SHOW_EXAMPLE">
            <ItemExample item={item} handleClick={moveToTheNextStep} />
          </CardAnimation>
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.id === 'SHOW_RUBRIC' && item.rubric && (
          <CardAnimation data-testid="SHOW_RUBRIC">
            <ItemRubric item={item} handleClick={moveToTheNextStep} />
          </CardAnimation>
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.id === 'SHOW_STAR' && (
          <React.Fragment>
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ ease: 'easeInOut' }}
              className="absolute z-40"
            >
              <ItemStar onEnded={handleStarAnimationFin} />
            </motion.div>
            {isLastItem && <ItemFantastic />}
          </React.Fragment>
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.id === 'SHOW_STAR_FOR_INTRO' && (
          <React.Fragment>
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ ease: 'easeInOut' }}
              className="absolute z-40"
            >
              <ItemStar onEnded={handleStarAnimationFin} />
            </motion.div>
            {userTask.final_audio?.src && (
              <AudioAutoplay
                itemAudio={{ src: userTask.final_audio?.src }}
                onEnded={moveToTheNextStep}
              />
            )}
          </React.Fragment>
        )}
      </AnimatePresence>

      <AnimatePresence>
        {step.navVisible && (
          <ItemNav
            step={step}
            item={item}
            moveToTheNextStep={moveToTheNextStep}
            userTask={userTask}
            itemNumber={itemNumber}
          />
        )}
      </AnimatePresence>
    </>
  );
};

export default ItemComponent;

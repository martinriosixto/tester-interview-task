import React from 'react';
import AudioAutoplay from 'components/AudioAutoplay';
import { Item } from 'types';
import IconForward from 'icons/IconForward';
import { CardWithButton } from 'components/Card';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { getItemAnalyticInfo } from 'lib/analytics';

interface ItemRubricProps {
  item: Item;
  handleClick: () => void;
  playAudio?: boolean;
}

const ItemRubric: React.FC<ItemRubricProps> = ({
  item,
  handleClick,
  playAudio = true,
}) => {
  const { recordEvent } = useAnalytics();
  if (!item.rubric) {
    return null;
  }

  const handleCardClick = () => {
    recordEvent(ANALYTIC_EVENT.select_ok, getItemAnalyticInfo(item));
    handleClick();
  };

  return (
    <CardWithButton
      Icon={IconForward}
      buttonText="OK"
      onClick={handleCardClick}
    >
      {item.rubric.rubric_text}

      {playAudio && item.rubric.rubric_audio.src && (
        <AudioAutoplay
          itemAudio={{ src: item.rubric.rubric_audio.src }}
          onEnded={() => {
            recordEvent(
              ANALYTIC_EVENT.audio_rubric_complete,
              getItemAnalyticInfo(item)
            );
          }}
        />
      )}
    </CardWithButton>
  );
};

export default ItemRubric;

import React from 'react';
import Lottie from 'react-lottie';

import animationData from 'images/lotties/stars.json';

type ItemStarProps = {
  onEnded: () => void;
};

const ItemStar: React.FC<ItemStarProps> = ({ onEnded }) => {
  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  const events = [
    {
      eventName: 'complete',
      callback: onEnded,
    },
  ];

  return (
    <Lottie
      options={defaultOptions}
      //@ts-ignore
      eventListeners={events}
      width={1024}
      height={768}
    />
  );
};

export default ItemStar;

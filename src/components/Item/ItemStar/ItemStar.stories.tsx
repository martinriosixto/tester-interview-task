import React from 'react';

import ItemStar from './index';

export default {
  title: 'Item Star',
  component: [ItemStar],
};

export const Example = () => (
  <div className="m-8">
    <ItemStar onEnded={() => {}} />
  </div>
);

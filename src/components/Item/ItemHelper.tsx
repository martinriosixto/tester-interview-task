import React from 'react';
import AudioAutoplay from 'components/AudioAutoplay';
import { Item } from 'types';
import { Card } from 'components/Card';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import { getItemAnalyticInfo } from 'lib/analytics';

interface ItemHelperProps {
  item: Item;
  moveToNextItem: () => void;
}

const ItemHelper: React.FC<ItemHelperProps> = ({ item, moveToNextItem }) => {
  const { recordEvent } = useAnalytics();
  if (!item.rubric) {
    return null;
  }

  return (
    <Card>
      {item.rubric.helper_text}

      {item.rubric.helper_audio.src && (
        <AudioAutoplay
          itemAudio={{ src: item.rubric.helper_audio.src }}
          onEnded={() => {
            recordEvent(
              ANALYTIC_EVENT.audio_helper_complete,
              getItemAnalyticInfo(item)
            );
          }}
        />
      )}
    </Card>
  );
};

export default ItemHelper;

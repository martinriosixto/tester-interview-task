import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { Card, CardWithButton } from './index';
import IconForward from 'icons/IconForward';

export default {
  title: 'Cards',
  component: [Card],
  decorators: [withKnobs],
};

export const AllCards = () => (
  <div>
    <div className="m-8">
      <Card>Choose a picture</Card>
    </div>
    <div className="m-8">
      <CardWithButton Icon={IconForward} buttonText="OK" onClick={() => {}}>
        Here is an example.
      </CardWithButton>
    </div>

    <div className="m-8">
      <CardWithButton buttonText="OK" onClick={() => {}}>
        Here is an example.
      </CardWithButton>
    </div>
  </div>
);

import React, { useEffect, useRef } from 'react';
import styles from './Card.module.css';
import { classNames } from 'lib';

interface CardProps {
  children: React.ReactNode;
  className?: string;
}
export const Card: React.FC<CardProps> = ({ children, className }) => {
  return (
    <div className={classNames([styles.card, className ?? ''])}>
      <div className={styles.content}>
        <span className={styles.text}>{children}</span>
      </div>
    </div>
  );
};

interface CardWithButtonProps {
  buttonText?: string;
  Icon?: any;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  style?: any;
}

export const CardWithButton: React.FC<CardWithButtonProps> = ({
  children,
  Icon,
  buttonText,
  onClick,
  style,
  ...props
}) => {
  const buttonRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    buttonRef.current && buttonRef.current.focus();
  }, []);

  return (
    <div className={styles.card} style={style} {...props}>
      <div className={styles.button__content}>
        <span className={styles.text}>{children}</span>
        <button
          ref={buttonRef}
          className={styles.button}
          onClick={onClick}
          {...props}
        >
          {Icon && (
            <span className={styles.button__icon}>
              <Icon />
            </span>
          )}
          {buttonText && (
            <span className={styles.button__text}>{buttonText}</span>
          )}
        </button>
      </div>
    </div>
  );
};

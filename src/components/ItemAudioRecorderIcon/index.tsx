import React from 'react';
import { classNames } from 'lib';
import IconMic from 'icons/IconMic';
import IconStop from 'icons/IconStop';
import styles from './ItemAudioRecorderIcon.module.css';

interface ItemAudioRecorderIconProps {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  isRecording?: boolean;
}

const ItemAudioRecorderIcon: React.FC<ItemAudioRecorderIconProps> = ({
  onClick,
  isRecording = true,
}) => {
  return (
    <button className={styles.button} onClick={onClick}>
      <span className={styles.content_wrapper}>
        <span
          className={classNames([
            styles.content,
            isRecording ? styles.animate : '',
          ])}
        >
          <span className={styles.icon}>
            {isRecording ? <IconStop /> : <IconMic />}
          </span>
        </span>
      </span>
    </button>
  );
};
export default ItemAudioRecorderIcon;

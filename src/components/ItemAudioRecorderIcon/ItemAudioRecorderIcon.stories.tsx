import React from 'react';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import ItemAudioRecorderIcon from './index';

export default {
  title: 'Item Audio Recorder Icon',
  component: ItemAudioRecorderIcon,
  decorators: [withKnobs],
};

export const AudioRecorderComponent = () => (
  <div className="m-8">
    <ItemAudioRecorderIcon isRecording={boolean('Recording State', false)} />
  </div>
);

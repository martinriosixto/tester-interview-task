import React, { useContext } from 'react';
import useFullScreen from 'hooks/useFullscreen';
import IconForward from 'icons/IconForward';
import { AppContext } from 'context/App';
import { ButtonSmall } from './Button';
import Overlay from './Overlay';
import * as Sentry from '@sentry/react';

function OpenFullScreen() {
  const { state } = useContext(AppContext);
  const { openFullScreen, isFullScreen } = useFullScreen();

  const showOverlay =
    !process.env.REACT_APP_DEV_MODE &&
    state.needsToBeInFullscreen &&
    !isFullScreen;

  if (showOverlay) {
    Sentry.withScope((scope) => {
      scope.setTag('analytics', 'not full screen');
      Sentry.captureMessage('Not in full screen');
    });
  }
  return showOverlay ? (
    <Overlay>
      <div className="w-auto">
        <ButtonSmall onClick={openFullScreen} Icon={IconForward} />
      </div>
    </Overlay>
  ) : null;
}

export default OpenFullScreen;

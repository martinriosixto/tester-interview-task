import React from 'react';

import RotateDevice from './index';

export default {
  title: 'Rotate Device',
  component: [RotateDevice],
};

export const RotateDeviceExample = () => (
  <div className="m-8">
    <RotateDevice />
  </div>
);

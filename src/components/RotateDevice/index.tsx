import useDevice from 'hooks/useDevice';
import React from 'react';
import styles from './RotateDevice.module.css';
import { isTouchDevice } from 'lib';
import { motion } from 'framer-motion';

export default function RotateDevice() {
  const { isPortrait } = useDevice();

  const showRotateDevice = isTouchDevice() && isPortrait;

  return !process.env.REACT_APP_DEV_MODE && showRotateDevice ? (
    <div
      className="fixed h-full w-full bg-white flex items-center justify-center"
      style={{ zIndex: 70 }}
    >
      <motion.div
        animate={{ rotate: 90 }}
        transition={{ repeatDelay: 3, repeat: Infinity, duration: 2 }}
        className={styles.phone}
      >
        <svg className="w-8 text-black" viewBox="0 0 1536 1536">
          <path d="M1536 128v448q0 26-19 45t-45 19h-448q-42 0-59-40-17-39 14-69l138-138Q969 256 768 256q-104 0-198.5 40.5T406 406 296.5 569.5 256 768t40.5 198.5T406 1130t163.5 109.5T768 1280q119 0 225-52t179-147q7-10 23-12 15 0 25 9l137 138q9 8 9.5 20.5t-7.5 22.5q-109 132-264 204.5T768 1536q-156 0-298-61t-245-164-164-245T0 768t61-298 164-245T470 61 768 0q147 0 284.5 55.5T1297 212l130-129q29-31 70-14 39 17 39 59z" />
        </svg>
      </motion.div>
    </div>
  ) : null;
}

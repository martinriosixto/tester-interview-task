import React, { useState, useEffect } from 'react';
import { TrialFormModel } from 'types';
import styles from './TrialForm.module.css';
import { AnimatePresence, motion } from 'framer-motion';
import { ButtonLarge } from '../Button';
import MaskedInput from 'react-text-mask';

import { classNames } from 'lib';

interface NameFieldset {
  setValue: Function;
  inputName: string;
  label: string;
  mask?: any;
  validateField: Function;
  errorText?: string;
}
const NameFieldset = ({
  setValue,
  inputName,
  label,
  mask,
  validateField,
  errorText,
  ...props
}: NameFieldset) => {
  return (
    <div>
      <fieldset
        className={classNames([
          styles.trailfield,
          errorText ? styles.error : '',
        ])}
      >
        <span
          className={errorText ? styles.extraBorder__error : styles.extraBorder}/>
        <legend
          className={errorText ? styles.triallegend__error : styles.triallegend}>
          {label}
        </legend>
        {mask ? (
          <MaskedInput
            mask={mask}
            guide={true}
            keepCharPositions={true}
            onChange={(e) => setValue(e.target.value)}
            onBlur={(e) => validateField()}
            className={classNames([
              styles.trailinput,
              styles.tokenfield,
              errorText ? styles.error : '',
            ])}
            type="text"
            id="tokenNumber"
            placeholderChar={'\u2000'}
          />
        ) : (
          <input
            onChange={(e) => setValue(e.target.value)}
            onBlur={(e) => validateField()}
            className={classNames([
              styles.trailinput,
              errorText ? styles.error : '',
            ])}
            type="text"
            id="tokenNumber"
            maxLength={45}
          />
        )}
      </fieldset>
      <p
        className={classNames([
          styles.helpertext,
          errorText ? styles.error : '',
        ])}
      >
        {errorText ? errorText : 'Required*'}
      </p>
    </div>
  );
};

interface RegistrationFormProps {
  handleFormChange: (payload: {
    isValid: boolean;
    TrialFormModel: TrialFormModel;
  }) => void;
  handleFormSubmit: () => void;
  nextEnabled: boolean;
  showLoadingDots: boolean;
  loadingDots?: string;
}

const RegistrationForm: React.FC<RegistrationFormProps> = ({
  handleFormChange,
  handleFormSubmit,
  nextEnabled = false,
  showLoadingDots = false,
  loadingDots = '.',
}) => {
  const [name, setName] = useState<string>();
  const [token, setToken] = useState<string>();
  const [nameError, setNameError] = useState<string>('');
  const [tokenError, setTokenError] = useState<string>('');
  const tokenMask = [ /\d/, /\d/,/\d/,/\d/, ' ','-',' ', /\d/, /\d/, /\d/,/\d/, ' ', '-', ' ', /\d/, /\d/,/\d/, /\d/,
  ];
  useEffect(() => {
    if (name && token && token.replace(/\s/g, '').length === 14) {
      handleFormChange({
        isValid: true,
        TrialFormModel: {
          token_id: token.replace(/\s/g, ''),
          username: name,
        },
      });
    }
  }, [token, name, handleFormChange, tokenMask.length, nameError, tokenError]);

  const submitForm = (e: any) => {
    e.preventDefault();
    handleFormSubmit();
  };

  const handleNameChange = (newName: string) => {
    setName(newName);
    if(nameError){
      validateName(newName);
    }
  };

  const handleTokenChange = (token: string) => {
    setToken(token);

    if(tokenError){
      validateToken(token);
    }
  };

  const validateName = (nameToValidate: string |undefined) => {
    setNameError(!nameToValidate ? 'Please enter a name' : '');
  };


  const validateToken = (tokenToValidate: string | undefined) => {
    if (tokenToValidate && tokenToValidate.replace(/\s/g, '').length === 14) {
      setTokenError('');
    } else {
      setTokenError('Please enter 12 numbers');
    }
  };

  return (
    <form noValidate className="flex flex-row w-full">
      <NameFieldset
        setValue={handleNameChange}
        inputName="user-name"
        label="Full name of child*"
        validateField={()=> validateName(name)}
        errorText={nameError}
      />
      <NameFieldset
        setValue={handleTokenChange}
        inputName="name"
        label="Token number*"
        mask={tokenMask}
        validateField={()=> validateToken(token)}
        errorText={tokenError}
      />
      <fieldset className={styles.trailbutton}>
        <AnimatePresence>
          {nextEnabled && (
            <motion.nav
              style={{ top: 220, right: 32, transition: 'all ease-in-out' }}
              initial={{ translateY: 1024 }}
              animate={{ translateY: 0 }}
              transition={{ ease: 'easeInOut', duration: 0.4, delay: 0.64 }}
            >
              <div className="w-40">
                <ButtonLarge
                  onClick={(e) => submitForm(e)}
                  text={showLoadingDots ? loadingDots : 'Next'}
                />
              </div>
            </motion.nav>
          )}
        </AnimatePresence>
      </fieldset>
    </form>
  );
};

export default RegistrationForm;

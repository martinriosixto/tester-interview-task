import React, { useEffect, useState } from 'react';
import useFullScreen from '../../hooks/useFullscreen';
import Overlay from 'components/Overlay';
import { Card } from 'components/Card';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';

function _isValidResolution() {
  return window.matchMedia(
    'screen and (min-width: 1024px) and (min-height: 700px)'
  ).matches;
}

export default function ValidateResolution() {
  const { isFullScreen } = useFullScreen();
  const [isError, setIsError] = useState(false);
  const { recordEvent } = useAnalytics();

  useEffect(() => {
    function handleResize() {
      setIsError(!_isValidResolution());
      if (isError) {
        recordEvent(ANALYTIC_EVENT.error_screen_resolution);
      }
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [isError, recordEvent]);

  //Only validate if screen is too small when we are in the full screen mode
  return !process.env.REACT_APP_DEV_MODE && isFullScreen && isError ? (
    <Overlay>
      <Card>This screen is too small.</Card>
    </Overlay>
  ) : null;
}

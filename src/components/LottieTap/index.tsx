import React from 'react';
import Lottie from 'react-lottie';

import animationData from 'images/lotties/tap.json';

interface LottieTapProps {
  width?: number;
  height?: number;
  top?: number;
  left?: number;
  right?: number;
  bottom?: number;
  position?: any;
}

const LottieTap: React.FC<LottieTapProps> = ({
  width = 248,
  height = '100%',
  top,
  left,
  bottom,
  right,
  position = 'absolute',
}) => {
  const options = {
    loop: true,
    autoplay: true,
    animationData: animationData,
  };

  return (
    <Lottie
      style={{
        position: position,
        zIndex: 10,
        pointerEvents: 'none',
        top: top ? `${top}px` : undefined,
        left: left ? `${left}px` : undefined,
        bottom: bottom ? `${bottom}px` : undefined,
        right: right ? `${right}px` : undefined,
      }}
      options={options}
      width={width}
      height={height}
    />
  );
};

export default LottieTap;

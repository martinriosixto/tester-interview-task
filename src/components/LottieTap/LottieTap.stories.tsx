import React from 'react';

import LottieTap from './index';

export default {
  title: 'Lottie Tap',
  component: [LottieTap],
};

export const Example = () => (
  <div className="m-8">
    <LottieTap />
  </div>
);

import React from 'react';
import { Card } from './Card';
import Overlay from './Overlay';

const AskForHelpOverlay = () => (
  <Overlay>
    <Card>Please ask for help.</Card>
  </Overlay>
);

export default AskForHelpOverlay;

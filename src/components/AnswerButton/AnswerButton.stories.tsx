import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import {
  AnswerButtonLarge,
  AnswerButtonSmall,
  ImageAnswerButtonSmall,
} from './index';

export default {
  title: 'Answer Buttons',
  component: [AnswerButtonLarge, AnswerButtonSmall],
  decorators: [withKnobs],
};

export const AnswerCards = () => (
  <div>
    <div className="m-8">
      <AnswerButtonLarge showLottieTap={true}>
        Choose a picture
      </AnswerButtonLarge>
    </div>

    <div className="m-8">
      <AnswerButtonLarge isSelected={true}>Choose a picture</AnswerButtonLarge>
    </div>

    <div className="m-8">
      <AnswerButtonSmall>Choose a picture</AnswerButtonSmall>
    </div>

    <div className="m-8">
      <AnswerButtonSmall showLottieTap={true}>
        Choose a picture
      </AnswerButtonSmall>
    </div>

    <div className="m-8">
      <ImageAnswerButtonSmall
        isDisabled={true}
        imgSrc="http://placekitten.com/g/240/240"
        imgAlt=""
      >
        Choose a picture
      </ImageAnswerButtonSmall>
    </div>
  </div>
);

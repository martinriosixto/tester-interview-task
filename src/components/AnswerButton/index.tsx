import React from 'react';
import styles from './AnswerButton.module.css';
import { classNames } from 'lib';
import LottieTap from 'components/LottieTap';

interface AnswerButtonProps {
  children?: React.ReactNode;
  isSelected?: boolean;
  isDisabled?: boolean;
  onClick?: () => void;
  showLottieTap?: boolean;
}

interface ImageAnswerButtonProps extends AnswerButtonProps {
  imgSrc: string;
  imgAlt: string;
}

interface AnswerButtonPropsWithSize extends AnswerButtonProps {
  size: 'Large' | 'Small';
}

export const AnswerButtonLarge: React.FC<AnswerButtonProps> = (props) => (
  <AnswerButton size="Large" {...props} />
);

export const AnswerButtonSmall: React.FC<AnswerButtonProps> = (props) => (
  <AnswerButton size="Small" {...props} />
);

export const ImageAnswerButtonSmall: React.FC<ImageAnswerButtonProps> = (
  props
) => (
  <AnswerButton size="Small" {...props}>
    <img
      src={props.imgSrc}
      alt={props.imgAlt}
      className={classNames([
        styles.image_content,
        props.isDisabled ? styles.image_content__disabled : '',
      ])}
    />
  </AnswerButton>
);

export const AnswerButton: React.FC<AnswerButtonPropsWithSize> = ({
  children,
  isSelected = false,
  isDisabled = false,
  onClick,
  showLottieTap = false,
  size = 'Large',
}) => {
  const isLarge = size === 'Large';
  return (
    <button
      disabled={isDisabled}
      className={classNames([
        styles.answerCard,
        isLarge ? '' : styles.small,
        isSelected ? styles.selected : '',
        isDisabled ? styles.disabled : '',
      ])}
      onClick={onClick}
    >
      {children}
      {showLottieTap && <LottieTap top={isLarge ? 162 : 122} />}
    </button>
  );
};

import React from 'react';
import { Link } from 'react-router-dom';

export type ButtonTheme = 'primary' | 'secondary';
export type ButtonSize = 'normal' | 'large';

interface Props {
  to: string;
  disabled?: boolean;
  theme?: ButtonTheme;
  size?: ButtonSize;
}

const ButtonLink: React.FC<Props> = ({
  to,
  disabled = false,
  theme,
  size = 'normal',
  children
}) => {
  if (theme === 'secondary') {
    return (
      <Link
        to={to}
        className={`inline-flex justify-center py-2 bg-white border-2 border-primary text-white font-bold w-64 rounded-lg shadow-lg ${
          size === 'normal' ? 'px-4' : 'w-64'
        }`}
      >
        {children}
      </Link>
    );
  }

  const primaryClass = `inline-flex justify-center py-2 bg-primary text-xl text-white font-bold rounded-lg shadow-lg ${
    size === 'normal' ? 'px-4' : 'w-64'
  }`;

  return disabled ? (
    <span className={primaryClass}>{children}</span>
  ) : (
    <Link to={to} className={primaryClass}>
      {children}
    </Link>
  );
};

export default ButtonLink;

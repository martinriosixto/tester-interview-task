import React from 'react';

const Overlay: React.FC = ({ children }) => (
  <div className="fixed h-full w-full z-50 bg-overlay flex items-center justify-center">
    {children}
  </div>
);

export default Overlay;

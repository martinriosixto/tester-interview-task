import React from 'react';
import { motion } from 'framer-motion';

import styles from './OnlineMessagesComponents.module.css';

import { ReactComponent as Camera } from 'images/tasks/camera.svg';
import { ReactComponent as Image } from 'images/tasks/gallery.svg';
import { FADE } from './AnimatedComponents';

type IconType = 'Image' | 'Camera';

interface ActionIconProps {
  icon: IconType;
  disabled?: boolean;
  handleClick: () => void;
}

const MenuBar: React.FC = ({ children }) => (
  <nav className={styles.menuBar}>{children}</nav>
);

export const ActionIcon: React.FC<ActionIconProps> = ({
  icon,
  disabled,
  handleClick,
}) => (
  <motion.button
    initial="hidden"
    animate="visible"
    variants={FADE}
    className={disabled ? 'pointer-events-none' : 'outline-none'}
    onClick={() => handleClick()}
  >
    <Icon icon={icon} disabled={disabled} />
  </motion.button>
);

const Icon = ({ icon, disabled }: { icon: IconType; disabled?: boolean }) => (
  <React.Fragment>
    {icon === 'Camera' && (
      <Camera opacity={disabled ? 0.5 : 1} fill="#8031A7" />
    )}
    {icon === 'Image' && <Image opacity={disabled ? 0.5 : 1} fill="#8031A7" />}
  </React.Fragment>
);

export default MenuBar;

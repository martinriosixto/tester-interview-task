import React, { useEffect, useState } from 'react';
import { TaskProps, ItemAnswer, OnlineMessagesItem } from 'types';
import { sleep } from 'lib';

import AnimatedBackground from 'components/AnimatedBackground';
import LottieTap from 'components/LottieTap';

import Phone from './Phone';
import Chat from './Chat';
import Message from './Message';
import MenuBar, { ActionIcon } from './MenuBar';
import Answers from './Answers';
import { AnimatePresence } from 'framer-motion';

const OnlineMessages: React.FC<TaskProps> = ({
  step,
  userTask,
  itemAnswer,
  item,
  handleSetItemAnswer,
  moveToTheNextStep,
}) => {
  let OMItem = item as OnlineMessagesItem;

  const [showQuestion, setShowQuestion] = useState<boolean>(false);
  const [showStimulus, setShowStimulus] = useState<boolean>(false);
  const [showAnswersToggle, setShowAnswersToggle] = useState<boolean>(false);
  const [showAnswers, setShowAnswers] = useState(false);
  const [showSubmitButton, setShowSubmitButton] = useState<boolean>(false);
  const [isFinalAnswer, setIsFinalAnswer] = useState<boolean>(false);

  const style = {
    backgroundImage: `url(${userTask.background_image?.src})` ?? null,
  };

  useEffect(() => {
    if (!step.id) return;
    // Switch on & stay visible
    step.id === 'SHOW_QUESTION' && setShowQuestion(true);
    step.id === 'SHOW_STIMULUS' && setShowStimulus(true);
    step.id === 'SHOW_ANSWERS_TOGGLE' && setShowAnswersToggle(true);
  }, [step]);

  const handleShowAnwers = () => {
    setIsFinalAnswer(false);
    setShowAnswers(true);
  };

  const handleSetAnswer = (itemAnswer: ItemAnswer[]) => {
    setShowStimulus(true);
    handleSetItemAnswer(itemAnswer);
    setShowAnswers(false);
    setShowSubmitButton(true);
  };

  const handleSend = async () => {
    setShowStimulus(false);
    setIsFinalAnswer(true);
    await sleep(2000);
    step.id !== 'SHOW_FINISH_BUTTON' && moveToTheNextStep();
  };

  return (
    <AnimatedBackground style={style} isExample={item.is_example}>
      <section className="flex h-full">
        <Phone>
          <Chat
            showQuestion={showQuestion}
            showResponse={isFinalAnswer}
            question={OMItem.system_question}
            stimulus={OMItem.user_answer_stimulus_text}
            answer={itemAnswer}
          />
          <Message
            showStimulus={showStimulus}
            showSubmitButton={showSubmitButton}
            clearMessage={isFinalAnswer}
            isExample={OMItem.is_example}
            stimulus={OMItem.user_answer_stimulus_text}
            handleSubmit={handleSend}
            answer={itemAnswer}
          />
          <MenuBar>
            {showAnswersToggle && (
              <ActionIcon
                icon={OMItem.icon_indicator}
                disabled={OMItem.is_example && showSubmitButton}
                handleClick={handleShowAnwers}
              />
            )}
            {showAnswersToggle && OMItem.is_example && !showSubmitButton && (
              <LottieTap top={-8} height={124} width={124} />
            )}
          </MenuBar>
        </Phone>
        <AnimatePresence>
          {showAnswers && (
            <Answers
              isExample={OMItem.is_example}
              stimulus={OMItem.user_answer_stimulus_text}
              handleSelect={handleSetAnswer}
              answers={OMItem.answers}
            />
          )}
        </AnimatePresence>
      </section>
    </AnimatedBackground>
  );
};

export default OnlineMessages;

import React from 'react';
import { ItemAnswer } from 'types';

import { AnswersBlock } from './AnimatedComponents';
import { ImageAnswerButtonSmall } from 'components/AnswerButton';

interface AnswerProps {
  isExample?: boolean;
  stimulus: string;
  handleSelect: (itemAnswer: ItemAnswer[]) => void;
  answers: ItemAnswer[];
}

const Answer: React.FC<AnswerProps> = ({
  isExample,
  stimulus,
  answers,
  handleSelect,
}) => (
  <AnswersBlock>
    <ol className="w-full flex items-center justify-center space-x-12">
      {answers.map((a) => (
        <li key={a.answer_id} onClick={() => handleSelect([a])}>
          {a.image?.src && (
            <ImageAnswerButtonSmall
              showLottieTap={a.is_correct_answer && isExample}
              imgSrc={a.image.src}
              imgAlt={`Answer option ${a.order}`}
              isDisabled={isExample && !a.is_correct_answer}
            />
          )}
        </li>
      ))}
    </ol>
  </AnswersBlock>
);

export default Answer;

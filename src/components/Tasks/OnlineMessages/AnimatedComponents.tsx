import React from 'react';
import { motion } from 'framer-motion';
import { classNames } from 'lib';

import styles from './OnlineMessagesComponents.module.css';

export const FADE = {
  hidden: { opacity: 0 },
  visible: { opacity: 1 },
};

export const ChatItem = ({
  children,
  response,
}: {
  children: React.ReactNode;
  response?: boolean;
}) => (
  <motion.li
    initial={{ opacity: 0, translateY: '100%' }}
    animate={{ opacity: 1, translateY: '0%' }}
    transition={{ ease: 'easeOut', duration: 0.64 }}
    className={classNames(['flex items-end', response ? 'justify-end' : ''])}
    layout
  >
    {children}
  </motion.li>
);

export const PhoneContainer = ({
  children,
  blur = false,
}: {
  children?: React.ReactNode;
  blur: boolean;
}) => (
  <motion.article
    initial="hidden"
    animate="visible"
    variants={FADE}
    transition={{ ease: 'easeOut', duration: 0.64 }}
    className={styles.phoneContainer}
    style={blur ? { filter: 'blur(16px)' } : {}}
  >
    {children}
  </motion.article>
);

export const MessageCopy: React.FC = ({
  children,
}: {
  children?: React.ReactNode;
}) => (
  <motion.p
    initial="hidden"
    animate="visible"
    variants={FADE}
    transition={{ ease: 'easeOut', duration: 0.64 }}
    className={styles.messageContainer__message}
  >
    {children}
  </motion.p>
);

export const AnswersBlock: React.FC = ({ children }) => (
  <motion.section
    initial={{ translateY: 304 }}
    animate={{ translateY: 0 }}
    exit={{ translateY: 304 }}
    transition={{ ease: 'easeInOut', duration: 0.64 }}
    className={styles.answerContainer}
  >
    {children}
  </motion.section>
);

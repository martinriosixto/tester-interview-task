import React from 'react';
import { AnimatePresence } from 'framer-motion';

import { ItemAnswer } from 'types';
import { classNames } from 'lib';

import styles from './OnlineMessagesComponents.module.css';

import { ReactComponent as Avatar } from 'images/tasks/portrait.svg';
import { ChatItem } from './AnimatedComponents';

interface ChatProps {
  showQuestion: boolean;
  showResponse: boolean;
  question: string;
  stimulus: string;
  answer?: (ItemAnswer | null)[];
}

const Chat: React.FC<ChatProps> = ({
  showQuestion,
  showResponse,
  question,
  stimulus,
  answer,
}) => (
  <section className={styles.chatContainer}>
    <ul>
      <AnimatePresence>
        {showQuestion && (
          <ChatItem key="1">
            <React.Fragment>
              <Avatar fill="#04A0FA" />
              <p className={styles.chatContainer__bubble}>{question}</p>
            </React.Fragment>
          </ChatItem>
        )}
        {showResponse && answer && (
          <ChatItem response key="2">
            <p
              className={classNames([
                styles.chatContainer__bubble,
                styles.response,
              ])}
            >
              {stimulus}
              <img
                className="m-auto pt-3 h-16"
                src={answer[0]?.image?.src}
                alt={`Answer option ${answer[0]?.order}`}
              />
            </p>
            <Avatar fill="#F4BB32" />
          </ChatItem>
        )}
      </AnimatePresence>
    </ul>
  </section>
);

export default Chat;

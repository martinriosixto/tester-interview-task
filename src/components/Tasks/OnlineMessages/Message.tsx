import React from 'react';
import { ItemAnswer } from 'types';
import { classNames } from 'lib';

import LottieTap from 'components/LottieTap';

import styles from './OnlineMessagesComponents.module.css';

import { ReactComponent as SubmitIcon } from 'images/tasks/keyboard-return.svg';
import { MessageCopy } from './AnimatedComponents';

interface MessageProps {
  showStimulus: boolean;
  showSubmitButton: boolean;
  clearMessage?: boolean;
  isExample?: boolean;
  stimulus: string;
  handleSubmit: () => void;
  answer?: (ItemAnswer | null)[];
}

const Message: React.FC<MessageProps> = ({
  showStimulus,
  showSubmitButton,
  clearMessage,
  isExample,
  stimulus,
  handleSubmit,
  answer,
}) => (
  <section className={styles.messageContainer}>
    {showStimulus && <MessageCopy>{stimulus}</MessageCopy>}
    {showSubmitButton && (
      <React.Fragment>
        {!clearMessage && (
          <img
            className={styles.messageContainer__attachment}
            src={answer && answer[0]?.image?.src}
            alt="Online message attachmet"
          />
        )}
        <button
          className={classNames([
            'absolute right-0 bottom-0 outline-none',
            clearMessage ? 'pointer-events-none opacity-50' : '',
          ])}
          onClick={handleSubmit}
        >
          <SubmitIcon fill="#8031A7" />
        </button>
        {isExample && !clearMessage && (
          <LottieTap bottom={-46} width={124} right={-36} />
        )}
      </React.Fragment>
    )}
  </section>
);

export default Message;

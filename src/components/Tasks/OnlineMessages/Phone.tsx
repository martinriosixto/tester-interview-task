import React from 'react';

import { ReactComponent as PhoneBG } from 'images/tasks/phone.svg';
import { PhoneContainer } from './AnimatedComponents';

const Phone = ({
  children,
  blur = false,
}: {
  children?: React.ReactNode;
  blur?: boolean;
}) => (
  <PhoneContainer blur={blur}>
    {children}
    <PhoneBG className="absolute top-0 right-0 bottom-0 left-0 z-0 w-full" />
  </PhoneContainer>
);

export default Phone;

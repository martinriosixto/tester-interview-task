import React from 'react';
import useTimeout from 'hooks/useTimeout';

interface WithTimeoutProps {
  duration: number;
  callback: () => void;
}
const WithTimeout: React.FC<WithTimeoutProps> = ({ duration, callback }) => {
  useTimeout(callback, duration);
  return null;
};

export default WithTimeout;

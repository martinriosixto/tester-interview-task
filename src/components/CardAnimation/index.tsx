import React from 'react';
import { motion } from 'framer-motion';

const leftStart = (1024 - 278) / 2;
const bottomStart = (768 - 140) / 2;

const CardAnimation: React.FC = ({ children, ...props }) => (
  <motion.div
    initial={{
      opacity: 0,
      left: `${leftStart}px`,
      bottom: `${bottomStart}px`,
    }}
    animate={{
      opacity: 1,
      left: '80px',
      bottom: '120px',
    }}
    exit={{ opacity: 0 }}
    transition={{ ease: 'easeInOut', duration: 0.64, delay: 0.64 }}
    className="absolute z-40"
    {...props}
  >
    {children}
  </motion.div>
);

export default CardAnimation;

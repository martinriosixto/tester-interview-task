import { motion } from 'framer-motion';
import React, { useState } from 'react';

interface AnimatedBackgroundProps {
  children?: React.ReactNode;
  style?: any;
  isExample?: boolean;
  requireAnimationComplete?: boolean;
}

const AnimatedBackground: React.FC<AnimatedBackgroundProps> = ({
  children,
  style,
  isExample = false,
  requireAnimationComplete = true,
}) => {
  const [animationComplete, setAnimationComplete] = useState<boolean>(false);

  return (
    <motion.div
      className="flex flex-col h-full w-full bg-cover"
      initial={{
        left: isExample ? '100%' : 'auto',
        position: 'relative',
        opacity: 0,
      }}
      animate={{ left: 'auto', opacity: 1 }}
      transition={{ ease: 'easeInOut', duration: 1 }}
      onAnimationComplete={() => setAnimationComplete(true)}
      style={style}
    >
      {((requireAnimationComplete && animationComplete) ||
        !requireAnimationComplete) &&
        children}
    </motion.div>
  );
};

export default AnimatedBackground;

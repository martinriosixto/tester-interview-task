import { ItemAnswer } from 'types';

type TaskTypeId =
  | 'minimal_differences'
  | 'labeling'
  | 'scene_creation'
  | 'picture_editing'
  | 'dialogue'
  | 'information_posters'
  | 'spelling'
  | 'online_messages'
  | 'comic_story'
  | 'factual_topic'
  // spelling doesnt have true task types
  | 'meet_and_greet';

// Speaking simple task
type ItemTypeId = 'SIMPLE';

export interface User {
  age: number;
  avatar?: string;
  nationality?: string;
  user_id: string;
  username: string;
  token_id: string;
  name: string;
}

export interface Test {
  skill_id: SkillId;
  test_by_user_id: string;
  status: string;
  test_id: string;
  user_id: string;
  tasks?: Task[];
}

export interface TaskProps {
  step: ItemStep;
  item: TaskItem;
  userTask: UserTask<TaskItem>;
  itemAnswer: ItemAnswers;
  taskAnswers?: UserTaskAnswers;
  handleSetItemAnswer: (itemAnswer: ItemAnswers) => void;
  moveToTheNextStep: () => void;
}

// ToDo: Do these need consolidating under the new context

//  THIS IS TASK
export interface UserTask<TaskItem> {
  task_type_id: TaskTypeId;
  items: T[];
  itemz?: T[];
  background_image?: {
    src: string;
  };
  task_id: string;
  colors?: string[];
  is_introductory?: boolean;
  final_audio?: {
    src: string;
  };
}

type TaskItem =
  | MinimalDifferencesItem
  | LabellingItem
  | SceneCreationItem
  | DialogueItem
  | PictureEditingItem
  | InformationPostersItem
  | OnlineMessagesItem
  | ComicStoryItem
  | FactualTopicItem;

type AnswerType = 'text' | 'image';

export interface Task {
  background_image: {
    src: string | null;
  };
  order: number;
  status: string;
  task_by_user_id: string;
  task_id: string;
  user_id: string;
  task_type_id: TaskTypeId;
  item_count: number;
  colors?: string[];
}

// ToDo: ItemAnswer now diverges - labelling is the only one with display_order
export interface ItemAnswer {
  item_id?: string;
  answer_id: string;
  display_order?: number;
  image?: {
    src: string;
    placement_image_src?: string;
    coords?: {
      x?: number;
      y?: number;
    };
  };
  text?: string;
  is_correct_answer?: boolean;
  order?: number;
  audio?: ItemAudio;
  coordinates?: string[];
  coordinate?: { top: number; left: number };
  color_hex_value?: string;
  stimulus?: string[];
  key?: string;
  user_answer_string?: string;
  is_capital?: boolean;
  stimulus_text?: string;
}

export interface LabellingItem extends Item {
  stimulus_text_1: string;
  stimulus_text_2: string;
  boxes: number;
}

export interface MinimalDifferencesItem extends Item {
  answer_type: AnswerType;
}

export interface DialogueItem extends Item {}

export interface PictureEditingItem extends Item {
  phase: 'PHASE_1' | 'PHASE_2';
}

export interface SceneCreationItem extends Item {
  background_image?: {
    src: string;
  };
}

export interface InformationPostersItem extends Item {
  stimulus_text: string;
}

export interface OnlineMessagesItem extends Item {
  system_question: string;
  user_answer_stimulus_text: string;
  icon_indicator: 'Image' | 'Camera';
}

export interface ComicStoryItem extends Item {
  stimulus_text: string;
  question_1?: string;
  question_2?: string;
  is_capital?: boolean;
}

export interface FactualTopicItem extends Item {
  dnd_answers?: string[];
  phase: 'PHASE_1' | 'PHASE_2';
}

export interface ItemAudio {
  src: string;
}

export interface Item {
  item_id: string;
  order: number;
  audio?: ItemAudio;
  rubric?: Rubric;
  answers: ItemAnswer[];
  show_rubric?: boolean;
  is_example?: boolean;
  image?: {
    src: string;
  };
}

export interface Rubric {
  rubric_audio: {
    src: string;
  };
  rubric_text: string;
  helper_audio: {
    src?: string;
  };
  helper_text?: string;
}

export type ItemStepId =
  | 'SHOW_BACKGROUND'
  | 'SHOW_ANSWERS_ON_BACKGROUND'
  | 'SHOW_RUBRIC'
  | 'SHOW_RUBRIC_WITH_OVERLAY'
  | 'SHOW_EXAMPLE'
  | 'CYCLE_THRU_ITEM_ANSWERS'
  | 'SHOW_HELPER_ANIMATION'
  | 'SHOW_CUSTOM_HELPER'
  | 'SHOW_ANSWERS'
  | 'SHOW_NAVIGATION'
  | 'SHOW_RECORD_BUTTON'
  | 'SHOW_QUESTION'
  | 'SHOW_STIMULUS'
  | 'SHOW_ASSETS'
  | 'PLAY_AUDIO_CANNOT_SELECT'
  | 'PLAY_AUDIO_CAN_SELECT'
  | 'USER_SELECT_FIRST_TIME'
  | 'CUSTOM_USER_SELECT_FIRST_TIME'
  | 'CUSTOM_EXAMPLE_USER_SELECT_FIRST_TIME'
  | 'SHOW_LISTEN_AGAIN'
  | 'PLAY_LISTEN_AGAIN_AUDIO'
  | 'SHOW_ANSWERS_TOGGLE'
  | 'SHOW_FINISH_BUTTON'
  | 'POST_FINISH_STEP'
  | 'SHOW_STAR'
  | 'SHOW_STAR_FOR_INTRO'
  | 'WAIT_FOR_EXAMPLE_TO_BE_COMPLETED'
  | 'SHOW_OVERLAY'
  | 'HIDE_OVERLAY'
  | 'HIDE_STAR_OVERLAY'
  | 'SHOW_CUSTOM_NAVIGATION'
  | 'WAIT_TO_SHOW_FINISH'
  | 'SHOW_INPUTS_SPECIFIC'
  | 'SHOW_KEYBOARD'
  | 'DROP_OPACITY_WRONG_ANSWERS';

export interface ItemStep {
  id: ItemStepId;
  canSelectAnswer: boolean;
  transitionTime?: number;
  navVisible: boolean;
  showProgress: boolean;
  bringProgressForward: boolean;
  overlayVisible: boolean;
  answersOverlayVisible: boolean;
  showRubricHelpButton: boolean;
}

export interface Country {
  alpha3Code: string;
  name: string;
}

export interface RegistrationFormModel {
  username: string;
  nationality: string;
  age: number;
}

export interface TrialFormModel {
  token_id: string;
  username: string;
}

export type Theme = {
  toneIndex: number;
  headIndex: number;
  headwearIndex?: number | null;
  facewearIndex?: number | null;
  bodyIndex: number;
  holdingIndex?: number | null;
  backwearIndex?: number | null;
  legsIndex: number;
  footwearIndex?: number | null;
};

export type SurveyResults = {
  Q1?: string | null;
  Q2?: string | null;
  Q3?: string | null;
  Q4?: string | null;
  Q5?: string | null;
};

// used by the store
export type State = {
  user?: User;
  isLoading: boolean;
  skill_id: SkillId;
  theme: Theme;
  error?: string;
  examplesSeen: string[];
  userTask?: UserTask<TaskItem>;
  taskAnswers?: UserTaskAnswers;
  test_by_user_id?: string;
  task_by_user_id?: string;
  userTasks?: UserTasks;
  currentTaskIndex: number;
  currentItemIndex: number;
  needsToBeInFullscreen: boolean;
  surveyResults: SurveyResults;
  answersToResend: any[];
  analyticsToResend: any[];
  debug?: boolean;
};

type ItemAnswers = (ItemAnswer | null)[];
type UserTaskAnswers = (ItemAnswer | null)[][];

type UserTasks = {
  task: Task;
  userTask?: UserTask<TaskItem>;
}[];

type SkillId = 'listening' | 'reading_writing' | 'speaking';

interface DYLAudioElement extends HTMLAudioElement {
  dylAudioLoaded?: boolean;
}

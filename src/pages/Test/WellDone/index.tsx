import React, { useContext, useEffect, useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';

import { AppContext } from 'context/App';
import { randomiseBackground } from 'images/backgrounds';
// import Avatar from 'components/AvatarBase';
// import MonkeyBase from 'pages/Test/Map/MonkeyBase';
import useFullScreen from 'hooks/useFullscreen';
import { OkButton } from 'components/Button';
import Overlay from 'components/Overlay';
import { Card } from 'components/Card';
import { api } from 'lib';
import useDylNavigate from 'hooks/useDylNavigate';

const BGImage = randomiseBackground();

const ScreenWrapper: React.FC = ({ children }) => {
  return (
    <AnimatePresence>
      <motion.article
        initial={{ translateX: '100vw' }}
        animate={{ translateX: '0' }}
        exit={{ translateY: '-100vw' }}
        transition={{ delay: 0.64, duration: 1, ease: 'easeInOut' }}
        className="flex bg-cover justify-center h-full w-full flex-col "
        style={{ backgroundImage: `url(${BGImage})` }}
      >
        {children}
      </motion.article>
    </AnimatePresence>
  );
};

const WellDone = () => {
  const { state, dispatch } = useContext(AppContext);
  const { closeFullScreen } = useFullScreen();

  const [showWaitModal, setShowWaitModal] = useState(
    state.answersToResend.length > 0
  );

  const { navigate } = useDylNavigate();

  const handleNextButtonClick = () => {
    dispatch({ type: 'NEEDS_TO_BE_IN_FULL_SCREEN', payload: false });
    closeFullScreen();
    navigate('/test/item/0');
  };

  useEffect(() => {
    async function run() {
      await api.resendAnswers(state.answersToResend);
      setShowWaitModal(false);
    }

    run();
  }, [dispatch, showWaitModal, state.answersToResend]);

  const skillText =
    state.skill_id === 'listening'
      ? 'Listening'
      : state.skill_id === 'speaking'
      ? 'Speaking'
      : 'Reading and Writing';

  return (
    <ScreenWrapper>
      <section className="text-center flex-initial pt-32">
        <h1 className="text-4xl font-bold block">Well done!</h1>
        <p className="text-2xl">This is the end of the {skillText} test.</p>
      </section>
      <div className="flex flex-col justify-center items-center flex-1">
        <div className="flex">
          {/* <Avatar theme={state.theme} />
          <MonkeyBase /> */}
        </div>
        <div className="width-auto">
          <OkButton onClick={handleNextButtonClick} />
        </div>
      </div>
      {(!navigator.onLine || showWaitModal) && (
        <Overlay>
          <Card>Please don’t move away from this tab.</Card>
        </Overlay>
      )}
    </ScreenWrapper>
  );
};

export default WellDone;

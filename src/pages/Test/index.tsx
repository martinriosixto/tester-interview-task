import React, { useContext } from 'react';
import Item from 'pages/Test/Item';
import WellDone from './WellDone';
import { Routes, Route } from 'react-router-dom';
import OpenFullScreen from 'components/OpenFullScreen';
import { AppContext } from 'context/App';
import useMountEffect from 'hooks/useMountEffect';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import useDisableRightClick from 'hooks/useDisableRightClick';
import useHandleReload from 'hooks/useHandleReload';
import useDylNavigate from 'hooks/useDylNavigate';

const NoItemId = () => {
  return <div>No item Number :(</div>;
};

function Test() {
  const { recordEvent } = useAnalytics();
  const { state } = useContext(AppContext);
  const { navigateHome } = useDylNavigate();

  useMountEffect(() => {
    if (!state.user) {
      recordEvent(ANALYTIC_EVENT.redirect_no_user);
      navigateHome();
    }
  });
  useHandleReload();
  useDisableRightClick();

  return !state.user ? null : (
    // ToDo: Move page in right animation up here & out of other screens
    <>
      <OpenFullScreen />
      <div className="dyl__container flex justify-center items-center w-full h-full">
        {/* {SHOW_DEBUG && <Debug />} */}
        <Routes>
          <Route path="/welldone" element={<WellDone />} />
          <Route path="/item/:itemId" element={<Item />} />
          <Route path="/item/*" element={<NoItemId />} />
        </Routes>
      </div>
    </>
  );
}

export default Test;

import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { AppContext } from 'context/App';
import ItemComponent from 'components/Item';
import { ItemAnswer, Item as ItemType } from 'types';
import { api, audio, apiMappers, itemCreator } from 'lib';
import IconForward from 'icons/IconForward';
import { ButtonSmall } from 'components/Button';
import { AudioContext } from 'context/Audio';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import * as Sentry from '@sentry/react';
import { getItemAnalyticInfo } from 'lib/analytics';
import useDylNavigate from 'hooks/useDylNavigate';

const Item: React.FC = () => {
  let { itemId } = useParams();
  const _itemIndex = itemId === undefined ? 0 : +itemId;
  const { state, dispatch } = useContext(AppContext);
  const { recordEvent } = useAnalytics();

  const { navigate } = useDylNavigate();
  const { loadAudio } = useContext(AudioContext);

  // for iPad going directly to an item won't work for security reasons
  const [needsUserInteraction, setNeedsUserInteraction] = useState(false);

  const { userTask, taskAnswers } = state;
  const item: ItemType = userTask?.items[_itemIndex];

  useEffect(() => {
    dispatch({ type: 'SET_CURRENT_ITEM_INDEX', payload: _itemIndex });
  }, [_itemIndex, dispatch]);

  if (!userTask || !item) {
    return null;
  }

  Sentry.setTag('task', userTask.task_type_id);
  Sentry.setContext('task', {
    item_id: item.item_id,
    user_id: state.user?.user_id,
    task_id: userTask.task_id,
    task_type_id: userTask.task_type_id,
  });

  if (needsUserInteraction) {
    loadAudio(audio.getAudioLinks(userTask));
    return (
      <ButtonSmall
        onClick={() => setNeedsUserInteraction(false)}
        Icon={IconForward}
      />
    );
  }

  const steps = itemCreator.createItemSteps(item, userTask.task_type_id);
  const completedCallback = async (itemAnswers: (ItemAnswer | null)[]) => {
    if (!userTask) {
      return;
    }

    if (userTask?.is_introductory) {
      return navigate('/test/map');
    }

    recordEvent(ANALYTIC_EVENT.item_complete, getItemAnalyticInfo(item));

    let taskAnswers = [...(state.taskAnswers || []), itemAnswers];

    dispatch({
      type: 'UPDATE_TASK_ANSWERS',
      payload: {
        taskAnswers,
      },
    });

    const nextItemIndex = _itemIndex + 1;

    if (nextItemIndex < userTask?.items.length) {
      navigate(`../${nextItemIndex}`);
    } else {
      const { user, task_by_user_id, test_by_user_id } = state;
      const payload = apiMappers.mapItemToApi(userTask, taskAnswers);
      const postAnswersRequest = {
        body: {
          items: payload,
        },
        task_by_user_id: task_by_user_id || '',
        test_by_user_id: test_by_user_id || '',
        user_id: user?.user_id || '',
        token_id: user?.token_id || '',
      };

      api.postAnswers(postAnswersRequest).then(({ error }) => {
        if (error) {
          dispatch({
            type: 'SET_ANSWERS_TO_RESEND',
            payload: [...state.answersToResend, postAnswersRequest],
          });
        }
      });

      dispatch({
        type: 'SET_CURRENT_TASK_INDEX',
        payload: state.currentTaskIndex + 1,
      });

      navigate('../../welldone');
    }
  };

  return (
    <ItemComponent
      key={item.item_id}
      itemIndex={_itemIndex}
      steps={steps}
      item={item}
      taskAnswers={taskAnswers}
      userTask={userTask}
      completedCallback={completedCallback}
    />
  );
};

export default Item;

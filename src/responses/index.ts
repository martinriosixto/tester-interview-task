import { UserTask, Item } from 'types';
import online_messages from './online_messages.json';

// @ts-ignore
export const online_messages_task_response: UserTask<Item> = online_messages;

import { AppContext } from 'context/App';
import { useContext } from 'react';
import { analytics } from 'lib';

export enum ANALYTIC_EVENT {
  load_page = 'load_page',
  page_reload = 'page_reload',
  item_start = 'item_start',
  item_complete = 'item_complete',
  avatar_skintone = 'avatar_skintone',
  avatar_item = 'avatar_item',
  avatar_selected = 'avatar_selected',
  select_next = 'select_next',
  select_start = 'select_start',
  select_ok = 'select_ok',
  select_help = 'select_help',
  select_finish = 'select_finish',
  select_listen_again = 'select_listen_again',
  select_answer = 'select_answer',
  select_try_again = 'select_try_again',
  audio_rubric_complete = 'audio_rubric_complete',
  audio_example_complete = 'audio_example_complete',
  audio_helper_complete = 'audio_helper_complete',
  audio_listen_again_complete = 'audio_listen_again_complete',
  audio_stimulus_complete = 'audio_stimulus_complete',
  redirect_no_user = 'redirect_no_user',
  error_audio_play = 'error_audio_play',
  error_screen_resolution = 'error_screen_resolution',
}

export const useAnalytics = () => {
  const { state, dispatch } = useContext(AppContext);
  const { user, userTask, currentTaskIndex } = state;

  function recordEvent(eventName: ANALYTIC_EVENT, extra?: any) {
    const analyticPayload = {
      user_id: user?.user_id,
      task_type_id: userTask?.task_type_id,
      task_id: userTask?.task_id,
      task_index: currentTaskIndex,
      event_name: eventName,
      timestamp: Math.floor(new Date().getTime() / 1000),
      screen_slug: window.location.pathname,
      ...extra,
    };
    if (window.navigator.onLine) {
      analytics.recordEvent(analyticPayload);
    } else {
      dispatch({
        type: 'SET_ANALYTICS_TO_RESEND',
        payload: [...state.analyticsToResend, analyticPayload],
      });
    }
  }

  return {
    recordEvent,
  };
};

import { useRef, useState, useEffect } from 'react';

const useBoundingRect = () => {
  const ref = useRef<HTMLDivElement>();
  const [bRect, setBRect] = useState<any>();

  const set = () => {
    setBRect(ref.current ? ref.current.getBoundingClientRect() : undefined);
  };

  useEffect(() => {
    set();
    window.addEventListener('resize', set);
    return () => window.removeEventListener('resize', set);
  }, []);

  return [bRect, ref];
};

export default useBoundingRect;

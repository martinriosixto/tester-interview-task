import { useEffect, useState } from 'react';

const useLoadingDots = () => {
  const [showLoadingDots, setShowLoadingDots] = useState(false);
  const [loadingDots, setLoadingDotsText] = useState<string>('.');
  useEffect(() => {
    let loadingInterval: NodeJS.Timeout;
    if (showLoadingDots) {
      loadingInterval = setInterval(() => {
        if (loadingDots.length === 3) {
          setLoadingDotsText('.');
        } else {
          setLoadingDotsText(loadingDots + '.');
        }
      }, 1000);
    }
    return () => {
      clearInterval(loadingInterval);
    };
  }, [showLoadingDots, loadingDots]);

  return {
    showLoadingDots,
    setShowLoadingDots,
    loadingDots,
  };
};

export default useLoadingDots;

import { AppContext } from 'context/App';
import { useContext, useEffect } from 'react';
import { api } from 'lib';

const useResendAnswers = () => {
  const { state, dispatch } = useContext(AppContext);

  useEffect(() => {
    const resendAnswers = async () => {
      if (state.answersToResend.length) {
        const answersNotSent = await api.resendAnswers(state.answersToResend);
        dispatch({ type: 'SET_ANSWERS_TO_RESEND', payload: answersNotSent });
      }
    };

    window.addEventListener('online', resendAnswers);
    return () => {
      window.removeEventListener('online', resendAnswers);
    };
  }, [dispatch, state.answersToResend]);
};
export default useResendAnswers;

import { useEffect } from 'react';

export default function useDisableRightClick() {
  useEffect(() => {
    function handleRightClick(event: any) {
      event.preventDefault();
    }

    if (!process.env.REACT_APP_DEV_MODE) {
      document.addEventListener('contextmenu', handleRightClick, false);
    }

    return () => window.removeEventListener('contextmenu', handleRightClick);
  }, []);
}

// @ts-nocheck -- screenfull is not TS friendly :(

import { useEffect, useState } from 'react';

// screenfull is required because it handles browser differences working with full screen
import screenfull from 'screenfull';

export default function useFullScreen() {
  const [isFullScreen, setIsFullScreen] = useState(screenfull.element !== null);

  async function openFullScreen() {
    if (screenfull.isEnabled) {
      await screenfull.request();
    }
  }

  async function closeFullScreen() {
    if (screenfull.isEnabled) {
      await screenfull.exit();
    }
  }

  useEffect(() => {
    function handleChange() {
      setIsFullScreen(screenfull.element !== null);
    }
    if (screenfull.isEnabled) {
      screenfull.on('change', handleChange);
    }

    return () => {
      if (screenfull.isEnabled) {
        screenfull.off('change', handleChange);
      }
    };
  }, []);

  return {
    isFullScreen,
    openFullScreen,
    closeFullScreen,
  };
}

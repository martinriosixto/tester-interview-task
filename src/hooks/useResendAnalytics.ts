import { AppContext } from 'context/App';
import { useContext, useEffect } from 'react';
import { analytics } from 'lib';

const useResendAnalytics = () => {
  const { state, dispatch } = useContext(AppContext);

  useEffect(() => {
    const resendAnswers = async () => {
      if (state.analyticsToResend.length) {
        state.analyticsToResend.forEach((analyticToResend) => {
          analytics.recordEvent(analyticToResend);
        });
        dispatch({
          type: 'SET_ANALYTICS_TO_RESEND',
          payload: [],
        });
      }
    };

    window.addEventListener('online', resendAnswers);
    return () => {
      window.removeEventListener('online', resendAnswers);
    };
  }, [dispatch, state.analyticsToResend]);
};
export default useResendAnalytics;

import { useState, useEffect } from 'react';

function _isPortrait() {
  return window.matchMedia('(orientation: portrait)').matches;
}
export default function useDevice() {
  const [isPortrait, setIsPortrait] = useState(_isPortrait());

  useEffect(() => {
    function handleResize() {
      setIsPortrait(_isPortrait());
    }

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return {
    isPortrait,
  };
}

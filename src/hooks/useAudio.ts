import { useRef, useState, useContext } from 'react';
import { AudioContext } from 'context/Audio';
import { ANALYTIC_EVENT, useAnalytics } from 'hooks/useAnalytics';
import * as Sentry from '@sentry/react';
import { getMp3FilenameFromUrl } from 'lib';

export const useAudio = (src?: string, errorRedirectPath?: string) => {
  const audioContext = useContext(AudioContext);
  const audioRef = useRef<HTMLAudioElement>();
  const [isPlaying, setIsPlaying] = useState(false);
  const { recordEvent } = useAnalytics();

  if (src && audioRef.current === undefined) {
    audioRef.current = audioContext.getAudio(src) || new Audio(src);
  }

  const play = () => {
    return new Promise<void>(async (resolve, reject) => {
      if (!audioRef.current?.paused) {
        resolve();
      } else {
        audioRef.current.onerror = reject;
        audioRef.current.onpause = () => {
          setIsPlaying(false);
          resolve();
        };

        audioRef.current.play().catch((e) => {
          recordEvent(ANALYTIC_EVENT.error_audio_play, {
            audio_src: src,
            exception: e,
          });
          Sentry.withScope((scope) => {
            const mp3Filename = getMp3FilenameFromUrl(src);
            scope.setTag('error', 'audio');
            scope.setTag('error.audio', 'play audio');
            Sentry.captureMessage(`Failed to play audio ${mp3Filename}`);
          });
          // audioContext.showAudioError(
          //   errorRedirectPath ? errorRedirectPath : '/test/map'
          // );
          reject();
        });
        setIsPlaying(true);
      }
    });
  };

  return {
    isPlaying,
    play,
    audio: audioRef.current,
  };
};

export default useAudio;

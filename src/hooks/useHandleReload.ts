import { ANALYTIC_EVENT, useAnalytics } from './useAnalytics';
import { useEffect } from 'react';

const LOCAL_STORAGE_KEY = 'DYL_APP_RELOADED';

const useHandleReload = () => {
  const { recordEvent } = useAnalytics();
  useEffect(() => {
    if (!!localStorage.getItem(LOCAL_STORAGE_KEY)) {
      localStorage.removeItem(LOCAL_STORAGE_KEY);
      recordEvent(ANALYTIC_EVENT.page_reload);
    }

    function handleReload(event: any) {
      event.preventDefault();
      event.returnValue = '';
      localStorage.setItem(LOCAL_STORAGE_KEY, 'true');
      return '';
    }

    if (!process.env.REACT_APP_DEV_MODE) {
      window.addEventListener('beforeunload', handleReload);
      window.addEventListener('pagehide', handleReload);
    }
    return () => {
      window.removeEventListener('beforeunload', handleReload);
      window.removeEventListener('pagehide', handleReload);
    };
  }, [recordEvent]);
};

export default useHandleReload;

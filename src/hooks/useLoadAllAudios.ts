import { useContext, useCallback } from 'react';
import { AudioContext } from 'context/Audio';

export const useLoadAllAudios = (interval = 1000) => {
  const { loadedAudios } = useContext(AudioContext);

  const loadAllAudios = useCallback(
    (audioFilesToWaitFor: string[]) => {
      let tries = 0;
      return new Promise<void>((resolve, reject) => {
        const loadedAudiosInterval = setInterval(() => {
          const isEveryFileLoaded = audioFilesToWaitFor.every((audioFile) => {
            if (loadedAudios && loadedAudios[audioFile]) {
              return loadedAudios[audioFile].dylAudioLoaded === true;
            }
            return false;
          });

          if (isEveryFileLoaded) {
            clearInterval(loadedAudiosInterval);
            return resolve();
          } else if (tries > 60) {
            clearInterval(loadedAudiosInterval);
            return reject();
          } else {
            tries++;
          }
        }, interval);
      });
    },
    [interval, loadedAudios]
  );
  return loadAllAudios;
};

export default useLoadAllAudios;

import { AppContext } from 'context/App';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';

export const useDylNavigate = () => {
  const { state } = useContext(AppContext);
  const navigate = useNavigate();

  return {
    navigate,
    navigateHome: () => {
      let homeUrl = '';
      switch (state.skill_id) {
        case 'reading_writing':
          homeUrl = 'reading';
          break;

        case 'speaking':
          homeUrl = 'speaking';
          break;
      }
      navigate(`/${homeUrl}`);
    },
  };
};

export default useDylNavigate;

import { useRef, useEffect } from 'react';

const useTimeout = (callback: () => void, duration?: number) => {
  const timeoutRef = useRef<number>();
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  useEffect(() => {
    if (duration !== undefined) {
      timeoutRef.current = window.setTimeout(
        () => callbackRef.current(),
        duration
      );

      return () => window.clearTimeout(timeoutRef.current);
    }
  }, [duration]);
  return timeoutRef;
};

export default useTimeout;

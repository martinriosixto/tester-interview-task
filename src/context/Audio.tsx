import React, { createContext, useRef, useEffect } from 'react';
import * as Sentry from '@sentry/react';
import { APP_AUDIO_FILES } from 'assets';
import { DYLAudioElement } from 'types';
import { getMp3FilenameFromUrl } from 'lib';

interface AudioContextProps {
  loadAudio: (src: string[]) => void;
  getAudio: (src: string) => HTMLAudioElement | undefined;
  playAudio: (src: string) => void;
  loadedAudios: Record<string, DYLAudioElement>;
}

export const noOP = () => {};

export const AudioContext = createContext<AudioContextProps>({
  loadAudio: (_: string[]) => noOP,
  playAudio: (_: string) => noOP,
  getAudio: (_: string) => undefined,
  loadedAudios: {},
});

interface AudioContextProviderProps {
  audioFiles?: string[];
}

export const AudioContextProvider: React.FC<AudioContextProviderProps> = ({
  children,
  audioFiles = [],
}) => {
  const loadedAudios = useRef<Record<string, DYLAudioElement>>({});
  const audiosToLoad = useRef<string[]>([...APP_AUDIO_FILES, ...audioFiles]);

  const loadAudioOnInteraction = () => {
    while (audiosToLoad.current.length) {
      const url = audiosToLoad.current.shift();
      if (url && !loadedAudios.current[url]) {
        loadedAudios.current[url] = new Audio(url);
        loadedAudios.current[url].load();
        loadedAudios.current[url].onerror = () => {
          Sentry.withScope((scope) => {
            scope.setTag('error', 'audio');
            scope.setTag('error.audio', 'preloading audio');
            Sentry.captureException(
              new Error(
                `Error loading audio ${getMp3FilenameFromUrl(
                  loadedAudios.current[url].src
                )}`
              )
            );
          });
        };
        loadedAudios.current[url].addEventListener('canplaythrough', () => {
          loadedAudios.current[url].dylAudioLoaded = true;
        });
      }
    }
  };

  const loadAudio = (urls: string[]) => {
    audiosToLoad.current = [...audiosToLoad.current, ...urls];
  };

  useEffect(() => {
    loadAudio(audioFiles);
    window.addEventListener('keydown', loadAudioOnInteraction);
    window.addEventListener('mousedown', loadAudioOnInteraction);
    window.addEventListener('touchstart', loadAudioOnInteraction);

    return () => {
      window.removeEventListener('keydown', loadAudioOnInteraction);
      window.removeEventListener('mousedown', loadAudioOnInteraction);
      window.removeEventListener('touchstart', loadAudioOnInteraction);
    };
  }, [audioFiles]);

  const playAudio = (url: string) => {
    loadedAudios.current[url] && loadedAudios.current[url].play();
  };

  const getAudio = (url: string) => {
    return loadedAudios.current[url];
  };

  return (
    <AudioContext.Provider
      value={{
        getAudio,
        loadAudio,
        playAudio,
        loadedAudios: loadedAudios.current,
      }}
    >
      {children}
    </AudioContext.Provider>
  );
};

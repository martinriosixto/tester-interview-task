/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { createContext, useReducer } from 'react';
import { AppReducer, Dispatch } from 'store';
import { User, State, UserTask, ItemAnswer, Item, Theme, Task } from 'types';

import { online_messages_task_response } from 'responses';

export const INITIAL_STATE: State = {
  isLoading: false,
  skill_id: 'reading_writing',
  examplesSeen: [],
  currentTaskIndex: 0,
  currentItemIndex: 0,
  needsToBeInFullscreen: false,
  theme: {
    headIndex: 1,
    headwearIndex: null,
    facewearIndex: null,
    bodyIndex: 2,
    holdingIndex: null,
    backwearIndex: null,
    legsIndex: 3,
    toneIndex: 0,
    footwearIndex: null,
  },
  surveyResults: {
    Q1: null,
    Q2: null,
    Q3: null,
    Q4: null,
    Q5: null,
  },
  answersToResend: [],
  analyticsToResend: [],
};

const WITH_USER_STATE: State = {
  ...INITIAL_STATE,
  user: {
    age: 35,
    nationality: 'european',
    user_id: '113faa95-deaf-4f4a-85ef-8b6a0e9a6f2a',
    username: 'euro96',
    token_id: '1111-2222-3333',
    name: 'DYL DUDE',
  },
};

const WITH_ONLINE_MESSAGING: State = {
  ...WITH_USER_STATE,
  userTask: online_messages_task_response,
};

export const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, WITH_ONLINE_MESSAGING);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export const AppContext = createContext<{
  state: State;
  dispatch: Dispatch;
}>({
  state: INITIAL_STATE,
  dispatch: () => null,
});

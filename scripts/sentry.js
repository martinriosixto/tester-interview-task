const SentryCli = require('@sentry/cli');

async function createReleaseAndUpload() {
  const COMMIT_REF = process.env.COMMIT_REF || process.env.BITBUCKET_COMMIT;
  if (!COMMIT_REF) {
    console.warn('COMMIT_REF is not set');
    return;
  }

  const cli = new SentryCli();

  try {
    console.log('Creating sentry release ' + COMMIT_REF);
    await cli.releases.new(COMMIT_REF);

    console.log('Uploading source maps');
    await cli.releases.uploadSourceMaps(COMMIT_REF, {
      include: ['build/static/js'],
      urlPrefix: '~/static/js',
      rewrite: false,
    });

    console.log('Finalizing release');
    await cli.releases.finalize(COMMIT_REF);
  } catch (e) {
    console.error('Source maps uploading failed:', e);
  }
}

createReleaseAndUpload();
